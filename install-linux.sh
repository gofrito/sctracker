#!/usr/bin/env bash

# pegged to:
# DiFX 2..6.3 (mark5access 1.5.5)
# Intel oneAPI version 2021.1.0.2427
# (these are hardcoded because future updates will need to
#      check API compatibility before bumping versions here)
DIFX_VERSION="DiFX-2.6.3"
IPP_URL="https://registrationcenter-download.intel.com/akdlm/irc_nas/18487/l_BaseKit_p_2022.1.2.146.sh"
M5A_URL="https://svn.atnf.csiro.au/difx/master_tags/$DIFX_VERSION/libraries/mark5access/"
# This script contains a lot of ">/dev/null" cruft so that output is minimal

# set script to a) error on unset var and b) exit if error occurs
set -u -e

# double check it's okay to nuke any existing IPP installation
RED="\033[0;31m"
RESET="\033[0m"
echo -e "${RED}Warning$RESET Installation requires clean install of Intel oneAPI components and may remove any that are already installed."
printf "Do you wish to continue? [Y/n] "
read stdin
if [[ $stdin != [Yy] ]]; then
    echo "Exiting..."
    exit
fi

echo "Installing SDTracker dependencies"

# make some folders for dependencies to install into
PROJECT_DIR="$PWD"
CACHE_DIR="$PROJECT_DIR/deps/cache"
LOGDIR="$PROJECT_DIR/deps/log"
mkdir -p "$CACHE_DIR"
mkdir -p "$LOGDIR"

echo "...check/install build dependencies"
sudo apt-get update >/dev/null
sudo apt-get install -y --reinstall build-essential >/dev/null
# function to make brew upgrade if already installed, else install
deps_check () {
	echo "Checking $1."
	# will only install if not already installed
	sudo apt-get install -y "$1" &>/dev/null
}
deps_check autoconf
deps_check automake
deps_check libtool
deps_check subversion
deps_check fftw3
deps_check pkg-config

echo "...install DiFX mark5access"
M5A_INSTALLDIR="$PROJECT_DIR/deps/m5a"
cd "$PROJECT_DIR/deps/"
svn co -q "$M5A_URL" m5a # clone mark5access svn repo
find . -name ".svn" -exec rm -fr '{}' + # unversion it
# (so we don't have an svn repo inside a git repo)
cd "$M5A_INSTALLDIR" # change to new directory
echo "Building project."
libtoolize --copy --force &>/dev/null # pre-build steps
aclocal && autoconf && autoheader
automake -a -c --warnings=none
./configure --prefix="$M5A_INSTALLDIR" --disable-dependency-tracking >/dev/null
make CFLAGS=-w >/dev/null || make # build and install with GNU Make
sudo make install >/dev/null || make install
cd "$PROJECT_DIR"

echo "...install Intel IPP and MKL"
IPP_IMAGE="$CACHE_DIR/$(basename "$IPP_URL")"
COMPONENTS="intel.oneapi.lin.ipp.devel:intel.oneapi.lin.mkl.devel"
IPP_INSTALLDIR="$PROJECT_DIR/deps/oneapi"
mkdir -p "$IPP_INSTALLDIR" # make directory for install
echo "Downloading image."
curl --output "$IPP_IMAGE" --url "$IPP_URL" --retry 5 --retry-delay 5 # download installer
chmod +x "$IPP_IMAGE"
EXTRACTED_IMAGE="$CACHE_DIR/$(basename "$IPP_IMAGE" .sh)"
"$IPP_IMAGE" -x -f "$CACHE_DIR" --log "$LOGDIR/extract.log" # extract installer
INSTALLED_COMPONENTS=$("$EXTRACTED_IMAGE/bootstrapper" --list-components)
if [[ $INSTALLED_COMPONENTS == *"true"* ]]; then
    echo "Clearing existing oneAPI components."
    # uninstall any components already installed
    sudo "$EXTRACTED_IMAGE/bootstrapper" --action remove --eula=accept \
        --silent --log-dir="$LOGDIR"
fi
echo "Unpacking installer."
echo -e "${RED}This part might take a few minutes. Don't panic.$RESET"
sudo "$EXTRACTED_IMAGE/bootstrapper" --action install \
    --components="$COMPONENTS" --eula=accept --silent --log-dir="$LOGDIR" \
	--install-dir="$IPP_INSTALLDIR" # install from image
rm -rf "$IPP_IMAGE"
rm -rf "$EXTRACTED_IMAGE"

echo "Installing SDTracker"
echo "...running Make"
make
echo "...running Make Install"
sudo make install

echo "Done."
