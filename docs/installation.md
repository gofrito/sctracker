# Installation

This guide is for those who, for whatever reason, wish to install and compile SDTracker without the use of the included installation scripts `install-macos.sh` and `install-linux.sh`.

It will guide you through the installation of project dependencies, as well as the build process for the project itself.

> ⚠️ If you are here because you tried the installation script and it failed to install the required oneAPI components, try **uninstalling any oneAPI components already installed on your system** and then running the script again.

Checklist:

- [] Install mark5access
- [] Install Intel oneAPI components: IPP and MKL
- [] Build SDTracker

Note that this process requires [GNU Make](https://www.gnu.org/software/make/), which is installed by default on all common GNU Linux systems but may require Xcode Command Line Tools to be installed on macOS (run `xcode-select -install`).

## Installing mark5access

SDTracker relies on the library module **mark5access** from the NRAO's [DiFX correlator suite](https://safe.nrao.edu/wiki/bin/view/VLBA/Software#DiFX), for parsing of various input formats. mark5access is only distributed as source code, so to install it also requires the tools to compile it.

**1. Download the current mark5access from the ATNF SVN mirror.**

> The current stable version of DiFX mark5access as of **01 April 2022** is **DiFX-2.6.3** (mark5access **1.5.5**) and can be found at [https://svn.atnf.csiro.au/difx/master_tags/DiFX-<ins>2.6.3</ins>/libraries/mark5access/](https://svn.atnf.csiro.au/difx/master_tags/DiFX-2.6.3/libraries/mark5access/).

Download the file from the repository with your preferred method (e.g. using [svn](https://subversion.apache.org) and then removing the version control components, or using a download tool like [curl](https://curl.se)). The canonical location for mark5access installation is `<project directory>/deps/m5a/<mark5access repository contents>`.

In Linux:
```sh
mkdir deps/m5a
cd deps/m5a/
wget -r --no-parent https://svn.atnf.csiro.au/difx/master_tags/DiFX-2.6.3/libraries/mark5access/
```
**2. Compile mark5access.**

mark5access requires [automake](https://www.gnu.org/software/automake/), [libtool](https://www.gnu.org/software/libtool/) and [pkgconfig](https://www.freedesktop.org/wiki/Software/pkg-config/) to build, and some versions also require [fftw](https://www.fftw.org) to be installed. After installing these with your preferred method (e.g. [apt-get](https://linux.die.net/man/8/apt-get) on Linux or [homebrew](https://brew.sh) on macOS), run:

```sh
# move to mark5access directory
cd deps/m5a
# pre-build configuration steps
aclocal
if [[ "$OSTYPE" == "Darwin" ]]; then 
    # on macOS libtoolize is glibtoolize
    glibtoolize --copy --force
else
    libtoolize --copy --force
fi
autoconf
autoheader
automake -a -c
./configure --prefix="$PWD"
# now build and install
make && make install
```

At this point, your directory structure should look like this:

```
📦 sdtracker
┣ 📂 deps
┃ ┗ 📂 m5a
┃   ┗ 📜 <mark5access repository contents>
┗ 📜 <sdtracker repository contents>
```

## Installing Intel oneAPI Components

**1. Download current oneAPI Installer from Intel Downloads Center.**

> As of **01 April 2022**, the **BaseKit installer** can be found at [https://www.intel.com/content/www/us/en/developer/tools/oneapi/base-toolkit-download.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/base-toolkit-download.html) with the current version **2022.1**.

**2. Run the oneAPI Installer.**

To run the installer often requires first unzipping with the password "accept", meaning "I accept the included EULA". One this is done, run the installer and select the components **Intel Performance Primitives (IPP)** and **Math Kernel Library (MKL)**. The canonical location for oneAPI installation is `<project directory>/deps/oneapi/<oneapi installer contents>`. Please note that Intel will not allow more than one installation at a time, so if you have either of these libraries already you may have to **uninstall them first**.

At this point, your directory structure should look like this:

```
📦 sdtracker
┣ 📂 deps
┃ ┣ 📂 m5a
┃ ┃ ┗ 📜 <mark5access repository contents>
┃ ┗ 📂 oneapi
┃   ┗ 📜 <oneapi installer contents>
┗ 📜 <sdtracker repository contents>
```

## Building SDTracker

Run in the project directory: `make && make install`

You should now be able to run the command `swspectrometer` or `sctracker` and get a results (albeit telling you your usage is incorrect and that the process requires an iniFile).

Return to the [Wiki homepage](https://gitlab.com/gofrito/sctracker/-/wikis/home) for usage instructions.
