# SDTracker Developer Guide

[[_TOC_]]

> :no_entry: This project only officially supports **macOS x86** and **Debian-based Linux** targets. Others platforms may work, but functionality and compatibility are **not guaranteed**.

## Quicklook

"There seems to be an issue with..."

* configuration -> Look in `swspectrometer::main()` (swspec) or `Settings::summarize()` (sctrac).
* input parsing, with a raw or internal format -> look in `DataUnpackers::extract_samples()`.
* input parsing, with a mark5access format -> ask the [DiFX users group](https://listmgr.nrao.edu/mailman/listinfo/difx-users).
* signal processing -> look in `TaskCore::doMaths()`.

## Structure and Components

```
📂 sctracker
┣ 📁 deps
┃ ┣ 📁 mark5access # open source NRAO code
┃ ┗ 📁 oneapi # closed source intel libs
┃   ┣ ipp # installed from script
┃   ┗ mkl # installed from script
┣ 📁 swspec
┃ ┣ 📄 ...code files
┃ ┗ 📄 ...
┣ 📁 sctrac
┃ ┣ 📄 ...code files
┃ ┗ 📄 ...
┣ 📁 shared
┃ ┣ 📄 ...code files
┃ ┗ 📄 ...
┣ 📄 (usage information)
┗ 📄 (installation scripts)
```

The top-level project repository contains:

* `deps` (created on install), where the source and libraries for dependencies Intel oneAPI and DiFX mark5access live. These should be built and aliased to the relevant places upon install and are not rebuilt with the project Make process. If changes are made in dependencies, they will need to be rebuilt via their respective processes.
* `swspec`, where source code specific to the software spectrometer lives. This is built as part of the top-level compilation process but can also be built independently with target `make swspec`.
* `sctrac`, where source code specific to the spacecraft tracker lives. This is built as part of the top-level compilation process but can also be built independently with target `make sctrac`. Note, however, that sctrac cannot operate without first having input produced by swspec.
* `shared`, where source code shared by both the software spectrometer and spacecraft tracker lives. Code duplication between the two projects remains, so more code is expected to slowly migrate into this shared lib over time.

## Data Flow

Entry points:
* software spectrometer - `/swspec/swspectrometer.cpp`
* spacecraft tracker - `/sctrac/sctracker.cpp`

The main function of each project begins by parsing command line arguments, then invoking `IniParser` functions to assign information to the `Settings` object which will hold configuration information throughout processing. In swspec, `Settings` is only provided as a header file and the parsing is done in swspec's `main` function, whereas in sctrac this instead happens in the `Settings` source file through the function `Settings::summarize()`. These may be able to be combined into a single abstract class in the future, for better consistency between the two sub-projects.

Once settings have been loaded, the requested `DataSource` and `DataSink` objects are loaded and any associated `FileSource`s opened. Then, at the end of each `main` function, a `TaskDispatcher` object is created with the given settings and then told to `run()`.

The `TaskDispatcher` is responsible for allocating work. It creates separate `TaskCore` objects for the requested number of cores (set in the iniFile) and distributes source data among them. Upon creation, the `TaskDispatcher` sets each core to reset any required buffers and `prepare()`--which does some process setup like allocating memory space and pre-calculating counter and size values for the upcoming task. Once the `TaskDispatcher` is told to `run()` it will iterate over each of its child cores, telling each to:

* run its core task
* preload the next source buffer
* await results from its core task
* combine its results with any existing already returned to `TaskDispatcher`
* reset its buffers
* if this is the end of this core's input stream, `finalise()` (free all the memory it had allocated)
* if this is the end of this core's input stream, tell each of its output sinks to `close()`

The high-level process is now complete. 

So the majority of time is spent in the core of the process: `TaskCore`. When a `TaskCore` is told to `prepare()` for work, it creates a pthread with the kernel `taskcore_worker`. The `taskcore_worker` is a wrapper around a function called `doMaths()`, in which all of the signal processing logic lives.

The missing piece here is how data is assembled for I/O surrounding the `TaskCore` processes. This primarily occurs through `Buffer` objects, which will have been written into by the corresponding `Settings` object's stored `DataSource` and `DataSink` references. Currently, these will defer to the concrete implementations `FileSource` and `FileSink` respectively, though this abstraction was evidently designed to allow the addition of other implementations such as `VSIBSource` and `TeeSink`. Vestigial, partially-implemented code for these classes remains in the project, and hopefully will be completed to allow for real-time stream input to swspec in the future.

As part of the work done by `TaskDispatcher::prepare()`, a `DataSource` will have used the `DataUnpackerFactory` functionality to find the suitable `DataUnpacker` for its input format. This will be called on to `extract_samples()` for processing inside the `TaskCore::doMaths()` function. This is critical to perform the demultiplexing required to read modern multiplexed input formats such as VDIF. Depending on the format, this unpacking may be deferred to mark5access via a `mark5_stream` object.