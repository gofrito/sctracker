#!/usr/bin/env bash

# pegged to:
# DiFX 2..6.3 (mark5access 1.5.5)
# Intel oneAPI version 2021.1.0.2427
# (these are hardcoded because future updates will need to
#      check API compatibility before bumping versions here)
DIFX_VERSION="DiFX-2.6.3"
IPP_URL="https://registrationcenter-download.intel.com/akdlm/irc_nas/18487/l_BaseKit_p_2022.1.2.146.sh"
M5A_URL="https://svn.atnf.csiro.au/difx/master_tags/$DIFX_VERSION/libraries/mark5access/"
# This script contains a lot of ">/dev/null" cruft so that output is minimal

# set script to a) error on unset var and b) exit if error occurs
set -u -e

# create a SUDO variable depending on whether we're in a CI env or not
SUDO=''

if [[ -z "${CI}" ]]; then
	SUDO='sudo'
fi

echo "Installing SDTracker dependencies"

# make some folders for dependencies to install into
PROJECT_DIR="$PWD"
CACHE_DIR="$PROJECT_DIR/deps/cache"
LOGDIR="$PROJECT_DIR/deps/log"
mkdir -p "$CACHE_DIR"
mkdir -p "$LOGDIR"

echo "...install DiFX mark5access"
M5A_INSTALLDIR="$PROJECT_DIR/deps/m5a"
cd "$PROJECT_DIR/deps/"
svn co -q "$M5A_URL" m5a # clone mark5access svn repo
find . -name ".svn" -exec rm -fr '{}' + # unversion it
# (so we don't have an svn repo inside a git repo)
cd "$M5A_INSTALLDIR" # change to new directory
echo "Building project."
libtoolize --copy --force &>/dev/null # pre-build steps
aclocal && autoconf && autoheader
automake -a -c --warnings=none
./configure --prefix="$M5A_INSTALLDIR" --disable-dependency-tracking >/dev/null
make CFLAGS=-w >/dev/null || make # build and install with GNU Make
$SUDO make install >/dev/null || make install
cd "$PROJECT_DIR"
