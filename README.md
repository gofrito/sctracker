![](docs/header_image.png)

# Spacecraft Doppler Tracker (SDTracker)

**© 2022 Jan Wagner / Guifre Molera Calves, with contributions from Nelson Nunes, Sergei Pogrebenko and Mars Buttfield-Addison.**

SDTracker is an end-to-end system for high spectral resolution multi-tone Doppler tracking of spacecraft. It is made up of the software spectrometer [`swspec`](/swspec) and the processing library [`sctrac`](/sctrac). SDTracker can extract the phase of spacecraft signals from Mark5A, Mark5B, VLBA or VDIF data, and output to either binary or ASCII format.

For detailed usage instructions, please see the [project documentation](https://gitlab.com/gofrito/sctracker/-/wikis/home). Developers may also like to read the [Developer Guide](https://gitlab.com/gofrito/sctracker/-/wikis/developer), in particular the [manual installation instructions](https://gitlab.com/gofrito/sctracker/-/wikis/installation). Please note that some operations require the Python utilities library [`pysctrack`](https://gitlab.com/gofrito/pysctrack).

## Quickstart

**User Quickstart**

1. Download the latest binaries from the [Releases](https://gitlab.com/gofrito/sctracker/-/releases) page.
2. Run the full SDTracker process with the command `sctracker [iniFile]`, or the software spectrometer alone with the command `swspectrometer [iniFile] [inputFile]`.

**Developer Quickstart**

1. Clone the repo.
2. Run the enclosed installation script** for your OS (macOS: `install-macos.sh`, Linux: `install-linux.sh`).

**May require elevated permissions for package installation. Remember to make your script executable with the command `chmod +x <script_filename>`.

## Dependencies

* [DiFX `mark5access`](https://safe.nrao.edu/wiki/bin/view/VLBA/DiFX)—handles parsing of VLBA, VDIF, Mark5A input
* [Intel oneAPI](https://www.intel.com/content/www/us/en/developer/tools/oneapi/overview.html)—IPP and MKL for FFT and matrix manipulation functionality
* [`gcc`](https://gcc.gnu.org)
* Additional build tools: [`automake`](https://www.gnu.org/software/automake/) (required to build `mark5access`), [`autoconf`](https://www.gnu.org/software/autoconf/) (required to build `mark5access`), [`libtool`](https://www.gnu.org/software/libtool/) (required to build `mark5access`), [`svn`](https://subversion.apache.org) (used to download `mark5access` in installation script)
* OS-specific utilities used during installation: [`homebrew`](https://brew.sh) on macOS, [Rosetta 2](https://developer.apple.com/documentation/apple-silicon/about-the-rosetta-translation-environment) on macOS with arm64

## License and Attribution

This project is licensed under the terms of the [GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.en.html). **This is a [copyleft](https://www.gnu.org/licenses/copyleft.en.html) license.**

Though it is not required under the terms of this license, the contributors appreciate the inclusion of the following citation in any works that make use of this software ([bib](/CITATION)):

> Molera Calvés, G. and Pogrebenko, S. V. and Wagner, J. F. and Cimò, G. and Gurvits, L. I. and Bocanegra-Bahamón, T. M. and Duev, D. A. and Nunes, N. V. (2021). High spectral resolution multi-tone Spacecraft Doppler tracking software: Algorithms and implementations. *Publications of the Astronomical Society of Australia*, 38, E065. doi:[10.1017/pasa.2021.56](https://doi.org/10.1017/pasa.2021.56)

## Planned Improvements

- [ ] Further user-facing documentation
- [ ] Support for K5 input format
- [ ] Integration of `pysctrack` utilities
- [ ] Real-time stream processing mode for `swspec`
- [ ] Further reduce duplicate classes between `swspec` and `sctrac` into shared lib
- [ ] Automate pipeline: lint, build and test
- [ ] IniFile debugging: add list of allowed keys, warn on unused key
- [ ] Proper 'debug mode' integration