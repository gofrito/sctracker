# Files description

## Short 10-sec test files

- Ettus_1MHz_32bits.rawS: 2 Msps, 32-bits, 1-channel. Data contains JUICE signal on 2023-04-17T10:30:10, baseband frequency: 8435.50 MHz.
- Data_250kHz_32bits.rawS: 0.5 Msps, 32-bits, 1-channel. Data contains a single tone frequency drifting on time.

