#!/usr/bin/env bash

# pegged to:
# DiFX 2.6.3 (mark5access 1.5.4/1.5.5/1.6.0 depending on install date)
# Intel oneAPI version 2021.1.0.2427
# (these are hardcoded because future updates will need to
#      check API compatibility before bumping versions here)
DIFX_VERSION="DiFX-2.6.3"
IPP_URL="https://registrationcenter-download.intel.com/akdlm/irc_nas/17426/m_BaseKit_p_2021.1.0.2427.dmg"
M5A_URL="https://svn.atnf.csiro.au/difx/master_tags/$DIFX_VERSION/libraries/mark5access/"
# This script contains a lot of ">/dev/null" cruft so that output is minimal

# set cleanup tasks to occur on script end or exit
trap "cleanup" EXIT
cleanup () {
	# if we've exited early, it might be while we have the IPP install volume mounted
	# so attempt to unmount it just in case and just keep quiet if it wasn't found
	hdiutil detach /Volumes/"$(basename "$IPP_URL" .dmg)" -quiet;
}
# set script to a) error on unset var and b) exit if error occurs
set -u -e

RED="\033[0;31m"
RESET="\033[0m"

# shortcut to bypass all the IPP questions while testing
if [[ $# < 1 || $1 != "--no-ipp" ]]; then
	# double check it's okay to nuke any existing IPP installation because there is no conclusive way to 
	# check if it is already installed with the required components
	echo -e "${RED}Warning$RESET Installation requires clean install of Intel oneAPI components and may remove any that are already installed."
	printf "Do you wish to continue and possibly overwrite your installation of IPP? [Y/n] "
	read stdin
	if [[ $stdin != [Yy] ]]; then
		printf "Do you wish to attempt installation without IPP, in case you already have the correct components installed? [Y/n] "
		read stdin
		if [[ $stdin != [Yy] ]]; then
			echo "Exiting..."
			exit
		fi
	fi
fi
	
echo "Installing SDTracker dependencies"

# make some folders for dependencies to install into
PROJECT_DIR="$PWD"
CACHE_DIR="$PROJECT_DIR/deps/cache"
LOGDIR="$PROJECT_DIR/deps/log"
mkdir -p "$CACHE_DIR"
mkdir -p "$LOGDIR"

echo "...check/install macOS Developer Tools"
# developer tools should include things we need: gcc and also (sometimes) svn
if [[ $(xcode-select -p) == "" ]]; then
	xcode-select --install
else
	echo "Already installed."
fi

echo "...check/install homebrew"
if [[ $(command -v brew) == "" ]]; then
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
	echo "Already installed."
fi

ARCH_PREFIX=""
CPU_MANUFACTURER=$(/usr/sbin/sysctl -n machdep.cpu.brand_string)
if [[ $CPU_MANUFACTURER = *"Apple"* ]]; then
	ARCH_PREFIX="arch -arm64 "
	echo "...check/install Rosetta"
	if [[ $(pkgutil --pkgs | grep "com.apple.pkg.Rosetta") == "" ]]; then 
		install-rosetta() ( # subshell to ensure scope of temp trap
			echo "Installing Rosetta."
			trap "echo 'Something went wrong.'" ERR
			/usr/sbin/softwareupdate --install-rosetta --agree-to-license >/dev/null
		)
		install-rosetta
	else
		echo "Already installed."
	fi
fi

echo "...check/install build dependencies"
brew update
# function to make brew upgrade if already installed, else install
brew_check () {
	echo "Checking $1."
	# check if already installed, else install
	if [[ $# > 1 && $2 == "--no-arm " && $ARCH_PREFIX != "" ]]; then
		brew list "$1" &>/dev/null || arch -x86_64 brew install "$1"
	else 
		brew list "$1" &>/dev/null || $ARCH_PREFIX brew install "$1"
	fi
}
brew_check autoconf
brew_check automake 
brew_check libtool
brew_check svn 
brew_check fftw --no-arm
brew_check pkgconfig

echo "...install DiFX mark5access"
M5A_INSTALLDIR="$PROJECT_DIR/deps/m5a"
cd "$PROJECT_DIR/deps/"
svn co -q "$M5A_URL" m5a # clone mark5access svn repo
find . -name ".svn" -exec rm -fr '{}' + # unversion it
# (so we don't have an svn repo inside a git repo)
cd "$M5A_INSTALLDIR" # change to new directory
build-m5a() (
	check-known-issue-m5a() {
		if [[ $ARCH_PREFIX != "" ]]; then 
			echo -e "${RED}Please check known issues such as the requirement for manual build and installation of x86 FFTW to make mark5access compile on arm64 macOS."
		fi
	}
	trap check-known-issue-m5a ERR
	echo "Building project."
	glibtoolize --copy --force &>/dev/null # pre-build steps
	aclocal &>/dev/null && autoconf &>/dev/null && autoheader &>/dev/null
	automake -a -c --warnings=none &>/dev/null
	./configure --prefix="$M5A_INSTALLDIR" >/dev/null
	make CFLAGS=-w >/dev/null || make # build and install with GNU Make
	sudo make install >/dev/null || make install
	cd "$PROJECT_DIR"
)
build-m5a

echo "...install Intel IPP and MKL"
IPP_IMAGE="${CACHE_DIR}/$(basename "$IPP_URL")"
COMPONENTS="intel.oneapi.mac.ipp.devel:intel.oneapi.mac.mkl.devel"
IPP_INSTALLDIR="$PROJECT_DIR/deps/oneapi"
mkdir -p "$IPP_INSTALLDIR" # make directory for install
echo "Downloading image."
curl --output "$IPP_IMAGE" --url "$IPP_URL" --retry 5 --retry-delay 5 # download install image
hdiutil attach "$IPP_IMAGE" # mount install image
IPP_DMG="/Volumes/$(basename "$IPP_IMAGE" .dmg)/bootstrapper.app/Contents/MacOS/bootstrapper"
INSTALLED_COMPONENTS=$("$IPP_DMG" --list-components)
if [[ $INSTALLED_COMPONENTS == *"true"* ]]; then
    echo "Clearing existing oneAPI components."
    # uninstall any components already installed
	$IPP_DMG --action remove --eula=accept --silent --log-dir="$LOGDIR" # uninstall any components already installed
fi
echo "Unpacking image."
$IPP_DMG --action install --components="$COMPONENTS" --eula=accept --silent \
	--log-dir="$LOGDIR" --install-dir="$IPP_INSTALLDIR" # install from image
cleanup # unmount image

echo "Installing SDTracker"
echo "...running Make"
make
echo "...running Make Install"
sudo make install

echo "Done."