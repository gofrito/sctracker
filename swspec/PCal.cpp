/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2008 Jan Wagner / Sergei Pogrebenko                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/********************************************************************************************************
 * @file PCal.h
 * Multi-tone Phase Cal Extraction
 *
 * @brief Extracts and integrates multi-tone phase calibration signal information from an input signal.
 *
 * The principle relies on the fact that with a comb spacing of say 1 MHz and a sampling rate of say
 * 32 MHz the single 1 MHz and also all of its multiples (1, 2, .. 16 MHz in the band) have at least
 * one full sine period in 32MHz/1MHz = 32 samples. For extraction and time integration, we simply
 * have to segment the input signal into 32-sample pieces (in the above example) and integrate these
 * pieces.
 *
 * A tiny FFT performed on the integrated 32-bin result gives you the amplitude and phase
 * of every tone. As the PCal amplitude is in practice constant over a short frequency band,
 * the amplitude and phase info after the FFT directly gives you the equipment filter response.
 *
 * The extracted PCal can also be analyzed in the time domain (no FFT). The relative, average instrumental
 * delay time can be found directly by estimating the position of the peak in the time-domain data.
 *
 * @author   Jan Wagner
 * @author   Sergei Pogrebenko
 * @version  1.0/2008
 * @license  GNU GPL v3
 *
 * Changelog:
 *   05Oct2009 - added support for arbitrary input segment lengths
 *
 ********************************************************************************************************/

#include "PCal.h"
#include <iostream>
#include <cmath>
using std::cerr;
using std::endl;

#if defined(__APPLE__)
#include <stdlib.h>
#include "MacPort.h"
#else
#include <malloc.h>
#endif

#define UNROLL_BY_4(x) \
    {x} {x} {x} {      \
        x              \
    }
#define VALIGN __attribute__((aligned(16)))

class pcal_config_pimpl
{
public:
    pcal_config_pimpl(){};
    ~pcal_config_pimpl(){};

public:
    double dphi;
    Ipp32fc *rotator;      // pre-cooked oscillator values
    Ipp32fc *rotated;      // temporary
    Ipp32fc *pcal_complex; // temporary unassembled output, later final output
    Ipp32f *pcal_real;     // temporary unassembled output for the pcaloffsethz==0.0f case
    size_t rotatorlen;
    size_t pcal_index;    // zero, changes when extract() is called at least once with "leftover" samples
    size_t rotator_index; // zero, changes when extract() is called at least once with "leftover" samples
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// BASE CLASS: factory and helpers
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Factory that returns a new PCal extractor object. The optimal implementation
 * is selected based on the input parameters.
 * @param bandwidth_hz     Bandwidth of the input signal in Hertz
 * @param pcal_spacing_hz  Spacing of the PCal signal, comb spacing, typically 1e6 Hertz
 * @param pcal_offset_hz   Offset of the first PCal signal from 0Hz/DC, typically 10e3 Hertz
 * @return new PCal extractor class instance
 */
PCal *PCal::getNew(double bandwidth_hz, double pcal_spacing_hz, double pcal_offset_hz)
{
    if (pcal_offset_hz == 0.0f)
    {
        return new PCalExtractorTrivial(bandwidth_hz, pcal_spacing_hz);
    }
    else
    {
        return new PCalExtractorShifting(bandwidth_hz, pcal_spacing_hz, pcal_offset_hz);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// DERIVED CLASS: extraction of zero-offset PCal signals
/////////////////////////////////////////////////////////////////////////////////////////////////////////

PCalExtractorTrivial::PCalExtractorTrivial(double bandwidth_hz, double pcal_spacing_hz)
{
    /* Derive config */
    _cfg = new pcal_config_pimpl();
    _fs_hz = 2 * bandwidth_hz;
    _N_bins = _fs_hz / std::abs(pcal_spacing_hz);
    _pcaloffset_hz = 0.0f;

    /* Allocate */
    _cfg->pcal_complex = static_cast<Ipp32fc *>(memalign(128, sizeof(Ipp32fc) * _N_bins * 2));
    _cfg->pcal_real = static_cast<Ipp32f *>(memalign(128, sizeof(Ipp32f) * _N_bins * 2));
    this->clear();
}

PCalExtractorTrivial::~PCalExtractorTrivial()
{
    free(_cfg->pcal_complex);
    free(_cfg->pcal_real);
    delete _cfg;
}

/**
 * Set the extracted and accumulated PCal data back to zero.
 */
void PCalExtractorTrivial::clear()
{
    _samplecount = 0;
    _finalized = false;
    ippsZero_32fc(_cfg->pcal_complex, _N_bins * 2);
    ippsZero_32f(_cfg->pcal_real, _N_bins * 2);
    _cfg->rotator_index = 0;
    _cfg->pcal_index = 0;
}

/**
 * Extracts multi-tone PCal information from a single-channel signal segment
 * and integrates it to the class-internal PCal extraction result buffer.
 * There are no restrictions to the segment length.
 *
 * If you integrate over a longer time and several segments, i.e. perform
 * multiple calls to this function, take care to keep the input
 * continuous (i.e. don't leave out samples).
 *
 * If extraction has been finalized by calling getFinalPCal() this function
 * returns False. You need to call clear() to reset.
 *
 * @paran samples Chunk of the input signal consisting of 'float' samples
 * @param len     Length of the input signal chunk
 * @return true on success
 */
bool PCalExtractorTrivial::extractAndIntegrate(Ipp32f const *samples, const size_t len)
{
    if (_finalized)
    {
        return false;
    }

    Ipp32f const *src = samples;
    Ipp32f *dst = &(_cfg->pcal_real[_cfg->pcal_index]);
    size_t tail = (len % _N_bins);
    size_t end = len - tail;

    /* Process the first part that fits perfectly */
    for (size_t n = 0; n < end; n += _N_bins, src += _N_bins)
    {
        ippsAdd_32f_I(src, dst, _N_bins);
    }

    /* Handle any samples that didn't fit */
    if (tail != 0)
    {
        ippsAdd_32f_I(src, dst, tail);
        _cfg->pcal_index = (_cfg->pcal_index + tail) % _N_bins;
    }

    /* Done! */
    _samplecount += len;
    return true;
}

/**
 * Performs finalization steps on the internal PCal results if necessary
 * and then copies these PCal results into the specified output array.
 * Data in the output array is overwritten with PCal results.
 *
 * @param pointer to user PCal array with getLength() values
 */
void PCalExtractorTrivial::getFinalPCal(Ipp32fc *out)
{
    if (!_finalized)
    {
        _finalized = true;
        ippsAdd_32f_I(/*src*/ &(_cfg->pcal_real[_N_bins]), /*srcdst*/ &(_cfg->pcal_real[0]), _N_bins);
        ippsRealToCplx_32f(/*srcRe*/ _cfg->pcal_real, /*srcIm*/ NULL, _cfg->pcal_complex, _N_bins);
    }
    ippsCopy_32fc(/*src*/ _cfg->pcal_complex, /*dst*/ out, _N_bins);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// DERIVED CLASS: extraction of PCal signals with non-zero offset
/////////////////////////////////////////////////////////////////////////////////////////////////////////

PCalExtractorShifting::PCalExtractorShifting(double bandwidth_hz, double pcal_spacing_hz, double pcal_offset_hz)
{
    /* Derive config */
    _fs_hz = 2 * bandwidth_hz;
    _N_bins = _fs_hz / std::abs(pcal_spacing_hz);
    _pcaloffset_hz = pcal_offset_hz;
    _cfg = new pcal_config_pimpl();
    _cfg->rotatorlen = _fs_hz / Helpers::gcd(std::abs(_pcaloffset_hz), _fs_hz);

    /* Allocate */
    _cfg->pcal_complex = static_cast<Ipp32fc *>(memalign(128, sizeof(Ipp32fc) * _N_bins * 2));
    _cfg->pcal_real = static_cast<Ipp32f *>(memalign(128, sizeof(Ipp32f) * _N_bins * 2));
    _cfg->rotator = static_cast<Ipp32fc *>(memalign(128, sizeof(Ipp32fc) * _cfg->rotatorlen * 2));
    _cfg->rotated = static_cast<Ipp32fc *>(memalign(128, sizeof(Ipp32fc) * _cfg->rotatorlen * 2));
    this->clear();

    /* Prepare frequency shifter/mixer lookup */
    _cfg->dphi = 2 * M_PI * (_pcaloffset_hz / _fs_hz);
    for (size_t n = 0; n < (2 * _cfg->rotatorlen); n++)
    {
        double arg = _cfg->dphi * double(n);
        _cfg->rotator[n].re = Ipp32f(cos(arg));
        _cfg->rotator[n].im = Ipp32f(sin(arg));
    }
}

PCalExtractorShifting::~PCalExtractorShifting()
{
    free(_cfg->pcal_complex);
    free(_cfg->pcal_real);
    free(_cfg->rotator);
    free(_cfg->rotated);
    delete _cfg;
}

/**
 * Set the extracted and accumulated PCal data back to zero.
 */
void PCalExtractorShifting::clear()
{
    _samplecount = 0;
    _finalized = false;
    ippsZero_32fc(_cfg->pcal_complex, _N_bins * 2);
    ippsZero_32f(_cfg->pcal_real, _N_bins * 2);
    ippsZero_32fc(_cfg->rotated, _cfg->rotatorlen * 2);
    _cfg->rotator_index = 0;
    _cfg->pcal_index = 0;
}

/**
 * Extracts multi-tone PCal information from a single-channel signal segment
 * and integrates it to the class-internal PCal extraction result buffer.
 * There are no restrictions to the segment length.
 *
 * If you integrate over a longer time and several segments, i.e. perform
 * multiple calls to this function, take care to keep the input
 * continuous (i.e. don't leave out samples).
 *
 * If extraction has been finalized by calling getFinalPCal() this function
 * returns False. You need to call clear() to reset.
 *
 * @paran samples Chunk of the input signal consisting of 'float' samples
 * @param len     Length of the input signal chunk
 * @return true on success
 */
bool PCalExtractorShifting::extractAndIntegrate(Ipp32f const *samples, const size_t len)
{
    if (_finalized)
    {
        return false;
    }

    Ipp32f const *src = samples;
    Ipp32fc *dst = &(_cfg->pcal_complex[_cfg->pcal_index]);
    size_t tail = (len % _cfg->rotatorlen);
    size_t end = len - tail;

    /* This method is only marginally different from the PCalExtractorTrivial method.
     * Because now our multi-tone PCal signal tones do not reside at integer MHz frequencies,
     * or rather, not at integer multiples of the tone spacing of the comb, the first PCal
     * tone is found at some offset '_pcaloffset_hz' away from 0Hz/DC.
     * So we just use a complex oscillator to shift the input signal back into place.
     * The complex oscillator has a period of _fs_hz/gcd(_fs_hz,_pcaloffset_hz).
     * The period is usually very short, say, 1600 samples. We precomputed those
     * in the constructor and use them here.
     */

    /* Process the first part that fits perfectly (and note: rotatorlen modulo _N_bins is 0!) */
    for (size_t n = 0; n < end; n += _cfg->rotatorlen, src += _cfg->rotatorlen)
    {
        ippsMul_32f32fc(/*A*/ src,
                        /*B*/ &(_cfg->rotator[_cfg->rotator_index]),
                        /*dst*/ &(_cfg->rotated[_cfg->rotator_index]),
                        /*len*/ _cfg->rotatorlen);
        Ipp32fc *pulse = &(_cfg->rotated[_cfg->rotator_index]);
        for (size_t p = 0; p < (_cfg->rotatorlen / _N_bins); p++)
        {
            ippsAdd_32fc_I(/*src*/ pulse, /*srcdst*/ dst, _N_bins);
            pulse += _N_bins;
        }
    }

    /* Handle any samples that didn't fit */
    if (tail != 0)
    {
        ippsMul_32f32fc(
            /*A*/ src,
            /*B*/ &(_cfg->rotator[_cfg->rotator_index]),
            /*dst*/ &(_cfg->rotated[_cfg->rotator_index]),
            /*len*/ tail);
        Ipp32fc *pulse = &(_cfg->rotated[_cfg->rotator_index]);
        size_t tail2 = (tail % _N_bins);
        size_t end2 = tail - tail2;
        for (size_t p = 0; p < (end2 / _N_bins); p++)
        {
            ippsAdd_32fc_I(/*src*/ pulse, /*srcdst*/ dst, _N_bins);
            pulse += _N_bins;
        }
        /* Samples that didn't fit _N_bins */
        ippsAdd_32fc_I(/*src*/ pulse, /*srcdst*/ dst, tail2);
        _cfg->rotator_index = (_cfg->rotator_index + tail) % _cfg->rotatorlen;
        _cfg->pcal_index = (_cfg->pcal_index + tail2) % _N_bins;
    }

    /* Done! */
    _samplecount += len;
    return true;
}

/**
 * Performs finalization steps on the internal PCal results if necessary
 * and then copies these PCal results into the specified output array.
 * Data in the output array is overwritten with PCal results.
 *
 * @param pointer to user PCal array with getLength() values
 */
void PCalExtractorShifting::getFinalPCal(Ipp32fc *out)
{
    if (!_finalized)
    {
        _finalized = true;
        ippsAdd_32fc_I(/*src*/ &(_cfg->pcal_complex[_N_bins]), /*srcdst*/ &(_cfg->pcal_complex[0]), _N_bins);
    }
    ippsCopy_32fc(/*src*/ _cfg->pcal_complex, /*dst*/ out, _N_bins);
}