/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "FileSink.h"
#include <iostream>
#include <fstream>
#include <string>

/**
 * Open the file
 * @return int Returns 0 on success
 * @param  uri File path
 */
int FileSink::open(std::string uri)
{
   if (ofile.is_open())
   {
      ofile.close();
   }
   ofile.open(uri.c_str(), std::ios::binary | std::ofstream::trunc | std::ios::out);
   if (ofile.is_open())
   {
      ofile.precision(12);
      return 0;
   }
   else
   {
      std::cerr << "Could not open output file " << uri << std::endl;
      return -1;
   }
}

/**
 * Write data into resource
 * @return Returns the number of bytes written
 * @param  buf Pointer to the buffer to write out.
 */
size_t FileSink::write(Buffer *buf)
{
   if (buf == NULL || buf->getLength() <= 0 || this->settings == NULL)
   {
      return 0;
   }

   return write(buf->getData(), buf->getLength());
}

/**
 * Write array data into resource
 * @return int Returns the number of bytes written
 * @param  data   Pointer to the data to write out.
 * @param  length The number of bytes to write.
 */
size_t FileSink::write(char *data, size_t length)
{
   if (data == NULL || length <= 0 || this->settings == NULL)
   {
      return 0;
   }
   if (!ofile.is_open())
   {
      std::cerr << "FileSink write: file not open" << std::endl
                << std::flush;
      return 0;
   }
   if (!ofile.good())
   {
      std::cerr << "FileSink write: file in bad state" << std::endl
                << std::flush;
      return 0;
   }

   /* Write according to the output format specified in the INI/Settings */
   if (this->settings->sinkformat == Binary)
   {
      ofile.write(data, length);
   }
   else
   {
      ofile << "// --------------------- DATA SET TIMESTAMP xxxx ---------------------" << std::endl;
      scfloat_t *src = reinterpret_cast<scfloat_t *>(data);
      size_t points = length / (2 * sizeof(scfloat_t));

      ofile << "// FFT bins 0.." << (points - 1) << " + Nyquist" << std::endl;
      scfloat_t mrNyquist = *(src + 1);
      for (size_t i = 0; i < points; i++)
      {
         // ofile << i << "\t\t" << *(src+0) << std::endl; // Re only
         ofile << *(src + 0) << std::endl; // Re only
         src += 2;
      }
      // ofile << len << "\t\t" << mrNyquist << std::endl;
      ofile << mrNyquist << std::endl;
   }

   ofile.flush();
   return length;
}

/**
 * Close the resource
 * @return int Returns 0 on success
 */
int FileSink::close()
{
   ofile.close();
   return 0;
}