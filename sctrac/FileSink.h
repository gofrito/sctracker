/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FILESINK_H
#define FILESINK_H

#include "Settings.h"
#include "DataSink.h"
#include "Buffer.h"

#include <string>
#include <iostream>
#include <fstream>

class FileSink : public DataSink
{
public:
   explicit FileSink(sctracker_settings_t *settings)
   {
      this->settings = settings;
      return;
   }
   explicit FileSink(std::string uri) { open(uri); }
   ~FileSink() { close(); }

   /**
    * Open the file
    * @return int Returns 0 on success
    * @param  uri File path
    */
   int open(std::string uri);

   /**
    * Write data into resource
    * @return int Returns the number of bytes written
    * @param  buf Pointer to the buffer to write out.
    */
   size_t write(Buffer *buf);

   /**
    * Write array data into resource
    * @return int Returns the number of bytes written
    * @param  data   Pointer to the data to write out.
    * @param  length The number of bytes to write.
    */
   size_t write(char *data, size_t length);

   /**
    * Close the resource
    * @return int Returns 0 on success
    */
   int close();

private:
   std::ofstream ofile;
   sctracker_settings_t *settings;
};

#endif // FILESINK_H
