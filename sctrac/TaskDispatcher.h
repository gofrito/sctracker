/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TASKDISPATCHER_H
#define TASKDISPATCHER_H

#include "Settings.h"

#include "TaskCore.h"

/**
 * class TaskDispatcher
 * Creates new TaskCore instances and supplies them with data. Result data from all
 * TaskCores may need to be gathered together and combined into one result data
 * set...
 */

class TaskDispatcher
{
public:
   ~TaskDispatcher();
   TaskDispatcher() { return; }

   /**
    * Creates 'numcores' amount of new TaskCore instances.
    * @param  settings  Pointer to task settings
    * @param  inSource  Pointer to input source
    * @param  outSource Pointer to output sink
    */
   void run(sctracker_settings_t &settings);

private:
   TaskCore **cores;
};

#endif // TASKDISPATCHER_H
