/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TASKCORE_H
#define TASKCORE_H

#include "Buffer.h"
#include "Settings.h"
#include "Helpers.h"
#include "DataUnpackerFactory.h"
#include "LogFile.h"

#include <vector>
#include <deque>
#include <string>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cmath>

#include <ipps.h>
#include <ippvm.h>
#include <ippcore.h>
#include <mkl_blas.h>
#include <mkl_lapacke.h>

#include <pthread.h>

// ------------------------------------------------------------------------
//   Processing stage defines
// ------------------------------------------------------------------------

#define STAGE_NONE 0
#define STAGE_RAWDATA 1
#define STAGE_FFTDONE 2
#define STAGE_EXIT 3

// ------------------------------------------------------------------------
//   Filter Settings struct
// ------------------------------------------------------------------------

typedef struct filterstate_tt
{

   int index;       // 0 .. Ntones-1
   Ipp64f fcarrier; // center frequency of the spacecraft in Hz
   Ipp64f foffset;  // offset of desired filter center freq from the carrier freq in Hz

   int Fbin_carr;  // middle DFT bin of the filter
   int Fbin_start; // starting DFT bin of the filter, inclusive
   int Fbin_end;   // ending DFT bin of the filter, inclusive
   bool valid;     // true if filter bins are inside 0..cfg.fft_points-1, false otherwise
   int Npso;       // number of of output points (=cfg.fft_points/fltratio)
   int Npfo;       // number of spectral points to extract to output FFT (=Npso/2 + 1)
   int Npfz;       // number of zero padding in output FFT (=Npso/2 - 1)

   Ipp64f dfi;    // frequency resolution of the input FFT (=1/(cfg.fft_points*dt))
   Ipp64f Tsetup; // filter set-up time (=dt*cfg.fft_points/2)

   Ipp64fc *delayline; // the output delay line for doing the overlapped add of new data

   Ipp64f Pss;  // the decimal-fractional part of the phase shift
   Ipp64fc Ess; // phase shift of the segment evaluated into Ess = exp(i*2pi*Pss)

   DataSink *sink;

} filterstate_t;

// ------------------------------------------------------------------------
//   PLL Lock Settings and Results struct
// ------------------------------------------------------------------------

struct pllstate_tt;
typedef struct pllstate_tt
{

   int jmax;    // bin location of detected peak
   Ipp64f max;  // value of detected peak
   Ipp64f xmax; // parabolically estimated "inter-bin" location of the actual peak

   int Fline;      // bin location of line frequency
   int Fspan;      // the span (in # of bins) in which to compute RMS value around Fline
   int Fvoid;      // half width of line avoidance
   Ipp64f mean;    // mean value inside the window
   Ipp64f rms;     // rms value inside the window
   Ipp64f timetag; // the start time of the integrated spectrum that gave the above values

} pllstate_t;

// ------------------------------------------------------------------------
//   Task Core class for Intel IPP
// ------------------------------------------------------------------------

/**
 * class TaskCore
 * This class does the actual computations for all input data in one
 * Bufferpartition. Output data is placed into another Bufferpartition. For Cell
 * processors, TaskCore will just wrap an SPE process. On other processors,
 * TaskCore contains the actual computation.
 */

class TaskCore
{
public:
   explicit TaskCore(int mrank) { rank = mrank; }

   /**
    * Do all necessary initializations
    * @param  settings  Pointer to the global settings
    */
   void prepare(sctracker_settings_t &settings);

   /**
    * Perform the spectrum computations.
    * @return int      Returns 0 when task was successfully started
    * @param  inbuf    Pointers to raw input Buffer(s)
    * @param  outbuf   Pointers to spectrum output Buffer(s)
    */
   int run(Buffer *inbuf, Buffer *outbuf);

   /**
    * Actual spectrum calculation. Call only from worker thread.
    */
   void doMaths();

   /**
    * Wait for computation to complete.
    * @return int     Returns -1 on failure, or the number >=0 of output
    *                 buffer(s) that contain a complete spectrum
    */
   int join();

   /**
    * Clean up all allocations etc
    * @return int     Returns 0 on success
    */
   int finalize();

   /**
    * Get core rank
    * @return int     Rank
    */
   int getRank() { return rank; }

   /**
    * Reset buffer to 0.0 floats
    * @param buf     Buffer to reset
    */
   void resetBuffer(Buffer *buf);

private:
   sctracker_settings_t cfg;

   pllstate_t pllstate;                    // PLL, frequency tracking
   std::deque<pllstate_t> pllstatehistory; // delay chain containing previous [jmax,max,mean,rms,...] values

   std::vector<filterstate_t> filters; // list of filters for selected tones in the spectrum
   filterstate_t scfilter;             // a "filter" for the S/C signal or main tone that is to be tracked

   int rank;
   Ipp64f *windowfct;
   Ipp64fc *windowfctout;

   DataUnpacker *unpacker;

   Ipp64f *unpacked_data;
   Ipp64f *windowed_data;
   Ipp64f *zeros;

   scint64_t unpacked_first_sample_nr;

   Ipp64fc *phcorrected_data_reim; // unpacked, windowed and phase-corrected data

   Ipp64fc *fft_result_reim;      // output of the FFT and post processing
   Ipp64fc *fft_result_reim_conj; // conjugate

   Ipp64fc *fft_padded_flt; // used for inverse FFT, left side has select bins from FFT right side is zero

   Ipp64fc *fft_accu_reim;     // accumulated complex spectrum
   int num_ffts_computed;      // how many FFTs have been performed for the in-progress integrated spectrum and SC tracking
   int num_spectra_calculated; // how many integrated spectra have been calculated in the current run or call
   int fft_stream_initialized; // 0 for the very first FFT, 1 later

   Ipp64f *Cpp; // detected phase polynomial coeffs
   Ipp64f *Cpm; // input phase model polynomial coeffs

   Ipp64fc spectrum_scaling;

   double total_runtime;
   scint64_t total_ffts;

   Ipp64f *zeroToNFFT; // preinitialized to {0,1,2,...,cfg.fft_points-1}

   Ipp64f *vtemp[8]; // some fft_points-sized temporary arrays for generic use

   IppsDFTSpec_C_64fc *fftSpecHandle; // forward FFT setup
   Ipp8u *fftWorkbuffer;
   IppsDFTSpec_C_64fc *ifftSpecHandle; // inverse FFT setup
   Ipp8u *ifftWorkbuffer;

   pthread_t wthread;
   bool terminate_worker;
   Buffer *buf_in;
   Buffer *buf_out;

private:
   std::ostream *log;

public:
   pthread_mutex_t mmutex;
   long processing_stage;

   bool shouldTerminate() { return terminate_worker; }

private:

   /**
    * Scales a completed integrated spectrum and writes it to the sink
    */
   void process_completed_spectrum();

   /**
    * Applies the phase correction polynomial to a Nfft sized vector
    * of real input samples 0..Nfft-1. The polynomial for sample# n is
    * f[n] = exp(sign * i*poly[n]), where
    * poly[n] = coeff[2]*xt[n]^2 + coeff[3]*xt[n]^3 + ..., where
    * xt[n] is the sample time or nr from the very start of the stream.
    * The output vector has elements output[n] = sample[n] * f[n].
    *
    * @param input   vector with real-valued samples to phase-correct
    * @param output  output vector for complex phase-corrected samples
    * @param jt      the sample number of the first sample, from start of stream
    * @param coeff   pointer to the coefficients to use in the polynomial
    */
   void correct_sample_phase(Ipp64f *input, Ipp64fc *output, scint64_t jt, Ipp64f *coeffs);

   /**
    * Analyse a given spectrum using Sergei's FindMax()
    * and GetRMS() functions in vectorized form. Updates the
    * classes' pllstate and pllstatehistory structures.
    * @param spectrum the large integrated (real-valued) power spectrum spectrum
    * @param filter   filter specifications that have the carrier signal location
    *
    */
   int analyze_spectrum(Ipp64f *spectrum, filterstate_t &filter);

   /**
    * Update the PLL by computing new phase polynomial coefficients
    * based on the current history of spectrum peaks.
    * TODO: this may not work as expected yet because it should operate on
    * the uncorrected spectrum and not use the already phase-corrected spectrum!
    */
   void update_pll(void);

   /**
    * Helpers to convert around between bin#
    * and frequency (Hz) in the full-range FFT.
    */
   int freqToBin(Ipp64f freq);
   Ipp64f binToFreq(int bin);

   /**
    * Update derived parameters in the filter struct based on the
    * current set of phase poly coefficients.
    * @param filter Reference to filter that should be updated
    */
   void update_filter(filterstate_t &filter);

   /**
    * Show a summary of filter infos for debugging or logging.
    * @param filter Reference to filter that should be shown.
    */
   void summarize_filter(const filterstate_t &filter);

   /**
    * Use a filter struct to filter out a part of the spectrum.
    * that is in fft_result_reim[]. Performs a phase correction
    * step and inverse DFT (~Hilbert xform) and writes results
    * to the DataSink of the filter.
    * @param filter    the filter struct to use
    */
   void apply_filter(filterstate_t &filter);

   /**
    * Fill out a vector with the specified window function.
    * @param output where to place the values
    * @param type   type of window function
    * @param points how many points
    */
   void generate_windowfunction(Ipp64f *output, WindowFunctionType type, int points);
   void generate_complex_windowfunction(Ipp64fc *output, WindowFunctionType type, int points);
};

#endif // TASKCORE_H
