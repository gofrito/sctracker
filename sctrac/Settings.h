/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

#include "Helpers.h"
#include "IniParser.h"
#include "LogFile.h"

#define DSRC_RAW 0
#define DSINK_SPECTRUM 0
#define DSINK_FILTER_A 1
#define DSINK_FILTER_B 2
#define MAX_FILES 3

class Buffer;
class DataSource;
class DataSink;
class Settings;
class LogFile;

typedef double scfloat_t;
typedef u_int64_t scint64_t;
typedef struct _sccomplex_t
{
  scfloat_t re, im;
} sccomplex_t;

enum OutputFormat
{
  Ascii = 0,
  Binary
};
enum InputFormat
{
  Unknown = -1,
  RawSigned,
  RawUnsigned,
  RDF,
  iBOB,
  Maxim,
  Mk5B,
  VDIX,
  VLBA,
  Mark5B,
  VDIF,
  K5
};

// ------------------------------------------------------------------------
//   Output tone config Struct
// ------------------------------------------------------------------------

typedef struct sctracker_tone_tt
{
  int tone_nr;
  scfloat_t dBc;
  scfloat_t offsetHz;
  std::string outfilename;
  DataSink *sink;
} sctracker_tone_t;

// ------------------------------------------------------------------------
//   Settings Struct
// ------------------------------------------------------------------------

typedef struct sctracker_settings_tt
{

  // -- command line / INI : calculation parameters

  size_t fft_points;            // how many points in the FFT (a 2^N number)
  scfloat_t fft_integ_seconds;  // how many seconds of data to integrate into a "dynamic spectrum"
  int fft_overlap_factor;       // how many new samples 1:N to the next FFT input data [S*(N-1)/N old samples | S*1/N new samples]
  int padding_factor;           // padding 2: input array will be padded with the same length of zeros
                                // padding 1: no padding
  int bits_per_sample;          // how many bits per sample (1,2,4,8,16,...)
  bool channelorder_increasing; // how the channels are ordered, channel#0 in MSB or channel#0 in LSB of first byte
  int source_channels;          // how many channels (or frequency bands) are in the data source(s)
  int use_channel;              // which one of the source channel(s) to use in calcs

  int seconds_to_skip; // how much to skip ahead in the input data
  int seconds_to_stop; // how much to stop after in the input data

  int oversamplingfactor;
  scfloat_t bandwidth;    // analog signal bandwidth
  scfloat_t samplingfreq; // sampling frequency of analog signal (oversampling etc)

  WindowFunctionType wfType;    // which window function to use on the input data
  WindowFunctionType wfOutType; // which window function to use on the output data

  int phase_poly_order;       // degree of the phase polynomial, Npf
  signed int phase_poly_sign; // +1 or -1

  scfloat_t filterbandwidth; // the filter bandwidth around the S/C signal

  size_t max_rawbuf_size; // how large buffers to allocate at most in bytes (in INI specified as Megabytes)

  bool write_doubleprecision_data; // type of output data files

  // -- command line / INI : file specifications
  InputFormat sourceformat;
  std::string sourceformat_str;
  std::string source_names[MAX_FILES]; // URIs of sources
  DataSource *sources[MAX_FILES];      // pointers to #sources of input data sources (channels)
  int num_sources;

  std::string basefilename_pattern; // base file name with path and placeholders

  std::string sink_names[MAX_FILES]; // URIs of sinks
  OutputFormat sinkformat;           // output data format
  DataSink *sinks[MAX_FILES];        // pointers to #sinks of output data sinks
  int num_sinks;

  std::string cpp_filename; // file and path of the Cpp phase poly coefficients
  std::string cpm_filename; // same for Cpm coeffs

  std::string foffsets_filename;   // input filename and path to the frequency offsets file that contains the tones to filter out
  std::string toneoutfile_pattern; // pattern for the output files that the tones will be output to

  PhaseCoeffType cppType; // "unit" of the Cpp, Cpm phase coefficients in the phase coeff files

  int phaselock_spectra; // number of integrated spectra needed before PLL tracking starts

  // bool use_channel_a;
  // bool use_channel_b;
  // bool calculate_xpol;

  bool do_SCtracking;        // if false, sctracker outputs only classic spectrometer results
  bool write_output_spectra; // if false do not write scspec file

  // -- internal parameters

  std::string basefilename; // placeholders filled, all output file names
                            // are derived from this (<basename>_scspec.bin, <basename>_tonebinning.txt, etc)

  size_t rawbuf_size;           // how large chunks of raw data per each TaskCore to allocate
  int max_spectra_per_buffer;   // maximum expected number of spectra from one raw buffer processing pass
  int max_buffers_per_spectrum; // the other way round, how many raw bufs are needed at most for one spectrum

  Buffer ***rawbuffers; // pointers to #sources double-buffered [2] buffers
  Buffer **outbuffers;  // pointers to #sources of output buffers - channel N spectra

  scfloat_t *Cpp; // initial detected phase poly coefficients
  scfloat_t *Cpm; // initial modelled phase poly coefficients

  std::vector<sctracker_tone_t> tones; // a list of tones around the S/C carrier; tones are defined in the 'cpp_filename' text file

  size_t totalRAM_rawbufs; // just for statistics
  size_t totalRAM_outbufs;

  // -- "derived" parameters

  int averaged_ffts;            // how many non-overlapped FFTs should be averaged for one result
  int averaged_overlapped_ffts; // how many FFTs the 'averaged_ffts' setting translates to when taking sample overlapping into account

  double rawbytes_per_channelsample; // how many input bytes are needed to get a single sample from the used channel
  size_t raw_fullfft_bytes;          // how many raw bytes each FFT needs as input
  size_t raw_overlap_bytes;          // how many bytes of this are actually new data
  size_t fft_bytes;                  // how many bytes the result of one real-to-complex FFT needs (Ipp64fc * FFT points)

  scfloat_t dt; // time resolution of the samples (=1/samplingfreq)
  scfloat_t df; // frequency resolution of the large FFT in Hz (=sampling frequency/FFT points)

  int pad_array_len;           // how many complex nums in the padding array
  int singlesided_fft_out_len; // output points in the left half of the symmetric real/complex-to-complex spectrum

  int filter_ratio; // the filter ratio, bandwidth to use around the S/C signal / the fraction of single-sideband FFT bins to copy

  int out_points;   // how long the filtered output signal is, counted in FFT bins (Npso)
  scfloat_t out_bw; // the actual bandwidth of the filtered signal (BWo), due to out_points# rounding this way be != cfg.filterbandwidth
  int out_Npfo;
  int out_Npfz;

  // -- text-based output files

  LogFile *logBins;
  LogFile *logIO;
  LogFile *logPhases;
  LogFile *log;
  TeeStream *tlog;

  // -- locking of the settings

  bool commit_done;

} sctracker_settings_t;

// ------------------------------------------------------------------------
//   Settings Class
// ------------------------------------------------------------------------

/**
 * class Settings
 *
 * This class handles a sctracker_settings_t struct, such
 * as initializing it to defaults, reading settings from
 * an INI file, and so on.
 *
 */

class Settings
{
public:
  Settings() { setDefaults(); }
  ~Settings() { return; }

public:
  /**
   * Return a reference to the contained settings.
   */
  sctracker_settings_t &getSettings() { return settings; }

  /**
   * Read settings from an INI file.
   * @param  path  Path and name of the INI file
   */
  void loadINI(std::string path);

  /**
   * Finalize all settings and allocate data input source buffers.
   */
  void commit();

  /**
   * Show a summary of the settings.
   */
  static void summarize(sctracker_settings_t &set);

private:
  void setDefaults(void);

  /**
   * Recalculate some internal parameters.
   */
  void updateDerivedParams();

  /**************************************************************************/

  /**
   * Load phase poly coefficients from a file.
   * The cfg.phase_poly_order must already be set.
   */
  int load_phase_poly_coeffs(std::string file, scfloat_t *out);

  /**
   * Load and parse the list of frequnecy offsets used in multi-tone extraction.
   * Currently the format is simple. All lines that start with '//' are comments.
   * Other lines contain three numbers separated by tabs or spaces. The numbers
   * are, in order: tone number (0...k), frequency offset from the S/C carrier in Hz,
   * and signal level versus S/C carrier in dBc
   *
   */
  int load_foffsets(std::string file);

  /**
   * Return a file name created from 'pattern' that contains the infos from 'tone'.
   */
  std::string tone_to_filename(std::string pattern, sctracker_tone_t &tone);

  /**
   * Return a file name created from 'pattern' filled out with SCtracker settings.
   */
  std::string cfg_to_filename(std::string pattern, sctracker_settings_t &set);

private:
  sctracker_settings_t settings;
};

#endif // SETTINGS_H
