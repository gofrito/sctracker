/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DATAUNPACKERS_H
#define DATAUNPACKERS_H

#include "Settings.h"
#include "DataUnpacker.h"
#include <mark5access.h>
#include <ippcore.h>

class SignedUnpacker : public DataUnpacker
{
public:
  explicit SignedUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class UnsignedUnpacker : public DataUnpacker
{
public:
  explicit UnsignedUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class TwoBitUnpacker : public DataUnpacker
{
public:
  explicit TwoBitUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class TwoBitSinglechannelUnpacker : public DataUnpacker
{
public:
  explicit TwoBitSinglechannelUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class Mk5BUnpacker : public DataUnpacker
{
public:
  explicit Mk5BUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class VDIXUnpacker : public DataUnpacker
{
public:
  explicit VDIXUnpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class K5Unpacker : public DataUnpacker
{
public:
  explicit K5Unpacker(sctracker_settings_t const *settings) { cfg = settings; }
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
};

class VLBAUnpacker : public DataUnpacker
{
public:
  explicit VLBAUnpacker(sctracker_settings_t const *);
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
  struct mark5_stream *ms;
  Ipp32f **allchannels;
};

class MarkIVUnpacker : public DataUnpacker
{
public:
  explicit MarkIVUnpacker(sctracker_settings_t const *);
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
  int fanout;
  struct mark5_stream *ms;
  Ipp32f **allchannels;
};

class Mark5BUnpacker : public DataUnpacker
{
public:
  explicit Mark5BUnpacker(sctracker_settings_t const *);
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
  int fanout;
  struct mark5_stream *ms;
  Ipp32f **allchannels;
};

class VDIFUnpacker : public DataUnpacker
{
public:
  explicit VDIFUnpacker(sctracker_settings_t const *);
  size_t extract_samples(char const *const, Ipp64f *, const size_t, const int) const;
  static bool canHandleConfig(sctracker_settings_t const *settings);

protected:
  sctracker_settings_t const *cfg;
  int fanout; // Fanout is not used
  struct mark5_stream *ms;
  Ipp32f **allchannels;
};

#endif // DATAUNPACKERS_H
