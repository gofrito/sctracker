/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Settings.h"
#include "DataUnpackers.h"

#include <cstdlib>
#include <iostream>

#include <ippcore.h>
#include <ipps.h>
#include <mark5access.h>

#if defined(__APPLE__)
#include <stdlib.h>
#include "MacPort.h"
#else
#include <malloc.h>
#endif

using std::cerr;
using std::endl;

///////////////////////////////////////////////////////////////////////////
// Simple 8-bit, 16-bit and 32-bit data unpacking
// - RawSigned (data type like Ettus)
// - RawUnsigned
///////////////////////////////////////////////////////////////////////////

/**
 * Signed data to float unpacking.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t SignedUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    size_t rc = 0, smp = 0;

    if (cfg->bits_per_sample == 8)
    {
        /* 8-bit data */
        signed char const *src8 = reinterpret_cast<signed char const *>(src);
        src8 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src8 + off * cfg->source_channels));
            }
            src8 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
    }
    else if (cfg->bits_per_sample == 16)
    {
        /* 16-bit data */
        short const *src16 = reinterpret_cast<short const *>(src);
        src16 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src16 + off * cfg->source_channels));
            }
            src16 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
    }
    else if (cfg->bits_per_sample == 32)
    {
        /* 32-bit data */
        float const *src32 = reinterpret_cast<float const *>(src);
        src32 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src32 + off * cfg->source_channels));
            }
            src32 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
    }
    rc = smp;
    if (rc != count)
    {
        cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
    }
    return rc;
}

bool SignedUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return (settings->bits_per_sample == 8 || settings->bits_per_sample == 16 || settings->bits_per_sample == 32);
}

/**
 * Unsigned data to float unpacking. Data is shifted so that 128 becomes 0.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t UnsignedUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    size_t rc = 0, smp = 0;

    if (cfg->bits_per_sample == 8)
    {
        /* 8-bit data */
        unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src);
        src8 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src8 + off * cfg->source_channels)) - 128.0f;
            }
            src8 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
    }
    else
    {
        /* 16-bit data */
        unsigned short const *src16 = reinterpret_cast<unsigned short const *>(src);
        src16 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src16 + off * cfg->source_channels)) - 32768.0f;
            }
            src16 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
    }
    rc = smp;
    if (rc != count)
    {
        cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
    }
    return rc;
}

bool UnsignedUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return (settings->bits_per_sample == 8 || settings->bits_per_sample == 16);
}

///////////////////////////////////////////////////////////////////////////
// 2-bit formats
///////////////////////////////////////////////////////////////////////////

/**
 * Raw 2-bit data to float unpacker. Handles data layouts where samples from
 * every channel are packed together and are samples always in the same order.
 * Assumes samples are packed in network byte order.
 *
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..channels-1
 * @return how many samples were unpacked
 */
size_t TwoBitUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    static Ipp64f precooked_LUT[256];
    static int precook_done = 0;
    size_t rc;

    // Data format:
    //   4 channels :  8-bit : [MSB .. LSB] : [ch3msb ch3lsb ch2msb ch2lsb ch1msb ch1lsb ch0msb ch0lsb]
    //   8 channels : 16-bit : [MSB .. LSB] : [byte0] [byte1] : big endian : [ch7msb ch7lsb .. ch0msb ch0lsb ]

    /* build lookup */
    if (!precook_done)
    {
        unsigned char shift;
        if (cfg->channelorder_increasing)
        {
            shift = (2 * (channel % 4)); // 0,2,4,6
        }
        else
        {
            shift = 6 - (2 * (channel % 4)); // 6,4,2,0
        }
        unsigned char mask = static_cast<unsigned char>(3) << shift;
        const Ipp64f map_rev[4] = {+1.0, -1.0, +3.3359, -3.3359}; // {m,s} : 00,01,10,11 : direct sign/mag bits
        for (int i = 0; i < 256; i++)
        {
            unsigned char s = (i & mask) >> shift;
            precooked_LUT[i] = map_rev[s];
        }
        precook_done = 1;
    }

    /* unpack using the lookup table */
    int step = cfg->source_channels / 4; // 1 or 2
    unsigned char const *src8;
    if (cfg->channelorder_increasing)
    {
        src8 = reinterpret_cast<unsigned char const *>(src) + channel / 4;
    }
    else
    {
        src8 = reinterpret_cast<unsigned char const *>(src) + (step - 1) - channel / 4; // src + 0..1 - 0..1
    }
    size_t smp;
    for (smp = 0; smp < count;)
    {
        const int unroll_factor = 8;
        for (char off = 0; off < unroll_factor; off++)
        {
            dst[smp + off] = precooked_LUT[*(src8 + off * step)];
        }
        src8 += unroll_factor * step;
        smp += unroll_factor;
    }
    rc = smp;
    if (rc != count)
    {
        cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
    }
    return rc;
}

bool TwoBitUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2) && ((settings->source_channels % 4) == 0));
}

/**
 * Raw 2-bit data to float unpacker. Handles the data layout where we have only
 * a single channel. The oldest sample is in the MSB, the newest in the LSB.
 *
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, always 0 (argument kept for API compatibility)
 * @return how many samples were unpacked
 */
size_t TwoBitSinglechannelUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    static Ipp64f precooked_LUT[256][4];
    static int precook_done = 0;
    size_t rc;

    // Data format:
    //   1 channel  :  2-bit : [MSB]        : [ch1s1 ch1s2 ch1s3 ch1s4]
    //    assert ( (count % 4) == 0); // number of samples must divide by 4

    /* build lookup */
    if (!precook_done)
    {
        const Ipp64f map_rev[4] = {+1.0, -1.0, +3.3359, -3.3359}; // {m,s} : 00,01,10,11 : direct sign/mag bits
        for (int byte = 0; byte < 256; byte++)
        {
            for (unsigned char samplenr = 0; samplenr < 4; samplenr++)
            {
                unsigned char s = (byte >> ((3 - samplenr) * 2)) & 3;
                precooked_LUT[byte][samplenr] = map_rev[s];
            }
        }
        precook_done = 1;
        // TODO(mars): for cross-pol or two input files, each file should be assigned its own unpacker! otherwise have to rebuild the LUT for every unpack!
    }

    /* unpack using the lookup table */
    unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src);
    size_t smp;
    for (smp = 0; smp < count;)
    {
        const int unroll_factor = 4;
        for (char off = 0; off < unroll_factor; off++)
        {
            unsigned char val = *(src8 + off);
            dst[smp + 4 * off + 0] = precooked_LUT[val][0];
            dst[smp + 4 * off + 1] = precooked_LUT[val][1];
            dst[smp + 4 * off + 2] = precooked_LUT[val][2];
            dst[smp + 4 * off + 3] = precooked_LUT[val][3];
        }
        src8 += unroll_factor;
        smp += 4 * unroll_factor;
    }
    rc = smp;
    if (rc != count)
    {
        cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
    }
    return rc;
}

bool TwoBitSinglechannelUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2) && (settings->source_channels == 1));
}

/**
 * Mark5B four or eight channel 2-bit data raw data to float unpacker.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t Mk5BUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    static Ipp64f precooked_LUT[256];
    static int precook_done = 0;
    size_t rc = 0, smp = 0;
    const Ipp64f map_rev[4] = {+1.0, -1.0, +3.3359, -3.3359}; // {m,s} : 00,01,10,11 : reversed sign/mag bits

    /* 2 channels */
    if (cfg->source_channels == 2)
    {
        if (!precook_done)
        {
            unsigned char shift = 2 * (channel % 2); // 0,2
            unsigned char shift2 = 2 * ((channel + 2) % 2);
            unsigned char mask = static_cast<unsigned char>(3) << shift;
            for (int i = 0; i < 128; i++)
            {
                unsigned char s1 = (i & mask) >> shift;
                unsigned char s2 = (i & mask) >> shift2;
                precooked_LUT[2 * i] = map_rev[s1];
                precooked_LUT[2 * i + 1] = map_rev[s2];
            }
            precook_done = 1;
        }

        /* unpack using the lookup table */
        unsigned short const *src16 = reinterpret_cast<unsigned short const *>(src) + channel / 8; // byte offset
        int step = cfg->source_channels / 8;                                                       // 1 or 2
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (short off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = precooked_LUT[*(src16 + off * step)];
            }
            src16 += unroll_factor * step;
            smp += unroll_factor;
        }
    }

    /* 4, 8, 12, 16 channels */
    else if (cfg->source_channels == 4 || cfg->source_channels == 8 || cfg->source_channels == 12 || cfg->source_channels == 16)
    {
        if (!precook_done)
        {
            unsigned char shift = 2 * (channel % 4); // 0,2,4,6
            unsigned char mask = static_cast<unsigned char>(3) << shift;
            for (int i = 0; i < 256; i++)
            {
                unsigned char s = (i & mask) >> shift;
                precooked_LUT[i] = map_rev[s];
            }
            precook_done = 1;
        }

        /* unpack using the lookup table */
        unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src) + channel / 4; // byte offset
        int step = cfg->source_channels / 4;                                                    // 0, 1, 2, 3
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = precooked_LUT[*(src8 + off * step)];
            }
            src8 += unroll_factor * step;
            smp += unroll_factor;
        }
    }

    rc = smp;
    if (rc != count)
    {
        cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
    }
    return rc;
}

bool Mk5BUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2) && ((settings->source_channels % 2) == 0));
}

/**
 * VDIX 1, 2, 4, 8, 12, 16 channel 2-bit or 8-bit data raw data to float unpacker.
 * It uses own made lookup table
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t VDIXUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    if (cfg->bits_per_sample == 2)
    {
        static Ipp64f precooked_LUT[256];
        static Ipp64f precooked_LUT1[256][4];
        static int precook_done = 0;
        size_t rc, smp = 0;
        const Ipp64f map[4] = {-3.3359, -1.0, +1.0, +3.3359}; // {m,s} : 00,01,10,11 : reversed sign/mag bits

        /* 1 channel */
        if (cfg->source_channels == 1)
        {
            if (!precook_done)
            {
                for (int byte = 0; byte < 256; byte++)
                {
                    for (unsigned char samplenr = 0; samplenr < 4; samplenr++)
                    {
                        unsigned char s = byte >> (2 * samplenr) & 0x03;
                        precooked_LUT1[byte][samplenr] = map[s];
                    }
                }
                precook_done = 1;
            }

            /* unpack using the lookup table */
            unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src);

            for (smp = 0; smp < count;)
            {
                const int unroll_factor = 4;
                for (char off = 0; off < unroll_factor; off++)
                {
                    unsigned char val = *(src8 + off);

                    dst[smp + 4 * off + 0] = precooked_LUT1[val][0];
                    dst[smp + 4 * off + 1] = precooked_LUT1[val][1];
                    dst[smp + 4 * off + 2] = precooked_LUT1[val][2];
                    dst[smp + 4 * off + 3] = precooked_LUT1[val][3];
                }
                src8 += unroll_factor;
                smp += 4 * unroll_factor;
            }
        }
        /* 2 channels */
        else if (cfg->source_channels == 2)
        {
            if (!precook_done)
            {
                for (int byte = 0; byte < 256; byte++)
                {
                    for (unsigned char samplenr = 0; samplenr < 4; samplenr++)
                    {
                        unsigned char s = byte >> (2 * samplenr) & 0x03;
                        precooked_LUT1[byte][samplenr] = map[s];
                    }
                }
                precook_done = 1;
            }

            unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src);

            for (smp = 0; smp < count;)
            {
                const int unroll_factor = 4;
                for (char off = 0; off < unroll_factor; off++)
                {
                    // Consider that the MSB wih LSB with VDIF data
                    unsigned char val = *(src8 + off);

                    // Unpack samples
                    dst[smp + 2 * off] = precooked_LUT1[val][channel + 0];
                    dst[smp + 2 * off + 1] = precooked_LUT1[val][channel + 2];
                }
                src8 += unroll_factor;
                smp += 2 * unroll_factor;
            }
        } /* 4, 8, 12, 16 channels */
        else if (cfg->source_channels == 4 || cfg->source_channels == 8 || cfg->source_channels == 12 || cfg->source_channels == 16)
        {
            if (!precook_done)
            {
                unsigned char shift = 2 * (channel % 4);
                unsigned char mask = static_cast<unsigned char>(3) << shift;
                for (int i = 0; i < 256; i++)
                {
                    unsigned char s = (i & mask) >> shift;
                    precooked_LUT[i] = map[s];
                }
                precook_done = 1;
            }

            /* unpack using the lookup table */
            unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src) + channel / 4; // byte offset
            int step = cfg->source_channels / 4;                                                    // 1, 2, 3 or 4
            for (smp = 0; smp < count;)
            {
                const int unroll_factor = 8;
                for (char off = 0; off < unroll_factor; off++)
                {
                    dst[smp + off] = precooked_LUT[*(src8 + off * step)];
                }
                src8 += unroll_factor * step;
                smp += unroll_factor;
            }
        }

        rc = smp;
        if (rc != count)
        {
            cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
        }
        return rc;
    }
    else
    {
        /* 8-bit data 1-4 channels */
        size_t rc, smp = 0;
        signed char const *src8 = reinterpret_cast<signed char const *>(src);
        src8 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp64f>(*(src8 + off * cfg->source_channels));
            }
            src8 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
        rc = smp;
        if (rc != count)
        {
            cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
        }
        return rc;
    }
}

bool VDIXUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2 && settings->source_channels % 1) or (settings->bits_per_sample == 8 && settings->source_channels % 1) == 0);
}

/**
 * K5 1, 2, 4, 8, 12, 16 channel 2-bit or 8-bit data raw data to float unpacker.
 * It uses own made lookup table
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t K5Unpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    if (cfg->bits_per_sample == 2)
    {
        //static Ipp32f precooked_LUT[256];
        static Ipp64f precooked_LUT1[256][4];
        static int precook_done = 0;
        size_t rc, smp = 0;
        const Ipp64f map[4] = {-3.3359, -1.0, +1.0, +3.3359}; // {m,s} : 00,01,10,11 : reversed sign/mag bits

        /* 1 channel */
        if (cfg->source_channels == 1)
        {
            if (!precook_done)
            {
                for (int byte = 0; byte < 256; byte++)
                {
                    for (unsigned char samplenr = 0; samplenr < 4; samplenr++)
                    {
                        unsigned char s = byte >> (2 * samplenr) & 0x03;
                        precooked_LUT1[byte][samplenr] = map[s];
                    }
                }
                precook_done = 1;
            }

            /* unpack using the lookup table */
            unsigned char const *src8 = reinterpret_cast<unsigned char const *>(src);

            for (smp = 0; smp < count;)
            {
                const int unroll_factor = 4;
                for (char off = 0; off < unroll_factor; off++)
                {
                    unsigned char val = *(src8 + off);

                    // Data format (K5 style):
                    // 1 channel  :  2-bit : [LSB]        : [ch1s4 ch1s3 ch1s2 ch1s1]
                    dst[smp + 16 - 4 * (off + 1) + 0] = precooked_LUT1[val][0];
                    dst[smp + 16 - 4 * (off + 1) + 1] = precooked_LUT1[val][1];
                    dst[smp + 16 - 4 * (off + 1) + 2] = precooked_LUT1[val][2];
                    dst[smp + 16 - 4 * (off + 1) + 3] = precooked_LUT1[val][3];
                }
                src8 += unroll_factor;
                smp += 4 * unroll_factor;
            }
            //cerr << "MEGA DUMP: " << dst[0] << ":" << dst[1] << ":" << dst[2] << ":" << dst[3] << ":" << dst[4] << ":" << dst[5] << ":" << dst[6] << ":" << dst[7] <<endl;
            //cerr << "           " << dst[8] << ":" << dst[9] << ":" << dst[10] << ":" << dst[11] << ":" << dst[12] << ":" << dst[13] << ":" << dst[14] << ":" << dst[15] <<endl;
        }

        rc = smp;
        if (rc != count)
        {
            cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
        }
        return rc;
    }
    else
    {
        /* 8-bit data 1-4 channels */
        size_t rc, smp = 0;
        signed char const *src8 = reinterpret_cast<signed char const *>(src);
        src8 += channel;
        for (smp = 0; smp < count;)
        {
            const int unroll_factor = 8;
            for (char off = 0; off < unroll_factor; off++)
            {
                dst[smp + off] = static_cast<Ipp32f>(*(src8 + off * cfg->source_channels));
            }
            src8 += unroll_factor * cfg->source_channels;
            smp += unroll_factor;
        }
        rc = smp;
        if (rc != count)
        {
            cerr << "Unpacked " << rc << " samples instead of " << count << " because of better unroll factor. " << endl;
        }
        return rc;
    }
}

bool K5Unpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2 && settings->source_channels % 1) or (settings->bits_per_sample == 8 && settings->source_channels % 1) == 0);
}


///////////////////////////////////////////////////////////////////////////
// Perverted formats
///////////////////////////////////////////////////////////////////////////

/**
 * VLBA raw data to float unpacker.
 * Initialize things required by the mark5access library.
 */
VLBAUnpacker::VLBAUnpacker(sctracker_settings_t const *settings)
{
    /* Preps */
    cfg = settings;
    size_t count = cfg->fft_points;

    /* <mark5acess/docs/UserGuide>
     * 3.2.3.1 struct mark5_stream_generic *new_mark5_stream_unpacker(int noheaders)
     * This function makes a new pseudo-stream.  "noheaders" should be set to 0
     * if the data to be later unpacked is in its original form and should be 1
     * if the data to be later unpacked has its headers stripped.  When used in
     * conjunction with mark5_stream_copy, "noheaders" should be set to 1.
     */
    ms = new_mark5_stream(
        new_mark5_stream_unpacker(/*noheaders*/ 1),
        new_mark5_format_generic_from_string(cfg->sourceformat_str.c_str()));
    if (!ms)
    {
        cerr << "Mark5access library did not like the format '" << cfg->sourceformat_str << "'" << endl;
        ms = NULL; // and then crash
    }

    /* mark5access limitation : all channels are unpacked, need lots of memory */
    allchannels = (Ipp32f **)malloc(ms->nchan * sizeof(float *));
    cerr << "VLBAUnpacker allocating " << (ms->nchan * count * sizeof(float)) / 1024.0 << "kB of mark5access buffers." << endl;
    for (int ch = 0; ch < ms->nchan; ch++)
    {
        allchannels[ch] = (float *)memalign(128, count * sizeof(float));
        if (allchannels[ch] == NULL)
        {
            cerr << "Malloc of " << (count * sizeof(float)) << " bytes for temp channel " << ch
                 << " out of " << ms->nchan << " channels failed!" << endl;
        }
    }
}

/**
 * VLBA raw data to float unpacker.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t VLBAUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    if (channel >= ms->nchan)
    {
        cerr << "Source has only " << ms->nchan << " channels but settings request channel " << (channel + 1) << endl;
    }

    /* Unpack and copy the desired channel */
    const size_t mark5access_max_size = 131072; // use 2^17 (~2^18 is the maximum due to some mark5access bug/limitation)
    size_t left = count;
    char const *mk5src = src;
    Ipp64f *mk5dst = dst;
    while (left > 0)
    {
        size_t to_unpack = std::min(mark5access_max_size, left);
        mark5_unpack(ms, (void *)mk5src, allchannels, to_unpack);
        ippsConvert_32f64f(allchannels[channel % ms->nchan], mk5dst, to_unpack);
        mk5src += to_unpack;
        mk5dst += to_unpack;
        left -= to_unpack;
    }

    /* VLBA is non data replacement so we don't need to add randomness to "header" locations */
    return count;
}

bool VLBAUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2 || settings->bits_per_sample == 1) && ((settings->source_channels % 2) == 0));
}

/**
 * Mark5B raw data to float unpacker. Mark5B mode uses the mark5access library
 * Initialize things required by the mark5access library.
 */
Mark5BUnpacker::Mark5BUnpacker(sctracker_settings_t const *settings)
{
    /* Preps */
    cfg = settings;
    size_t count = cfg->fft_points;
    fanout = 1;

    ms = new_mark5_stream(
        new_mark5_stream_unpacker(/*noheaders*/ 1),
        new_mark5_format_generic_from_string(cfg->sourceformat_str.c_str()));
    if (!ms)
    {
        cerr << "Mark5access library did not like the format '" << cfg->sourceformat_str << "'" << endl;
        ms = NULL; // and then crash
    }

    /* mark5access limitation : all channels are unpacked, need lots of memory */
    allchannels = (Ipp32f **)malloc(ms->nchan * sizeof(float *));
    cerr << "Mark5BUnpacker allocating " << (ms->nchan * count * sizeof(float)) / 1024.0 << "kB of mark5access buffers." << endl;
    for (int ch = 0; ch < ms->nchan; ch++)
    {
        allchannels[ch] = (float *)memalign(128, count * sizeof(float));
        if (allchannels[ch] == NULL)
        {
            cerr << "Malloc of " << (count * sizeof(float)) << " bytes for temp channel " << ch
                 << " out of " << ms->nchan << " channels failed!" << endl;
        }
    }

    /* fill the output arrays with noise */
    for (int ch = 0; ch < ms->nchan; ch++)
    {
        for (size_t n = 0; n < count; n++)
        {
            allchannels[ch][n] = float(2 * (std::rand() % 2) - 1); // <= +-1.0f, TODO: use something better
        }
    }
}

/**
 * Mark5B raw data to float unpacker.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t Mark5BUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    if (channel >= ms->nchan)
    {
        cerr << "Source has only " << ms->nchan << " channels but settings request channel " << (channel + 1) << endl;
    }

    mark5_unpack(ms, (void *)src, allchannels, count);
    ippsConvert_32f64f(allchannels[channel % ms->nchan], dst, count);
    size_t headeroffset = 16;
    for (size_t n = 0; n < headeroffset; n++)
    {
        dst[n] = 3.3359f * float(2 * (std::rand() % 2) - 1);
    }
    return count;
}

bool Mark5BUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2 || settings->bits_per_sample == 1) && ((settings->source_channels % 2) == 0));
}

/**
 * VDIF raw data to float unpacker. VDIF mode uses the mark5access library
 * Initialize things required by the mark5access library.
 */
VDIFUnpacker::VDIFUnpacker(sctracker_settings_t const *settings)
{
    /* Preps */
    cfg = settings;
    size_t count = cfg->fft_points;

    ms = new_mark5_stream_absorb(
        new_mark5_stream_unpacker(0),
        new_mark5_format_generic_from_string(cfg->sourceformat_str.c_str()));
    if (!ms)
    {
        cerr << "Mark5access library did not like the format '" << cfg->sourceformat_str << "'" << endl;
        ms = NULL; // and then crash
    }

    /* mark5access limitation : all channels are unpacked, need lots of memory */
    allchannels = (Ipp32f **)malloc(ms->nchan * sizeof(float *));
    cerr << "VDIFUnpacker allocating " << (ms->nchan * count * sizeof(float)) / 1024.0 << "kB of mark5access buffers." << endl;
    for (int ch = 0; ch < ms->nchan; ch++)
    {
        allchannels[ch] = (float *)memalign(128, count * sizeof(float));
        if (allchannels[ch] == NULL)
        {
            cerr << "Malloc of " << (count * sizeof(float)) << " bytes for temp channel " << ch
                 << " out of " << ms->nchan << " channels failed!" << endl;
        }
    }
}

/**
 * VDIF raw data to float unpacker.
 * @param src     raw input data
 * @param dst     destination of unpacked floatingpoint data
 * @param count   how many samples to unpack
 * @param channel the channel to use, 0..nchannels-1
 * @return how many samples were unpacked
 */
size_t VDIFUnpacker::extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const
{
    if (channel >= ms->nchan)
    {
        cerr << "Source has only " << ms->nchan << " channels but settings request channel " << (channel + 1) << endl;
    }

    /* Unpack and copy the desired channel */
    mark5_unpack(ms, (void *)src, allchannels, count);
    ippsConvert_32f64f(allchannels[channel % ms->nchan], dst, count);

    return count;
}

bool VDIFUnpacker::canHandleConfig(sctracker_settings_t const *settings)
{
    return ((settings->bits_per_sample == 2 || settings->bits_per_sample == 1));
}
