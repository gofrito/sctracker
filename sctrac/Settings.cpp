/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Settings.h"
#include "Helpers.h"
#include "DataSource.h"
#include "DataSink.h"
#include "LogFile.h"

#include <cmath>
#include <iostream>
using std::cerr;
using std::endl;
#include <fstream>
#include <sstream>

// ------------------------------------------------------------------------
//  Platform
// ------------------------------------------------------------------------

#define DEFAULT_N_FFT 16384
#define DEFAULT_I_FFT 8
#define DEFAULT_OVERLAP 2
#define MAXIMUM_RAW_BUF_SIZE (PLATFORM_MAX_RAW_BUF_SIZE_MB * 1024 * 1024)

#include "TaskCore.h"
#define PLATFORM_MAX_RAW_BUF_SIZE_MB 1 * 64

// ------------------------------------------------------------------------
//  Class methods
// ------------------------------------------------------------------------

/**
 * Fill out the settings struct with default values.
 */
void Settings::setDefaults(void)
{
   /* Set default options */
   settings.fft_points = DEFAULT_N_FFT;
   settings.averaged_ffts = DEFAULT_I_FFT;
   settings.fft_overlap_factor = DEFAULT_OVERLAP;
   settings.bits_per_sample = 8;
   settings.channelorder_increasing = true;
   settings.source_channels = 1;
   settings.use_channel = 0;
   settings.padding_factor = 1;
   settings.bandwidth = 8e6;
   settings.oversamplingfactor = 1;
   settings.samplingfreq = 2.0 * settings.oversamplingfactor * settings.bandwidth;
   settings.seconds_to_skip = 0;
   settings.seconds_to_stop = 0;
   settings.wfType = Cosine2;
   settings.wfOutType = Cosine2;
   settings.sourceformat_str = std::string("RawSigned");
   settings.sinkformat = Binary;
   settings.phase_poly_order = 3;
   settings.phase_poly_sign = +1;
   settings.num_sources = 0;
   settings.num_sinks = 0;
   settings.cpp_filename = std::string("");
   settings.cpm_filename = std::string("");
   settings.foffsets_filename = std::string("");
   settings.basefilename_pattern = std::string("ProjDate_StationID_Instrument_ScanNo_\%fftpoints\%_\%integrtime\%_\%channel\%");
   settings.toneoutfile_pattern = std::string("_tone\%tonenr\%_atOffset_\%offsethz\%Hz.bin");
   settings.Cpp = new scfloat_t[settings.phase_poly_order + 1];
   settings.Cpm = new scfloat_t[settings.phase_poly_order + 1];
   settings.cppType = SampleBased;
   settings.filterbandwidth = 4000; // Hz
   settings.max_rawbuf_size = MAXIMUM_RAW_BUF_SIZE;
   settings.do_SCtracking = true;
   settings.write_output_spectra = false;
   settings.phaselock_spectra = 5;
   settings.commit_done = false;
   settings.write_doubleprecision_data = false;
   settings.tones.clear();

   /* Update */
   this->updateDerivedParams();

   return;
}

/**
 * Read settings from an INI file.
 * @param  path  Path and name of the INI file
 */
void Settings::loadINI(std::string path)
{
   IniParser iniParser(path);
   std::string keyval;

   if (!iniParser.selectSection("Settings"))
   {
      cerr << "Error: ERROR: No section called [Settings] in the INI file " << path << endl
           << endl;
      exit(-1);
   }

   /* Read input and output filename settings */
   if (iniParser.getKeyValue("BaseFilename", settings.basefilename_pattern))
   {
      settings.num_sinks = 1;
   }
   if (iniParser.getKeyValue("InputSource", settings.source_names[DSRC_RAW]))
   {
      settings.num_sources = 1;
   }
   iniParser.getKeyValue("ToneOutPattern", settings.toneoutfile_pattern);

   /* Read settings for output file data format */
   iniParser.getKeyValue("WriteDoublePrecision", settings.write_doubleprecision_data);

   /* Read FFT settings */
   iniParser.getKeyValue("FFTpoints", settings.fft_points);
   iniParser.getKeyValue("FFTIntegrationTimeSec", settings.fft_integ_seconds);
   iniParser.getKeyValue("FFToverlapFactor", settings.fft_overlap_factor);
   iniParser.getKeyValue("PaddingFactor", settings.padding_factor);
   if (iniParser.getKeyValue("WindowType", keyval))
   {
      settings.wfType = Helpers::parseWindowingString(keyval);
   }
   if (iniParser.getKeyValue("WindowOutType", keyval))
   {
      settings.wfOutType = Helpers::parseWindowingString(keyval);
   }

   /* Read hardware settings */
   // Currently 1-core only: iniParser.getKeyValue("NumCores", settings.num_cores);
   if (iniParser.getKeyValue("MaxRawBufSize", keyval))
   {
      settings.max_rawbuf_size = atoi(keyval.c_str()) * 1024 * 1024;
   }

   /* Read sampling and data settings */
   iniParser.getKeyValue("BandwidthHz", settings.bandwidth);
   iniParser.getKeyValue("OverSamplingFactor", settings.oversamplingfactor);
   iniParser.getKeyValue("BitsPerSample", settings.bits_per_sample);
   iniParser.getKeyValue("ChannelOrderIncreasing", settings.channelorder_increasing);
   iniParser.getKeyValue("SourceFormat", settings.sourceformat_str);
   iniParser.getKeyValue("SourceSkipSeconds", settings.seconds_to_skip);
   iniParser.getKeyValue("SourceStopSeconds", settings.seconds_to_stop);
   iniParser.getKeyValue("SourceChannels", settings.source_channels);
   if (iniParser.getKeyValue("UseChannel", settings.use_channel))
   {
      settings.use_channel -= 1;
   }

   /* Spacecraft tracking, external a priori polynomials */
   iniParser.getKeyValue("FilterBandwidthHz", settings.filterbandwidth);
   iniParser.getKeyValue("PhasePolySign", settings.phase_poly_sign);
   iniParser.getKeyValue("PhasePolyOrder", settings.phase_poly_order);
   iniParser.getKeyValue("PhasePolyCpmFile", settings.cpm_filename);
   iniParser.getKeyValue("PhasePolyCppFile", settings.cpp_filename);
   iniParser.getKeyValue("ToneOffsetsFile", settings.foffsets_filename);
   iniParser.getKeyValue("PhaseLockSpectra", settings.phaselock_spectra);
   iniParser.getKeyValue("DoSpacecraftTracking", settings.do_SCtracking);
   iniParser.getKeyValue("WriteOutputSpectra", settings.write_output_spectra);
   if (iniParser.getKeyValue("PhasePolyCoeffType", keyval))
   {
      settings.cppType = Helpers::parsePhaseCoeffString(keyval);
   }

   /* Refresh parameters */
   updateDerivedParams();
}

/**
 * Finalize all settings and allocate data input source buffers
 */
void Settings::commit(void)
{
   /* Commit can be done only once */
   if (settings.commit_done)
   {
      cerr << "Warning: Settings::commit() already called once!" << endl;
      return;
   }
   settings.commit_done = true;

   /* Make sure derived params are valid */
   this->updateDerivedParams();

   /* Check that we have sources/files */
   if (settings.num_sources < 1)
   {
      cerr << "No input source specified in the INI settings (needs InputSource=...)!" << endl
           << endl;
      exit(-1);
   }
   if (settings.num_sinks < 1)
   {
      cerr << "No output file(s) specified in the INI settings (needs BaseFilename=...)!" << endl
           << endl;
      exit(-1);
   }

   /* Determine the best raw buffer size:
    * - if entire integrated spectrum data won't fit, cram as many FFTs into buffer as possible
    * - if entire integrated spectrum data does fit, try to cram more integrated spectra into buffer
    * - the TaskCore is assumed to do streamed processing so we don't have to care about
    *   any of the overlapped input data processing, just use full FFT input bytecount here
    */
   size_t tent_rawbuf_size = (settings.raw_fullfft_bytes * settings.averaged_ffts);
   if (tent_rawbuf_size > settings.max_rawbuf_size)
   {
      settings.max_spectra_per_buffer = 0;
      settings.max_buffers_per_spectrum = settings.averaged_ffts;
      tent_rawbuf_size = settings.raw_fullfft_bytes;
      while ((2 * tent_rawbuf_size) <= settings.max_rawbuf_size)
      {
         tent_rawbuf_size *= 2;
         settings.max_buffers_per_spectrum /= 2;
      }
   }
   else
   {
      settings.max_spectra_per_buffer = 1;
      settings.max_buffers_per_spectrum = 0;
      while ((2 * tent_rawbuf_size) <= settings.max_rawbuf_size)
      {
         tent_rawbuf_size *= 2;
         settings.max_spectra_per_buffer *= 2;
      }
   }
   settings.rawbuf_size = tent_rawbuf_size;

   /* Create the double-buffered raw input bufs : Buffer*[doublebuffers][sources] */
   settings.totalRAM_rawbufs = 2 * settings.num_sources * settings.rawbuf_size;
   settings.rawbuffers = new Buffer **[2];
   for (int b = 0; b < 2; b++)
   {
      settings.rawbuffers[b] = new Buffer *[settings.num_sources];
      for (int s = 0; s < settings.num_sources; s++)
      {
         settings.rawbuffers[b][s] = new Buffer(settings.rawbuf_size);
      }
   }

   /* Create complex data output buffers : Buffer*[sources] */
   size_t max_space_needed = settings.fft_bytes * std::max(1, settings.max_spectra_per_buffer);
   settings.totalRAM_outbufs = settings.num_sources * max_space_needed;
   settings.outbuffers = new Buffer *[settings.num_sources];
   for (int s = 0; s < settings.num_sources; s++)
   {
      settings.outbuffers[s] = new Buffer(max_space_needed);
      settings.outbuffers[s]->setLength(settings.outbuffers[s]->getAllocated());
   }

   /* Allocate then load phase poly coefficients and change their sign */
   settings.Cpp = new scfloat_t[settings.phase_poly_order + 1];
   settings.Cpm = new scfloat_t[settings.phase_poly_order + 1];
   load_phase_poly_coeffs(settings.cpp_filename, settings.Cpp);
   load_phase_poly_coeffs(settings.cpm_filename, settings.Cpm);

   /* Read the frequency offsets */
   if (settings.do_SCtracking)
   {
      load_foffsets(settings.foffsets_filename);
      if (settings.tones.empty())
      {
         sctracker_tone_t t;
         t.tone_nr = 0;
         t.offsetHz = 0.0;
         t.dBc = 0.0;
         settings.tones.push_back(t);
      }
   }

   /* Prepare the base file names */
   settings.basefilename = cfg_to_filename(settings.basefilename_pattern, settings);
   settings.sink_names[DSINK_SPECTRUM] = settings.basefilename + std::string("_scspec.bin");

   /* Open all log files */
   settings.logBins = new LogFile(settings.basefilename + std::string("_tonebinning.txt"));   // bin infos
   settings.logIO = new LogFile(settings.basefilename + std::string("_starttiming.txt"));     // timing in I/O files
   settings.logPhases = new LogFile(settings.basefilename + std::string("_phasevalues.txt")); // phases for debugging
   settings.log = new LogFile(settings.basefilename + std::string("_runlog.txt"));            // 'tee' copy of all screen output
   settings.tlog = new TeeStream(std::cerr, settings.log->stream());

   /* Open all input sources */
   for (int i = 0; (i < settings.num_sources) && (i < MAX_FILES); i++)
   {
      std::string uri = settings.source_names[i];
      settings.sources[i] = DataSource::getDataSource(uri, &settings);
      if (settings.sources[i] == NULL)
      {
         cerr << "Error: problem with input data source '" << uri << "' or format error while opening!" << endl;
         exit(-1);
      }
      if (settings.sources[i]->open(uri))
      {
         cerr << "Error: Could not open input source '" << uri << "'!" << endl;
         exit(-1);
      }
   }

   /* Open all output sinks for spectra */
   for (int i = 0; (i < settings.num_sinks) && (i < MAX_FILES); i++)
   {
      std::string uri = settings.sink_names[i];
      settings.sinks[i] = DataSink::getDataSink(uri, &settings);
      if (settings.sinks[i] == NULL)
      {
         cerr << "Error: Invalid output data sink '" << uri << "'!" << endl;
         exit(-1);
      }
      if (settings.sinks[i]->open(uri))
      {
         cerr << "Could not open output data sink '" << uri << "'!" << endl;
         exit(-1);
      }
   }

   /* Open all output sinks for tones */
   for (unsigned int i = 0; i < settings.tones.size(); i++)
   {
      std::string uri = tone_to_filename(settings.basefilename + settings.toneoutfile_pattern, settings.tones[i]);
      settings.tones[i].outfilename = uri;
      settings.tones[i].sink = DataSink::getDataSink(uri, &settings);
      if (settings.tones[i].sink == NULL)
      {
         cerr << "Error: Invalid filter output sink '" << uri << "'!" << endl;
         exit(-1);
      }
      if (settings.tones[i].sink->open(uri))
      {
         cerr << "Could not open filter output data sink '" << uri << "'!" << endl;
         exit(-1);
      }
   }

   return;
}

/**
 * Show a summary of the settings.
 */
void Settings::summarize(sctracker_settings_t &set)
{

   /* select the output: cerr or tee'ed to cerr and log file */
   std::ostream *os;
   if (set.tlog != NULL)
   {
      os = set.tlog;
   }
   else
   {
      os = &(std::cerr);
   }

   /* normal summary */
   *os << "Input file   : ";
   if (set.num_sources >= 1)
   {
      *os << set.source_names[0] << endl;
   }
   else
   {
      *os << "<still unspecified>" << endl;
   }
   *os << "Data store   : " << set.num_sources << " input sources, " << set.num_sinks << " output sinks" << endl
       << "File format  : " << set.bits_per_sample << " bits/sample, "
       << set.source_channels << " channels, "
       << "channel " << (set.use_channel + 1) << " selected for processing" << endl;
   *os << "Analog info  : " << set.bandwidth / 1e3 << " kHz bandwidth, "
       << set.samplingfreq / 1e3 << " kHz sampling rate"
       << ", dt=" << set.dt << endl;
   *os << "FFT info     : " << set.fft_points << " points, "
       << set.df << " Hz resolution, "
       << set.averaged_ffts << "-fold averaging (" << (set.averaged_ffts * set.fft_points) / set.samplingfreq << "s), "
       << 100.0 * (1.0 - 1.0 / set.fft_overlap_factor) << "% overlap, "
       << set.padding_factor << "-fold padding" << endl;
   *os << "Phase poly   : "
       << "order " << set.phase_poly_order << ", "
       << "sign " << set.phase_poly_sign << ", "
       << "type " << set.cppType << endl;
   *os << "Filter/PLL   : " << set.out_bw << " Hz bandwidth, " << set.out_points << " points including padding, "
       << "initial S/C carrier at " << set.Cpp[1] * set.samplingfreq / (2 * M_PI) << " Hz " << endl;
   if (!set.do_SCtracking)
   {
      *os << "SC tracking  : disabled" << endl;
   }

   if (set.max_buffers_per_spectrum > 0)
   {
      *os << "Buffer use   : 1 averaged spectrum consumes "
          << set.max_buffers_per_spectrum << " raw buffers" << endl;
   }
   if (set.max_spectra_per_buffer > 0)
   {
      *os << "Buffer use:  : 1 raw buffer gives "
          << set.max_spectra_per_buffer << " averaged spectra" << endl;
   }
   *os << "Raw buffers  : " << (set.rawbuf_size * byte_To_kB) << " kB per source" << endl;
   *os << "Raw input buffers       : " << (set.totalRAM_rawbufs * byte_To_kB) << " kB total" << endl;
   *os << "Spectrum result buffers : " << (set.totalRAM_outbufs * byte_To_kB) << " kB total" << endl;

   /* after the summary print-out, if some settings are "wrong", exit! */
   if ((set.fft_points % 8) != 0)
   {
      *os << "Assert: unpack assumption, 8-sample loop unrolling: FAILED" << endl;
      exit(-1);
   }
   if ((set.fft_overlap_factor % 2) != 0 && (set.fft_overlap_factor != 1))
   {
      *os << "Assert: FFT overlap factor is " << set.fft_overlap_factor << ": even 1/(2*N) overlap: FAILED" << endl;
      exit(-1);
   }
   if (set.filter_ratio <= 2)
   {
      *os << "Assert: filter ratio must be larger than 2: FAILED" << endl;
      exit(-1);
   }
   if ((set.out_points % 2) != 0 || set.out_points <= 2)
   {
      *os << "Assert: (FFT points/2)/filterratio = " << set.out_points << " output points: value must be even: FAILED" << endl;
      exit(-1);
   }

   return;
}

/**
 * Recalculate some internal parameters.
 */
void Settings::updateDerivedParams()
{
   /* value range limiting */
   if (settings.padding_factor <= 0)
   {
      settings.padding_factor = 1;
   }
   if (settings.fft_overlap_factor <= 0)
   {
      settings.fft_overlap_factor = 1;
   }
   if (settings.phase_poly_sign >= 0)
   {
      settings.phase_poly_sign = +1;
   }
   else
   {
      settings.phase_poly_sign = -1;
   }

   /* generic params */
   settings.samplingfreq = 2.0 * settings.oversamplingfactor * settings.bandwidth;

   /* large FFT parameters */
   settings.averaged_ffts = int(settings.fft_integ_seconds * settings.samplingfreq / settings.fft_points);
   settings.averaged_overlapped_ffts = settings.fft_overlap_factor * settings.averaged_ffts - (settings.fft_overlap_factor - 1);
   settings.rawbytes_per_channelsample = (settings.bits_per_sample * settings.source_channels) / 8.0;
   settings.raw_fullfft_bytes = int((settings.fft_points * settings.rawbytes_per_channelsample));
   settings.raw_overlap_bytes = int((settings.fft_points * settings.rawbytes_per_channelsample) / settings.fft_overlap_factor);
   settings.fft_bytes = settings.fft_points * 2 * sizeof(scfloat_t);
   settings.pad_array_len = settings.fft_points * (settings.padding_factor - 1);
   settings.dt = 1 / settings.samplingfreq;
   settings.df = settings.samplingfreq / settings.fft_points;

   /* filter and small IFFT parameters */
   settings.filter_ratio = settings.bandwidth / settings.filterbandwidth;
   settings.out_points = settings.fft_points / settings.filter_ratio;
   settings.out_bw = settings.bandwidth / settings.filter_ratio;
   settings.out_Npfo = (settings.out_points / 2) + 1;
   settings.out_Npfz = (settings.out_points / 2) - 1;

   settings.singlesided_fft_out_len = (settings.fft_points * settings.padding_factor) / 2 + 1;

   return;
}

/**
 * Load phase poly coefficients from a file.
 *
 * Requirements:
 * - the cfg.phase_poly_order must be set before calling, *out must have been preallocated
 * - coeff file contains one coefficient per line in ASCII/text using scientific ("-12.34e5" notation)
 * - coefficients are in radians per sample
 */
int Settings::load_phase_poly_coeffs(std::string file, scfloat_t *out)
{
   std::ifstream f;
   f.open(file.c_str());
   if (!f)
   {
      cerr << "ERROR: Could not open phase coeff file " << file << endl
           << endl;
      exit(-1);
   }

   /* read all values, apply sign to coeffs C2, C3, C4,... */
   for (int i = 0; i <= settings.phase_poly_order; i++)
   {
      f >> out[i];
      if ((settings.phase_poly_sign < 0) && (i >= 2))
      {
         out[i] = -out[i];
      }

      /* check that we could read enough */
      if (!f.good())
      {
         cerr << "ERROR: Could find only " << i << " instead of " << (settings.phase_poly_order + 1)
              << " poly coefficients in file " << file << endl
              << endl;
         exit(-1);
      }
   }

   f.close();
   return 0;
}

/**
 * Load and parse the list of frequnecy offsets used in multi-tone extraction.
 * Currently the format is simple. All lines that start with '//' are comments.
 * Other lines contain three numbers separated by tabs or spaces. The numbers
 * are, in order: tone number (0...k), frequency offset from the S/C carrier in Hz,
 * and signal level versus S/C carrier in dBc
 *
 */
int Settings::load_foffsets(std::string file)
{
   std::ifstream f(file.c_str(), std::ios::in);
   if (!f || !f.is_open())
   {
      cerr << "ERROR: Could not open frequency offsets file " << file << endl
           << endl;
      exit(-1);
   }

   std::string line;
   while (getline(f, line))
   {

      sctracker_tone_t tone;
      std::string::size_type idx;

      /* skip any lines with comments and any lines that are just blank */
      idx = line.find_first_not_of(" \t\n\v");
      if (idx == std::string::npos)
      {
         continue;
      }
      if (line[idx] == '/' || line[idx] == '#')
      {
         continue;
      }

      /* parse the numbers */
      std::stringstream ss(line, std::ios::in);
      ss >> tone.tone_nr;
      ss >> tone.offsetHz;
      ss >> tone.dBc;
      settings.tones.push_back(tone);
   }
   return 0;
}

/**
 * Return a file name created from 'pattern' that contains the infos from 'tone'.
 */
std::string Settings::tone_to_filename(std::string pattern, sctracker_tone_t &tone)
{
   typedef struct keypair_tt
   {
      std::string match;
      std::string replacement;
   } keypair_t;

   std::string::size_type idx;

   keypair_t keys[3] = {{std::string("\%tonenr\%"), std::to_string((tone.tone_nr))},
                        {std::string("\%offsethz\%"), std::to_string(static_cast<int>(tone.offsetHz))},
                        {std::string("\%dbc\%"), std::to_string(static_cast<int>(tone.dBc))}};

   // remove spaces in the filename
   std::replace(pattern.begin(), pattern.end(), ' ', '_');
   std::string filename = pattern;

   // replace found keys with actual strings
   for (size_t i = 0; i < sizeof(keys) / sizeof(keypair_t); i++)
   {
      while (1)
      {
         idx = filename.find(keys[i].match, 0);
         if (idx == std::string::npos)
            break;
         filename = filename.replace(idx, keys[i].match.length(), keys[i].replacement);
      }
   }

   return filename;
}

/**
 * Return a file name created from 'pattern' filled out with SCtracker settings.
 */
std::string Settings::cfg_to_filename(std::string pattern, sctracker_settings_t &set)
{
   typedef struct keypair_tt
   {
      std::string match;
      std::string replacement;
   } keypair_t;

   std::string::size_type idx;
   keypair_t keys[4] = {{std::string("\%fftpoints\%"), std::to_string(static_cast<int>(set.fft_points))},
                        {std::string("\%integrtime\%"), std::to_string(static_cast<int>(set.fft_integ_seconds))},
                        {std::string("\%channel\%"), std::to_string(set.use_channel + 1)},
                        {std::string("\%filterbandwidth\%"), std::to_string(static_cast<int>(set.filterbandwidth))}};

   // remove spaces in the filename
   std::replace(pattern.begin(), pattern.end(), ' ', '_');
   std::string filename = pattern;

   // replace found keys with actual strings
   for (size_t i = 0; i < sizeof(keys) / sizeof(keypair_t); i++)
   {
      while (1)
      {
         idx = filename.find(keys[i].match, 0);
         if (idx == std::string::npos)
            break;
         filename = filename.replace(idx, keys[i].match.length(), keys[i].replacement);
      }
   }

   return filename;
}
