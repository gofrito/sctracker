/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "TaskDispatcher.h"
#include "Helpers.h"
#include "DataSource.h"
#include "DataSink.h"

#include <string>
#include <iostream>

using std::cerr;
using std::endl;

/**
 * Creates 'numsources' amount of new TaskCore instances.
 * @param  set   Settings and the input and output buffers
 */
void TaskDispatcher::run(sctracker_settings_t &set)
{
   int dblbuf_nr = 0;
   bool gotEOF = false;

   int bufs_processed = 0;
   int prev_bufs_processed = 0;

   double times[4];

   /* Pre-fill the first buffers */
   cerr << "TaskDispatcher buffer prefill..." << endl;
   for (int s = 0; s < set.num_sources; s++)
   {
      (set.sources[s])->read(set.rawbuffers[dblbuf_nr][s]);
   }

   /* Initialize the cores */
   cerr << "TaskDispatcher init TaskCores..." << endl;
   this->cores = new TaskCore *[set.num_sources];
   for (int s = 0; s < set.num_sources; s++)
   {
      cerr << s << " ";
      cores[s] = new TaskCore(s);
      cores[s]->prepare(set);
   }
   cerr << endl;

   /* Process all available sample data -- allocate [raw]=>[core0][core1]...[coreN] */
   cerr << "TaskDispatcher processing all input chunks... " << endl;
   times[0] = Helpers::getSysSeconds();
   times[2] = times[0];
   do
   {
      /* start background processing of the current buffers */
      for (int s = 0; s < set.num_sources; s++)
      {
         cores[s]->run(set.rawbuffers[dblbuf_nr][s], set.outbuffers[s]);
         bufs_processed++;
      }

      /* pre-fill the next raw input buffers */
      for (int s = 0; s < set.num_sources; s++)
      {
         (set.sources[s])->read(set.rawbuffers[(dblbuf_nr + 1) % 2][s]);
      }

      /* wait for calculations to complete and write out results on the go */
      for (int s = 0; s < set.num_sources; s++)
      {
         cores[s]->join();
      }

      /* continue with the next buffered data */
      dblbuf_nr = (dblbuf_nr + 1) % 2;

      /* combined check for EOF on all of the input sources */
      for (int s = 0; s < set.num_sources; s++)
      {
         if ((set.sources[s])->eof())
         {
            gotEOF = true;
         }
      }

      /* throughput statistics */
      if (1)
      {
         times[3] = times[2];
         times[2] = Helpers::getSysSeconds();
         double dT = times[2] - times[3];
         int newbufs = bufs_processed - prev_bufs_processed;
         double fracspecs = 1.0;
         if (set.max_buffers_per_spectrum > 0)
         {
            fracspecs = double(newbufs) / set.max_buffers_per_spectrum;
         }
         else
         {
            fracspecs = double(newbufs) * set.max_spectra_per_buffer;
         }
         cerr << "TaskDispatcher: processed " << newbufs << " new buffers, "
              << bufs_processed << " in total, "
              << " time " << dT << "s"
              << " rate " << fracspecs * (set.fft_integ_seconds / dT) << "x realtime"
              << endl;
         prev_bufs_processed = bufs_processed;
      }

   } while (!gotEOF || bufs_processed == 144);
   times[1] = Helpers::getSysSeconds();
   cerr << endl;

   /* clean up */
   for (int s = 0; s < set.num_sources; s++)
   {
      cores[s]->finalize();
      set.sources[s]->close();
      set.sinks[s]->close();
   }

   cerr << "TaskDispatcher completed, time delta " << (times[1] - times[0]) << "s, "
        << bufs_processed << " raw buffers processed. " << endl;
   return;
}