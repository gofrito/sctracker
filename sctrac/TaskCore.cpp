/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/* Spectrum calculations for the Intel Performance Primitives v5.3 series
 * Set your enviromnent variable 'IPP_PATH' to /opt/intel/ipp/...etc/
 *
 **************************************************************************/

#include "TaskCore.h"
#include "DataSink.h"
#include "LogFile.h"

#include <cmath>
#include <iostream>
using std::cerr;
using std::endl;
using std::flush;

#include <assert.h>
#include <string.h>
#include <memory.h>

#if defined(__APPLE__)
#include <stdlib.h>
#include "MacPort.h"
#else
#include <malloc.h>
#endif

void *taskcore_worker(void *p);

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
//   V E C T O R   A N D   M A T R I X   D E B U G   P R I N T O U T
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

void print_vec_64f(Ipp64f *v, int n)
{
   cerr << " { ";
   for (int i = 0; i < n; i++)
   {
      cerr << v[i] << " ";
   }
   cerr << "}" << endl;
}

void print_sqma_64f(const char *name, Ipp64f *mat, int N)
{
   cerr << name << " = [ ";
   for (int ii = 0; ii < N; ii++)
   {
      for (int jj = 0; jj < N; jj++)
      {
         cerr << mat[jj + ii * N] << "  ";
      }
      cerr << " ; ";
   }
   cerr << " ]; " << endl;
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
//   T A S K   M A N A G I N G
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

/**
 * Do all necessary initializations
 * @param  settings  Pointer to the global settings
 */
void TaskCore::prepare(sctracker_settings_t &settings)
{
   /* make copy of settings */
   this->cfg = settings;

   /* some nonpretty checks */
   assert(cfg.fft_bytes == (sizeof(Ipp64fc) * cfg.fft_points));

   ippInit();

   /* prepare log files */
   cfg.logPhases->log("# Logging started");
   cfg.logPhases->stream() << std::endl;
   cfg.logBins->log("# ToneNr StartBin StopBin StartFreq StopFreq");
   cfg.logBins->stream() << std::fixed << std::endl;
   this->log = cfg.tlog;

   /* get a new raw data unpacker object */
   this->unpacker = DataUnpackerFactory::getDataUnpacker(&settings);
   if (this->unpacker == NULL)
   {
      *log << "No suitable data unpacker found!" << endl;
      return;
   }

   /* prepare buffers and counters */
   this->windowfct = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2));          // real window function
   this->unpacked_data = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2));      // real unpacked data (data re-use for overlap)
   this->windowed_data = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2));      // real windowed data prior to phase correction
   this->zeroToNFFT = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2));         // real data, {0,1,...,fft_points-1}
   this->zeros = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2));              // real data, {0,0,...,0}
   this->phcorrected_data_reim = static_cast<Ipp64fc *>(memalign(128, cfg.fft_bytes)); // complex data
   this->fft_result_reim = static_cast<Ipp64fc *>(memalign(128, cfg.fft_bytes));
   this->fft_accu_reim = static_cast<Ipp64fc *>(memalign(128, cfg.fft_bytes));
   this->fft_result_reim_conj = static_cast<Ipp64fc *>(memalign(128, cfg.fft_bytes));
   this->fft_padded_flt = static_cast<Ipp64fc *>(memalign(128, cfg.out_points * sizeof(Ipp64fc)));
   this->windowfctout = static_cast<Ipp64fc *>(memalign(128, cfg.out_points * sizeof(Ipp64fc)));
   for (int i = 0; i < 8; i++)
   {
      this->vtemp[i] = static_cast<Ipp64f *>(memalign(128, cfg.fft_bytes / 2)); // real data, temps for generic use
   }

   /* check the allocations */
   if (!windowfct || !unpacked_data || !windowed_data || !zeros)
   {
      cerr << "Could not allocate sample arrays" << endl;
      exit(-1);
   }
   for (int i = 0; i < 8; i++)
   {
      if (!vtemp[i])
      {
         *log << "Could not allocate temp arrays" << endl;
         exit(-1);
      }
   }
   if (!phcorrected_data_reim || !fft_result_reim || !fft_accu_reim || !fft_result_reim_conj || !fft_padded_flt)
   {
      *log << "Could not allocate complex arrays" << endl;
      exit(-1);
   }

   /* reset internal state */
   this->processing_stage = STAGE_NONE;
   this->total_runtime = 0.0;
   this->total_ffts = 0;
   this->unpacked_first_sample_nr = 0;
   this->buf_in = NULL;
   this->buf_out = NULL;
   this->num_ffts_computed = 0;
   this->num_spectra_calculated = 0;
   this->fft_stream_initialized = 0;
   ippsZero_64fc(fft_accu_reim, cfg.fft_points);
   ippsZero_64fc(fft_padded_flt, cfg.out_points);
   ippsZero_64f(unpacked_data, cfg.fft_points);
   ippsZero_64f(zeros, cfg.fft_points);

   /* prepare windowing functions */
   generate_windowfunction(windowfct, cfg.wfType, cfg.fft_points);
   generate_complex_windowfunction(windowfctout, cfg.wfOutType, cfg.out_points);

   /* FFT setup */
   int fftSpecSize = 0;
   int fftInitSize = 0;
   int fftWorkbufferSize = 0;
   ippsDFTGetSize_C_64fc(static_cast<int>(cfg.fft_points), IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, &fftSpecSize, &fftInitSize, &fftWorkbufferSize);
   fftSpecHandle = reinterpret_cast<IppsDFTSpec_C_64fc *>(ippsMalloc_8u(fftSpecSize));
   Ipp8u *fftInitHandle = ippsMalloc_8u(fftInitSize);
   fftWorkbuffer = static_cast<Ipp8u *>(memalign(128, fftWorkbufferSize));
   ippsDFTInit_C_64fc(static_cast<int>(cfg.fft_points), IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, fftSpecHandle, fftInitHandle);
   ippsFree(fftInitHandle);

   /* Inverse FFT setup for shorter data */
   int ifftSpecSize = 0;
   int ifftInitSize = 0;
   int ifftWorkbufferSize = 0;
   ippsDFTGetSize_C_64fc(cfg.out_points, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, &ifftSpecSize, &ifftInitSize, &ifftWorkbufferSize);
   ifftSpecHandle = reinterpret_cast<IppsDFTSpec_C_64fc *>(ippsMalloc_8u(ifftSpecSize));
   Ipp8u *ifftInitHandle = ippsMalloc_8u(ifftInitSize);
   ifftWorkbuffer = static_cast<Ipp8u *>(memalign(128, ifftWorkbufferSize));
   ippsDFTInit_C_64fc(cfg.out_points, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, ifftSpecHandle, ifftInitHandle);
   ippsFree(ifftInitHandle);

   /* prepare the accumulated FFT normalization factor, various help vectors*/
   spectrum_scaling.re = 1.0 / cfg.averaged_overlapped_ffts;
   spectrum_scaling.im = 0.0;

   /* make a copy of the initial phase coeffs */
   this->Cpp = new Ipp64f[cfg.phase_poly_order + 1];
   this->Cpm = new Ipp64f[cfg.phase_poly_order + 1];
   for (int i = 0; i <= cfg.phase_poly_order; i++)
   {
      this->Cpp[i] = cfg.Cpp[i];
      this->Cpm[i] = cfg.Cpm[i];
   }

   /* initialize helper vectors for correct_sample_phase() */
   for (size_t i = 0; i < cfg.fft_points; i++)
   {
      if (cfg.cppType == SampleBased)
      {
         this->zeroToNFFT[i] = Ipp64f(i);
      }
      else
      {
         this->zeroToNFFT[i] = Ipp64f(i) * cfg.dt;
      }
   }

   /* for PLL tracking, create a separate SC/tone filter struct at 0 Hz offset */
   scfilter.index = 0;
   scfilter.foffset = 0;
   scfilter.sink = NULL;
   scfilter.delayline = NULL;
   update_filter(scfilter);

   /* for general filtering, create and initialize the list of other filter structs */
   filters.clear();
   for (unsigned int i = 0; i < cfg.tones.size(); i++)
   {
      filterstate_t fs;
      int Npts = cfg.out_points + cfg.out_points / cfg.fft_overlap_factor;
      fs.index = i;
      fs.foffset = cfg.tones[i].offsetHz;
      fs.sink = cfg.tones[i].sink;
      fs.delayline = static_cast<Ipp64fc *>(memalign(128, Npts * sizeof(Ipp64fc)));
      if (!fs.delayline)
      {
         *log << "Could not allocate filter " << i << " output delay line!" << endl;
         exit(-1);
      }
      ippsZero_64fc(fs.delayline, Npts);
      update_filter(fs);
      summarize_filter(fs);
      filters.push_back(fs);
   }

   /* initialize PLL structure */
   pllstate.Fline = filters[0].Fbin_carr;
   pllstate.Fspan = (cfg.out_Npfo - 1) / 2;
   pllstate.Fvoid = pllstate.Fspan / 8; // TODO(mars): find better value!
   pllstatehistory.clear();

   /* init mutexeds and start the worker thread */
   terminate_worker = false;
   pthread_mutex_init(&mmutex, NULL);
   pthread_mutex_lock(&mmutex);
   pthread_create(&wthread, NULL, taskcore_worker, (void *)this);

   return;
}

/**
 * Perform the spectrum computations.
 * @return int      Returns 0 when task was successfully started
 * @param  inbuf    Pointers to raw input Buffer(s)
 * @param  outbuf   Pointers to spectrum output Buffer(s)
 */
int TaskCore::run(Buffer *inbuf, Buffer *outbuf)
{
   terminate_worker = false;
   this->buf_in = inbuf;
   this->buf_out = outbuf;
   this->processing_stage = STAGE_RAWDATA;
   this->num_spectra_calculated = 0;
   pthread_mutex_unlock(&mmutex);
   return 0;
}

/**
 * Wait for computation to complete.
 * @return int     Returns -1 on failure, or the number >=0 of output
 *                 buffer(s) that contain a complete spectrum
 */
int TaskCore::join()
{
   pthread_mutex_lock(&mmutex);
   while (processing_stage != STAGE_FFTDONE)
   {
      pthread_mutex_unlock(&mmutex);
      sched_yield();
      pthread_mutex_lock(&mmutex);
   }
   processing_stage = STAGE_NONE;
   return this->num_spectra_calculated;
}

/**
 * Clean up all allocations etc
 * @return int     Returns 0 on success
 */
int TaskCore::finalize()
{
   /* process final completed spectrum */
   if (cfg.fft_overlap_factor > 1)
   {
      process_completed_spectrum();
   }

   terminate_worker = true;
   this->processing_stage = STAGE_NONE;
   while (processing_stage != STAGE_EXIT)
   {
      pthread_mutex_unlock(&mmutex);
      sched_yield();
      pthread_mutex_lock(&mmutex);
   }
   pthread_mutex_unlock(&mmutex);

   pthread_join(wthread, NULL);
   pthread_mutex_destroy(&mmutex);

   *log << "IPP core " << rank << " completed: " << total_runtime << "s total internal calculation time, " << total_ffts << " FFTs" << endl
        << flush;

   ippsFree(fftSpecHandle);
   free(fftWorkbuffer);
   ippsFree(ifftSpecHandle);
   free(ifftWorkbuffer);

   free(windowfct);
   free(unpacked_data);
   free(windowed_data);
   free(zeros);
   free(phcorrected_data_reim);
   free(fft_accu_reim);
   free(fft_result_reim);
   // Line commented out for memory error 20.03.2018 - enabled 2022
   free(fft_result_reim_conj);
   free(fft_padded_flt);
   return 0;
}

/**
 * Worker thread. When woken up by a TaskCore object (TaskCore
 * releases a mutex), calls back to the TaskCore spectrum calc
 * function.
 * @param  p   Pointer to the TaskCore that created the thread
 */

void *taskcore_worker(void *p)
{
   TaskCore *host = static_cast<TaskCore *>(p);

   while (1)
   {

      /* grab mutex i.e. wait until there is data */
      pthread_mutex_lock(&host->mmutex);

      /* check for exiting */
      if (host->shouldTerminate())
      {
         host->processing_stage = STAGE_EXIT;
         pthread_mutex_unlock(&host->mmutex);
         break;
      }

      /* do the maths */
      if (host->processing_stage == STAGE_RAWDATA)
      {
         host->doMaths();
         host->processing_stage = STAGE_FFTDONE;
      }

      /* let main program continue */
      pthread_mutex_unlock(&host->mmutex);
   }
   pthread_exit((void *)0);
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
//   T H E   L I T T L E   M A T H S
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

/**
 * Helpers to convert around between bin#
 * and frequency (Hz) in the full-range FFT.
 */
int TaskCore::freqToBin(Ipp64f freq)
{
   return int(0.5 + freq / cfg.df);
}

Ipp64f TaskCore::binToFreq(int bin)
{
   return Ipp64f(bin * cfg.df);
}

/**
 * Reset buffer to 0.0 floats
 * @param buf     Buffer to reset
 */
void TaskCore::resetBuffer(Buffer *buf)
{
   ippsZero_64f(reinterpret_cast<Ipp64f *>(buf->getData()), buf->getAllocated() / sizeof(Ipp64f));
}

/**
 * Update derived parameters in the filter struct based on the
 * current set of phase poly coefficients.
 */
void TaskCore::update_filter(filterstate_t &filter)
{
   /* calculate filter location */
   filter.fcarrier = this->Cpp[1] * cfg.samplingfreq / (2 * M_PI); // Hz carrier, from INI/Config and-or tracking PLL
   filter.fcarrier += filter.foffset;                              // Hz offset, from INI/Config, constant
   filter.Fbin_carr = freqToBin(filter.fcarrier);                  // bin#
   filter.Fbin_start = filter.Fbin_carr - (cfg.out_Npfo - 1) / 2;
   filter.Fbin_end = filter.Fbin_carr + (cfg.out_Npfo - 1) / 2;

   /* check if the filter is outside boundaries */
   if (filter.Fbin_start < 0 || filter.Fbin_end >= static_cast<int>(cfg.fft_points))
   {
      filter.valid = false;
   }
   else
   {
      filter.valid = true;
   }

   /* calculate phase shift correction factor of the segment */
   Ipp64f Pss_0 = binToFreq(filter.Fbin_start) * (cfg.fft_points / 2) * cfg.dt;
   filter.Pss = Pss_0 - floor(Pss_0);
   filter.Ess.re = cos(2 * M_PI * filter.Pss);
   filter.Ess.im = sin(2 * M_PI * filter.Pss);
}

/**
 * Show a summary of filter infos for debugging or logging.
 * @param filter Reference to filter that should be shown.
 */
void TaskCore::summarize_filter(const filterstate_t &filter)
{
   Ipp64f Pss_0 = binToFreq(filter.Fbin_start) * (cfg.fft_points / 2) * cfg.dt;
   *log << "Core " << this->rank << " filter infos\t: fc=" << filter.fcarrier << " Hz, "
        << "bins " << filter.Fbin_start << ".." << filter.Fbin_end << " inclusive ("
        << binToFreq(filter.Fbin_start) << ".." << binToFreq(filter.Fbin_end) << " Hz), "
        << "Pss_0=" << Pss_0 << ", fractional Pss=" << filter.Pss << ", "
        << "Ess=" << filter.Ess.re << " + i " << filter.Ess.im << endl;
   cfg.logBins->stream() << filter.index << " " << filter.Fbin_start << " " << filter.Fbin_end << " "
                         << binToFreq(filter.Fbin_start) << " " << binToFreq(filter.Fbin_end) << endl;
}

/**
 * Fill out a vector with the specified window function.
 * @param output where to place the values
 * @param type   type of window function
 * @param points how many points
 */
void TaskCore::generate_windowfunction(Ipp64f *output, WindowFunctionType type, int points)
{
   switch (type)
   {
   case None:
      // no windowing: currently implemented as *1.0 rectangular windowing...
      ippsSet_64f(1, output, points);
      break;
   case Cosine:
      for (int i = 0; i < points; i++)
      {
         output[i] = cos(M_PI * (Ipp64f(i) - 0.5 * Ipp64f(points) + 0.5) / Ipp64f(points));
      }
      break;
   case Cosine2:
      for (int i = 0; i < points; i++)
      {
         Ipp64f w = cos(M_PI * (Ipp64f(i) - 0.5 * Ipp64f(points) + 0.5) / Ipp64f(points));
         output[i] = w * w;
      }
      break;
   case Hamming:
      ippsSet_64f(1, output, points);
      ippsWinHamming_64f_I(output, points);
      break;
   case Blackman:
      ippsSet_64f(1, output, points);
      ippsWinBlackman_64f_I(output, points, 0.5 /* alpha, hardcoded for now... */);
      break;
   case Hann:
      ippsSet_64f(1, output, points);
      ippsWinHann_64f_I(output, points);
      break;
   default:
      ippsSet_64f(1, output, points);
      *log << "TaskCore: unsupported window function " << type << ", reverting to None." << endl;
      break;
   }
   return;
}
void TaskCore::generate_complex_windowfunction(Ipp64fc *output, WindowFunctionType type, int points)
{

   /* complex WF: first generate real WF */
   Ipp64f *tmp = static_cast<Ipp64f *>(memalign(128, points * sizeof(Ipp64f)));
   if (tmp == NULL)
   {
      *log << "Could not allocate tmp for complex window function." << endl;
      exit(-1);
   }
   generate_windowfunction(tmp, type, points);

   /* copy real WF into real parts of the output WF, set imag parts to zero */
   for (int i = 0; i < points; i++)
   {
      output[i].re = tmp[i];
      output[i].im = 0;
   }
   return;
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
//   T H E   R E A L   M A T H S
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

/**
 * Perform the spectrum computations.
 */
void TaskCore::doMaths()
{
   double times[5];
   long iteration = 0;

   /* TODO: since contiguous S/C tracking needs to go through the data without
    * any left out overlapped FFTs around the e.g. 20s spectrometer spectrum integration
    * times, and S/C tracking can't use threads to compute subparts of integrated spectra,
    * the double-buffering and TaskDispatcher become unnecessary. So in theory we
    * can move the InputSource reading parts into here!
    *
    *   Buffer *buf = new Buffer(cfg.raw_fullfft_bytes);
    *   source->read_overlapped(buf, cfg);
    */
   char *src = buf_in->getData();
   size_t raw_remaining = buf_in->getLength();

   /* assert that there is enough data to do the very first FFT */
   if (!(this->fft_stream_initialized) && (raw_remaining < cfg.raw_fullfft_bytes))
   {
      *log << "Error: not enough raw data to initialize FFTs stream! "
           << "Buffer has " << raw_remaining << ", we need " << cfg.raw_fullfft_bytes << endl;
      exit(-1);
   }

   /* precalculate shifting in of fresh Ipp64f input samples based on FFT overlap factor */
   int mm_new_count = cfg.fft_points / cfg.fft_overlap_factor;
   int mm_old_count = cfg.fft_points - mm_new_count;

   /* start performance timing */
   times[1] = times[2] = times[3] = 0.0;
   times[4] = times[0] = Helpers::getSysSeconds();

   /* calculate full or partial integrated spectrum, do contiguous S/C tracking */
   while (raw_remaining >= cfg.raw_overlap_bytes)
   {
      times[2] = Helpers::getSysSeconds();

      /* consume more raw data: initially unpack full FFT, later only new overlap-factor data */
      if ((this->fft_stream_initialized == 0) || (cfg.fft_overlap_factor == 1))
      {
         // full unpack
         this->unpacker->extract_samples(src, &unpacked_data[0], cfg.fft_points, cfg.use_channel);
         this->fft_stream_initialized = 1;
         raw_remaining -= cfg.raw_fullfft_bytes;
         src += cfg.raw_fullfft_bytes;
      }
      else
      {
         // overlapped unpack, shift old data downwards and append new data
         ippsMove_64f(/*src*/ &unpacked_data[mm_new_count],
                      /*dst*/ &unpacked_data[0], mm_old_count);
         this->unpacker->extract_samples(src, &unpacked_data[mm_old_count], mm_new_count, cfg.use_channel);
         raw_remaining -= cfg.raw_overlap_bytes;
         src += cfg.raw_overlap_bytes;
      }

      times[3] += Helpers::getSysSeconds() - times[2];

      /* window the unpacked data */
      ippsMul_64f(windowfct, unpacked_data, windowed_data, cfg.fft_points);
      /* apply phase correction to the windowed real data and get complex output */
      correct_sample_phase(windowed_data, phcorrected_data_reim, unpacked_first_sample_nr, this->Cpp);

      /* update first_sample# for the next iteration */
      unpacked_first_sample_nr += cfg.fft_points / cfg.fft_overlap_factor;

      /* do the large forward transform */
      ippsDFTFwd_CToC_64fc(phcorrected_data_reim, fft_result_reim, fftSpecHandle, fftWorkbuffer);
      this->num_ffts_computed++;
      this->total_ffts++;

      /* apply the set of tone filters to the current fft_result_reim[] spectrum */
      if (cfg.do_SCtracking)
      {
         for (unsigned int fn = 0; fn < filters.size(); fn++)
         {
            apply_filter(filters[fn]);
         }
      }

      /* accumulate the power spectrum, only positive frequencies of the FFT */
      if (this->num_ffts_computed <= cfg.averaged_overlapped_ffts)
      {
         ippsPowerSpectr_64fc(fft_result_reim, vtemp[0], cfg.singlesided_fft_out_len);
         ippsAdd_64f_I(vtemp[0], reinterpret_cast<Ipp64f *>(fft_accu_reim), cfg.singlesided_fft_out_len);
      }
      else
      {
         // For S/C tracking we have to compute the FFTs, too, that overlap the e.g. 20s integration window
         // boundaries. These integration time boundary-overlapping FFTs should not be included in the
         // integrated spectrum, so we just ignore them here.
         // *log << "Skipping FFT integration: " << cfg.averaged_overlapped_ffts << " required until boundary, now computed " << this->num_ffts_computed << endl;
      }

      /* process a completed integrated spectrum */
      bool gotCompleteSpectrum = (this->num_ffts_computed >= (cfg.averaged_overlapped_ffts + (cfg.fft_overlap_factor - 1)));
      if (gotCompleteSpectrum)
      {
         process_completed_spectrum();

         /* recompute the PLL coefficients and readjust the tones */
         if (cfg.do_SCtracking)
         {

            /* extract new peak frequency location and handle PLL */
            analyze_spectrum(reinterpret_cast<Ipp64f *>(fft_accu_reim), scfilter);
            // update_pll(); -- not used for the time being (and sometimes the ipp matrix invert causes segfault!)

            /* (re)adjust all tones */
            for (unsigned int fn = 0; fn < filters.size(); fn++)
            {
               update_filter(filters[fn]);
            }
         }

         /* reset old results */
         ippsZero_64fc(fft_accu_reim, cfg.fft_points);
         this->num_ffts_computed = 0;
      }

      iteration++;

   } // while(raw bytes remaining)

   /* end performance timing */
   times[1] = Helpers::getSysSeconds();
   total_runtime += (times[1] - times[0]);

   /* partial results are continued on the next doMaths call */
   // ...

   return;
}

/**
* Scales a completed integrated spectrum and writes it to the sink
*/
void TaskCore::process_completed_spectrum()
{
   /* scale */
   size_t nbytes;
   ippsMulC_64f_I(1.0 / cfg.averaged_overlapped_ffts, reinterpret_cast<Ipp64f *>(fft_accu_reim), cfg.singlesided_fft_out_len);

   /* convert double/float and copy to output Buffer */
   if (cfg.write_doubleprecision_data)
   {
      nbytes = cfg.singlesided_fft_out_len * sizeof(Ipp64f);
      memcpy((void *)buf_out->getData(), (void *)fft_accu_reim, nbytes);
   }
   else
   {
      nbytes = cfg.singlesided_fft_out_len * sizeof(Ipp32f);
      ippsConvert_64f32f(reinterpret_cast<Ipp64f *>(fft_accu_reim), reinterpret_cast<Ipp32f *>(this->vtemp[0]), cfg.singlesided_fft_out_len);
      memcpy((void *)buf_out->getData(), (void *)this->vtemp[0], nbytes);
   }
   buf_out->setLength(nbytes);

   /* write full spectra to sink */
   if (cfg.write_output_spectra)
   {
      cfg.sinks[DSINK_SPECTRUM]->write(buf_out); // currently write() is a blocking call, buffercopying actually unnecessary...
   }

   /* statistics and join() output info */
   this->num_spectra_calculated++;
}

/**
 * Applies the phase correction polynomial to a Nfft sized vector
 * of real input samples 0..Nfft-1. The polynomial for sample# n is
 * f[n] = exp(sign * i*poly[n]), where
 * poly[n] = coeff[2]*xt[n]^2 + coeff[3]*xt[n]^3 + ..., where
 * xt[n] is the sample time or nr from the very start of the stream.
 * The output vector has elements output[n] = sample[n] * f[n].
 *
 * @param input   vector with real-valued samples to phase-correct
 * @param output  output vector for complex phase-corrected samples
 * @param jt      the sample number of the first sample, from start of stream
 * @param coeff   pointer to the coefficients to use in the polynomial
 */
void TaskCore::correct_sample_phase(Ipp64f *input, Ipp64fc *output, scint64_t jt, Ipp64f *coeffs)
{
   const int tt_i = 0;
   const int sin_i = 0;
   const int ttN_i = 1;
   const int cos_i = 1;
   const int sum_i = 2;
   const int prd_i = 3;
   bool do_print = false;

   /* Calculate:
    *  poly(n) = C[2]*jt(n)^2 + C[3]*jt(n)^3 + ...
    *  out(n)  = exp(sign * sqrt(-1) * poly[n])
    *    the 'sign' has already been multiplied into C[] coefficients in Settings.cpp
    *
    * First apply polys to calculate the exponent vector, then evaluate poly,
    * then output the evaluated poly result to Ipp64fc* output.
    *
    * To use L1/L2 CPU cache a lot more efficiently, we'll process only
    * an amount of 'stepsize' samples at a time. This way the temporary
    * helper vectors are very short and can stay in the cache.
    *
    */
   if ((jt % 160000000ULL) == 0)
   {
      do_print = true;
   }
   const int stepsize = 512; // you may have to experiment with this value
   for (size_t sn = 0; sn < cfg.fft_points; sn += stepsize)
   {
      int points = std::min(static_cast<size_t>(stepsize), static_cast<size_t>(cfg.fft_points - sn));

      /* initialize tt_i with (first_samplenr + {0,1,2,...}) * dt */
      if (cfg.cppType == SampleBased)
      {
         ippsAddC_64f(zeroToNFFT, Ipp64f(jt + scint64_t(sn)), vtemp[tt_i], points);
      }
      else
      {
         ippsAddC_64f(zeroToNFFT, Ipp64f(jt + scint64_t(sn)) * cfg.dt, vtemp[tt_i], points);
      }

      /* initialize ttN_i with tt_i^2 */
      ippsSqr_64f(vtemp[tt_i], vtemp[ttN_i], points);

      /* compute the exponent value from the series */
      ippsZero_64f(vtemp[sum_i], points);
      ippsSet_64f(coeffs[cfg.phase_poly_order], vtemp[sum_i], points);
      for (int i = (cfg.phase_poly_order - 1); i >= 2; i--)
      {
         ippsMul_64f(vtemp[sum_i], vtemp[tt_i], vtemp[prd_i], points);
         ippsAddC_64f(vtemp[prd_i], coeffs[i], vtemp[sum_i], points);
      }
      ippsMul_64f_I(vtemp[ttN_i], /*srcdest*/ vtemp[sum_i], points);
      if (do_print)
      {
         cfg.logPhases->stream() << vtemp[tt_i][0] << "\t " << std::setprecision(24) << static_cast<double>(*vtemp[sum_i]) << std::endl;
         do_print = false;
      }

      /* evaluate the exponential */
      ippsSinCos_64f_A50(/*src:*/ vtemp[sum_i], /*sin:*/ vtemp[sin_i], /*cos:*/ vtemp[cos_i], points);

      /* apply the phase correction to the real-valued input data */
      ippsMul_64f_I(&input[sn], vtemp[cos_i], points);
      ippsMul_64f_I(&input[sn], vtemp[sin_i], points);

      /* assemble into complex output */
      ippsRealToCplx_64f(/*Re*/ vtemp[cos_i], /*Im*/ vtemp[sin_i], &output[sn], points);
   }

   return;
}

/**
 * Analyse a given spectrum using Sergei's FindMax()
 * and GetRMS() functions in vectorized form. Updates the
 * classes' pllstate and pllstatehistory structures.
 * @param spectrum the large integrated (real-valued) power spectrum spectrum
 * @param filter   filter specifications that have the carrier signal location
 */
int TaskCore::analyze_spectrum(Ipp64f *spectrum, filterstate_t &filter)
{
   int np;

   Ipp64f max;
   int imax;

   /* find Min, Max and their indices within the S/C filter passband */
   np = filter.Fbin_end - filter.Fbin_start;
   if (filter.Fbin_end >= cfg.singlesided_fft_out_len)
   {
      *log << "post_analysis_fftint: passband end " << filter.Fbin_end << " outside FFT lenght!" << endl;
      return -1;
   }
   if (np <= 4)
   {
      *log << "post_analysis_fftint: passband has too few (" << np << ") bins!" << endl;
      return -1;
   }
   ippsMaxIndx_64f(&spectrum[filter.Fbin_start], np, &max, &imax);
   imax = filter.Fbin_start + imax;

   /* inter-bin parabolic estimate 'xmax' for the S/C frequency */
   if (imax == 0)
   {
      imax = 1;
   }
   if (imax == (np - 1))
   {
      imax = np - 2;
   }
   Ipp64f a2 = (spectrum[imax - 1] + spectrum[imax + 1] - 2 * spectrum[imax]) / 2;
   Ipp64f a1 = (spectrum[imax + 1] - spectrum[imax - 1]) / 2;
   Ipp64f djx = -a1 / (2 * a2);
   Ipp64f xmax = Ipp64f(imax) + djx;

   /* compute the mean and the RMS of power around the PLL carrier */
   Ipp64f means[2], stddevs[2], mean, rms;
   int ilowerL = pllstate.Fline - pllstate.Fspan;
   int iupperL = pllstate.Fline + pllstate.Fvoid;
   int span = pllstate.Fspan - pllstate.Fvoid;
   if ((ilowerL <= 0) || ((iupperL + span) >= cfg.singlesided_fft_out_len))
   {
      *log << "post_analysis_fftint: bins around line (" << ilowerL << ".." << (ilowerL + span)
           << " and " << iupperL << ".." << (iupperL + span) << ") fall outside of available bins 0.."
           << (cfg.singlesided_fft_out_len - 1) << endl;
      return -1;
   }
   ippsMeanStdDev_64f(&spectrum[ilowerL], span, &means[0], &stddevs[0]);
   ippsMeanStdDev_64f(&spectrum[iupperL], span, &means[1], &stddevs[1]);
   mean = (means[0] + means[1]) / 2;
   rms = sqrt((stddevs[0] * stddevs[0] + stddevs[1] * stddevs[1]) / 2); // the two stddev's come from different mean's, so this "RMS" is an "estimate"

   /* store our PLL results */
   pllstate.timetag = this->num_spectra_calculated * cfg.fft_integ_seconds;
   pllstate.xmax = xmax;
   pllstate.jmax = imax;
   pllstate.max = max;
   pllstate.mean = mean;
   pllstate.rms = rms;

   /* push new values down into the history delay line */
   pllstatehistory.push_back(pllstate);
   if ((int)pllstatehistory.size() > cfg.phaselock_spectra)
   {
      pllstatehistory.pop_front();
   }

   return 0;
}

/**
 * Use a filter struct to filter out a part of the spectrum.
 * that is in fft_result_reim[]. Performs a phase correction
 * step and inverse DFT (~Hilbert xform) and writes results
 * to the DataSink of the filter.
 * @param filter    the filter struct to use
 */
void TaskCore::apply_filter(filterstate_t &filter)
{
   /* if filter bins are out of bounds (invalid filter), write zero-data */
   if (!filter.valid)
   {
      int Novln = cfg.out_points / cfg.fft_overlap_factor;
      if (cfg.write_doubleprecision_data)
      {
         filter.sink->write(reinterpret_cast<char *>(this->zeros), sizeof(Ipp64fc) * Novln);
      }
      else
      {
         ippsZero_32fc(reinterpret_cast<Ipp32fc *>(zeros), Novln);
         filter.sink->write(reinterpret_cast<char *>(this->zeros), sizeof(Ipp32fc) * Novln);
         ippsZero_64fc(reinterpret_cast<Ipp64fc *>(zeros), Novln);
      }
      return;
   }

   /* filter by copying the bins around the tone or S/C signal */
   ippsCopy_64fc(&fft_result_reim[filter.Fbin_start], fft_padded_flt, cfg.out_Npfo);
   fft_padded_flt[0].re /= 2;
   fft_padded_flt[0].im = 0;
   fft_padded_flt[cfg.out_Npfo - 1].re /= 2;
   fft_padded_flt[cfg.out_Npfo - 1].im = 0;

   /* leave the remaining already zero-padded (see prepare()) end untouched and zero */
   // ippsZero(...);

   /* do the small inverse transform and window the entire result */
   ippsDFTInv_CToC_64fc(fft_padded_flt, reinterpret_cast<Ipp64fc *>(vtemp[0]), ifftSpecHandle, ifftWorkbuffer);
   ippsMul_64fc_I(windowfctout, reinterpret_cast<Ipp64fc *>(vtemp[0]), cfg.out_points);

   /* rotate the timedomain result with complex Ess^(fftnumber=0..inf) */
   filter.Ess.re = cos(2 * M_PI * filter.Pss * (this->total_ffts - 1));
   filter.Ess.im = sin(2 * M_PI * filter.Pss * (this->total_ffts - 1));
   ippsMulC_64fc_I(filter.Ess, reinterpret_cast<Ipp64fc *>(vtemp[0]), cfg.out_points);

   /* shift the old data upper part down and append zeroes */
   int Novln = cfg.out_points / cfg.fft_overlap_factor; // nr# of new points to shift out
   int Novlo = cfg.out_points - Novln;                  // nr# of old points to keep
   ippsMove_64fc(/*src*/ &(filter.delayline[Novln]),
                 /*dst*/ filter.delayline, Novlo);
   ippsZero_64fc(&(filter.delayline[Novlo]), Novln);

   /* do an overlapped-add: sum new data over the downshifted old data */
   ippsAdd_64fc_I(reinterpret_cast<Ipp64fc *>(vtemp[0]), filter.delayline, cfg.out_points);

   /* write out the lower part that contains completed data */
   if (cfg.write_doubleprecision_data)
   {
      filter.sink->write(reinterpret_cast<char *>(filter.delayline), sizeof(Ipp64fc) * Novln);
   }
   else
   {
      // convert complex-double to complex-float, during conversion treat complex as 2 reals
      ippsConvert_64f32f(reinterpret_cast<Ipp64f *>(filter.delayline), reinterpret_cast<Ipp32f *>(this->vtemp[0]), 2 * Novln);

      // write the filtered output tone to disk
      filter.sink->write(reinterpret_cast<char *>(this->vtemp[0]), sizeof(Ipp32fc) * Novln);
   }

   return;
}

/**
 * Update the PLL by computing new phase polynomial coefficients
 * based on the current history of spectrum peaks.
 * TODO: this may not work as expected yet because it should operate on
 * the uncorrected spectrum and not use the already phase-corrected spectrum!
 */
void TaskCore::update_pll(void)
{

   /* enough data for poly fitting? */
   if ((int)pllstatehistory.size() < cfg.phaselock_spectra)
   {
      return;
   }

   /* some shorthands */
   Ipp64f Tspan = cfg.phaselock_spectra * cfg.fft_integ_seconds;
   int polyN = cfg.phase_poly_order + 1;
   int points = cfg.phaselock_spectra;

   /* prepare vectors and matrices */
   Ipp64f *ma_freq = new Ipp64f[polyN * polyN];
   Ipp64f *ma_inv = new Ipp64f[polyN * polyN];
   Ipp64f *ma_buffer = new Ipp64f[polyN * polyN];
   Ipp64f *ma_tmp = new Ipp64f[polyN * polyN];
   Ipp64f *vec_freq = new Ipp64f[polyN];
   Ipp64f *vec_cpp = new Ipp64f[polyN];
   ippsZero_64f(ma_freq, polyN * polyN);
   ippsZero_64f(ma_inv, polyN * polyN);
   ippsZero_64f(ma_buffer, polyN * polyN);
   ippsZero_64f(ma_tmp, polyN * polyN);
   ippsZero_64f(vec_freq, polyN);
   ippsZero_64f(vec_cpp, polyN);

   /* non-simd-vectorized input matrix X vs vector Y initialization */
   for (int jp = 0; jp < polyN; jp++)
   {
      for (int pt = 0; pt < points; pt++)
      {
         pllstate_t &state = pllstatehistory.at(pt);
         vec_freq[jp] += state.xmax * state.rms * pow(state.timetag / Tspan, jp);
      }
      for (int ip = 0; ip < polyN; ip++)
      {
         for (int pt = 0; pt < points; pt++)
         {
            // could compute only ~1/2 due to symmetry...
            pllstate_t &state = pllstatehistory.at(pt);
            ma_freq[ip + jp * polyN] += state.rms * pow(state.timetag / Tspan, jp + ip);
         }
      }
   }

   /* compute the inverse X^-1 of X */

   int ipiv[polyN];
   LAPACKE_dgetrf(LAPACK_ROW_MAJOR, polyN, polyN, ma_freq, polyN, ipiv);
   LAPACKE_dgetri(LAPACK_ROW_MAJOR, polyN, ma_freq, polyN, ipiv);

   /* multiply the X^-1 with Y to get coefficients Vandermonde style */
   const char *op = "T";
   double alpha = 1.0, beta = 2.0;
   dgemv(op, &polyN, &polyN, &alpha, ma_inv, &polyN, vec_freq, &polyN, &beta, vec_cpp, &polyN);

   /* rescale the result into phase poly coeffient form */
   for (int jp = 0; jp < polyN; jp++)
   {
      this->Cpm[jp] = 2.0 * M_PI * vec_cpp[jp] / ((jp + 1) * pow(Tspan, jp) * pow(cfg.samplingfreq, jp + 1)); // in Hz per sec
   }

   return;
}