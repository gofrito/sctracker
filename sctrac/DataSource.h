/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DATASOURCE_H
#define DATASOURCE_H

#include "Buffer.h"
#include "Settings.h"

#include <string>

class DataSource
{
   friend class FileSource;
   friend class UdpSource;
   friend class VSIBSource;

public:
   virtual ~DataSource() {}

   /**
    * Open the resource
    * @return int Returns 0 on success
    * @param  uri The URL or path or other identifier for the resource location
    */
   virtual int open(std::string uri) = 0;

   /**
    * Try to fill the entire buffer with new data and return the number of bytes actually read
    * @return int    Returns the amount of bytes read
    * @param  buf    Pointer to Buffer to fill out
    */
   virtual int read(Buffer *buf) { return 0; }

   /**
    * Tries to fill the entire buffer with new data and return the number of bytes actually read.
    * This read function will first seek backwards by (Nfft - Nfft/overlap) points and then read
    * the entire buffer full of data.
    * @return int    Returns number of bytes read
    * @param  buf    Try to read enough data to fill buffer, return amount of bytes read
    * @param  cfg    S/C tracker settings
    */
   virtual int read_overlapped(Buffer *buf, sctracker_settings_t &cfg) { return 0; }

   /**
    * Close the resource
    * @return int Returns 0 on success
    */
   virtual int close() = 0;

   /**
    * Check for end of file
    * @return bool EOF
    */
   virtual bool eof() = 0;

   /**
    * Skip over N bytes
    * @return bool EOF
    */
   virtual void skip_bytes(std::streampos pos) { return; }

   /**
    * Return a new data source object corresponding to the URI.
    * @return DataSource* A data source that can be FileSource, VSIBSource etc
    * @param  uri         The URL or path or other identifier for the resource location
    * @param  set         Settings that also contain the underlying 'sourceformat_str' used
    *                     by the specific data source later to remove headers.
    */
   static DataSource *getDataSource(std::string const &uri, struct sctracker_settings_tt *set);

private:
   struct sctracker_settings_tt *cfg;
};

#endif // DATASOURCE_H
