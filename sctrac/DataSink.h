/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DATASINK_H
#define DATASINK_H

#include "Settings.h"
#include <string>
#include <iostream>

class Buffer;

class DataSink
{

public:
   virtual ~DataSink() {}

   /**
    * Open and truncate the resource
    * @return int Returns 0 on success
    * @param  uri Location of the sink e.g. file path.
    */
   virtual int open(std::string uri) = 0;

   /**
    * Write data into resource
    * @return Returns the number of bytes written
    * @param  buf Pointer to the buffer to write out.
    */
   virtual size_t write(Buffer *buf) = 0;

   /**
    * Write array data into resource
    * @return Returns the number of bytes written
    * @param  data   Pointer to the data to write out.
    * @param  length The number of bytes to write.
    */
   virtual size_t write(char *data, size_t length) { return 0; }

   /**
    * Close the resource
    * @return int Returns 0 on success
    */
   virtual int close() = 0;

   /**
    * Return a new data sink object corresponding to the URI.
    * @return DataSink*   A data sink that can be FileSink, etc
    * @param  uri         The URL or path or other identifier for the resource location
    */
   static DataSink *getDataSink(std::string const &, sctracker_settings_t *);
};

#endif // DATASINK_H
