/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Version.h"
#include "sctracker.h"

#include <string>
#include <sstream>
#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
   Settings settings;
   sctracker_settings_t &sset = settings.getSettings();

   /* Show banner */
   cout << endl
        << BUILD_VERSION << endl;
   cout << "Built " << BUILD_DATE << " (#" << BUILD_NUMBER << ")" << endl
        << endl;

   /* Load INI file settings */
   if (argc >= 2)
   {
      settings.loadINI(argv[1]);
   }

   /* Parse command line options */
   for (int i = 2; i < argc; i++)
   {
      std::string opt, val;
      std::string str = std::string(argv[i]);

      opt = str.substr(2, str.find("=", 0) - 2);
      val = str.substr(str.find("=", 0) + 1, std::string::npos);
      std::istringstream istr(val);
      int ival = 0;
      istr >> ival;

      if (opt.compare("fftpts") == 0)
      {
         sset.fft_points = ival;
         if (sset.fft_points < 500)
         {
            cerr << "Option fftpts: " << sset.fft_points << " smaller than 500! Will use " << 16 * 1024 << " instead." << endl;
            sset.fft_points = 16 * 1024;
         }
      }
      else if (opt.compare("fftint") == 0)
      {
         sset.averaged_ffts = ival;
         if (sset.averaged_ffts < 2)
         {
            sset.averaged_ffts = 2;
         }
      }
      else if (opt.compare("bits") == 0)
      {
         sset.bits_per_sample = ival;
         if (sset.bits_per_sample < 1 || sset.bits_per_sample > 32)
         {
            sset.bits_per_sample = 2;
         }
      }
      else if (opt.compare("channels") == 0)
      {
         sset.source_channels = ival;
         if (sset.source_channels < 1 || sset.bits_per_sample > 32)
         {
            sset.source_channels = 2;
         }
      }
      else if (opt.compare("usechan") == 0)
      {
         sset.use_channel = ival - 1;
      }
      else
      {
         cerr << "Unknown option: " << opt << endl;
      }
   }

   /* Check that options are complete/supported */
   if (argc <= 1 || sset.num_sources < 1)
   {
      cout << "Multi-tone spacecraft tracking software - SCtracker" << endl
           << endl
           <<" Usage: sctracker <settings.ini> [--option=x]" << endl
           << endl
           << "   settings.ini - name and path to the INI file that contains processing settings" << endl
           << "   --fftpts=x   - how many points per FFT (a 2^N value)" << endl
           << "   --fftint=x   - how many FFTs to integrate" << endl
           << "   --bits=x     - how many bits per sample" << endl
           << "   --channels=x - how many channels from each resource" << endl
           << "   --usechan=x  - which of the channels to process" << endl
           << endl
           << " Settings in the INI file can be overriden with the above --options." << endl
           << endl;
      return 0;
   }

   /* Now settings are final, commit: prepare memory buffers, open files */
   settings.commit();

   /* Display some infos */
   Settings::summarize(sset);

   /* Process the data */
   TaskDispatcher *td = new TaskDispatcher();
   td->run(sset);
   return 0;
}
