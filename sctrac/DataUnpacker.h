/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DATAUNPACKER_H
#define DATAUNPACKER_H

#include "Settings.h"

#include <ippcore.h>
#include <string>

/**
 * DataUnpacker base class
 */
class DataUnpacker
{
public:
  DataUnpacker() { return; }
  explicit DataUnpacker(sctracker_settings_t const *settings) { return; }
  virtual size_t extract_samples(char const *const src, Ipp64f *dst, const size_t count, const int channel) const = 0;
  static bool canHandleConfig(sctracker_settings_t const *settings) { return false; }
};

#endif // DATAUNPACKER_H
