CC = g++
SHELL = /bin/bash
PROJ_PATH=${CURDIR}
OSTYPE := $(shell uname -s)
.DEFAULT_GOAL := all

# DEPENDENCY PATHS
IPP_INCLUDE_PATH=${PROJ_PATH}/deps/oneapi/ipp/latest/include/
MKL_INCLUDE_PATH=${PROJ_PATH}/deps/oneapi/mkl/latest/include/
SHARED_INCLUDE_PATH=${PROJ_PATH}/shared/
M5A_INCLUDE_PATH=${PROJ_PATH}/deps/m5a/
M5A_LIB_PATH=${PROJ_PATH}/deps/m5a/lib/
ifeq ($(OSTYPE), Darwin) # macOS paths
	IPP_LIB_PATH=${PROJ_PATH}/deps/oneapi/ipp/latest/lib/
	MKL_LIB_PATH=${PROJ_PATH}/deps/oneapi/mkl/latest/lib/
	IPP_SHAREDLIB_PATH=${PROJ_PATH}/deps/oneapi/compiler/latest/mac/compiler/lib/
else # Linux paths
	IPP_LIB_PATH=${PROJ_PATH}/deps/oneapi/ipp/latest/lib/intel64
	MKL_LIB_PATH=${PROJ_PATH}/deps/oneapi/mkl/latest/lib/intel64
	IPP_SHAREDLIB_PATH=${PROJ_PATH}/deps/oneapi/compiler/latest/linux/compiler/lib/intel64_lin
endif

# COMPILER/LINKER FLAGS
CFLAGS = -g -O3 -Wall -Wstrict-aliasing -pthread 
deps_CFLAGS := $(CFLAGS) -I${SHARED_INCLUDE_PATH} -I${IPP_INCLUDE_PATH} -I${MKL_INCLUDE_PATH} -I${M5A_INCLUDE_PATH}
deps_LFLAGS := -L${IPP_LIB_PATH} -Wl,-rpath,${IPP_LIB_PATH} \
	-L${MKL_LIB_PATH} -Wl,-rpath,${MKL_LIB_PATH} \
	-L${IPP_SHAREDLIB_PATH} -Wl,-rpath,${IPP_SHAREDLIB_PATH} \
	-L${M5A_LIB_PATH} -Wl,-rpath,${M5A_LIB_PATH} \
	-L${SHARED_INCLUDE_PATH} -Wl,-rpath,${SHARED_INCLUDE_PATH}

ifeq ($(OSTYPE), Darwin)
	swspec_LFLAGS := $(deps_LFLAGS) -liomp5 -lipps -lippvm -lippcore -lm -lmark5access
	sctrac_LFLAGS := $(swspec_LFLAGS) -lmkl_rt -Wl,-no_pie
else
	swspec_LFLAGS := -Wl,-Bdynamic $(deps_LFLAGS) -lipps -lippvm -lippcore -liomp5 -lmark5access
	sctrac_LFLAGS := $(swspec_LFLAGS) -lmkl_rt
endif
swspec_FLAGS := -I${PROJ_PATH}/swspec/ $(deps_FLAGS) $(swspec_LFLAGS)
sctrac_FLAGS := -I${PROJ_PATH}/sctrac/ $(deps_FLAGS) $(sctrac_LFLAGS)


# BUILD VERSIONING
include shared/Buildnumber.inc

# MAKE TARGETS
all: shared/Version.cpp _swspec _sctrac
	$(call buildver)

swspec: shared/Version.cpp _swspec
	$(call buildver)

sctrac: shared/Version.cpp _sctrac
	$(call buildver)

install:
	cp swspec/swspectrometer /usr/local/bin
	cp sctrac/sctracker /usr/local/bin

clean:
	rm -f s*/*.o
	rm -f swspec/swspectrometer
	rm -f sctrac/sctracker

.cpp.o:
	$(CC) $(deps_CFLAGS) -c $< -o $@	

_swspec:
	$(CC) $(deps_CFLAGS) shared/*.cpp swspec/*.cpp $(swspec_FLAGS) -o swspec/swspectrometer

_sctrac:
	$(CC) $(deps_CFLAGS) shared/*.cpp sctrac/*.cpp $(sctrac_FLAGS) -o sctrac/sctracker