/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "Helpers.h"

#include <sys/time.h>
#include <strings.h>
#include <string>
#include <iostream>
using std::cerr;
using std::endl;

/**
 * Get system seconds
 * @return double Current count of system seconds
 */
double Helpers::getSysSeconds(void)
{
   struct timeval time;
   gettimeofday(&time, NULL);
   return static_cast<double>(time.tv_sec) + static_cast<double>(time.tv_usec) * 1.0e-6;
}

/**
 * Reinterpret a "yes" or "no" string as a boolean.
 * @return String "yes"/"no" translated into true/false
 */
bool Helpers::parseBoolString(const char *str)
{
   if (strcasecmp(str, "yes") == 0 || strcasecmp(str, "true") == 0)
   {
      return true;
   }
   else if (strcasecmp(str, "no") == 0 || strcasecmp(str, "false") == 0)
   {
      return false;
   }

   cerr << "Warning: Setting should be yes/no or true/false. Instead we got '" << str << "'." << endl;
   return false;
}

/**
 * Reinterpret string as Window Function
 * @return WindowFunctionType corresponding to
 *         the string or Cosine2 as default.
 */
WindowFunctionType Helpers::parseWindowingString(const std::string &str)
{
   if (caseInsensitiveMatch(str, "None"))
   {
      return None;
   }
   else if (caseInsensitiveMatch(str, "Cosine"))
   {
      return Cosine;
   }
   else if (caseInsensitiveMatch(str, "Cosine2"))
   {
      return Cosine2;
   }
   else if (caseInsensitiveMatch(str, "Hamming"))
   {
      return Hamming;
   }
   else if (caseInsensitiveMatch(str, "Hann"))
   {
      return Hann;
   }
   else if (caseInsensitiveMatch(str, "Blackman"))
   {
      return Blackman;
   }
   else
   {
      cerr << "Warning: window func  '" << str << "' not supported, will use Cosine2 instead." << endl;
      return Cosine2;
   }
}

/**
 * Reinterpret string as PhaseCoeffType.
 * @return PhaseCoeffType corresponding to the
 *         string or SampleBased as default.
 */
PhaseCoeffType Helpers::parsePhaseCoeffString(const std::string &str)
{
   if (caseInsensitiveMatch(str, "SampleBased"))
   {
      return SampleBased;
   }
   else if (caseInsensitiveMatch(str, "TimeBased"))
   {
      return TimeBased;
   }
   else
   {
      cerr << "Warning: phase coeff type '" << str << "' not supported, will use SampleBased instead." << endl;
      return SampleBased;
   }
}

/**
 * Print out a complex vector. Mainly for debugging.
 */
void Helpers::printComplexVector(float *v, int pts)
{
   for (int i = 0; i < pts; i++)
   {
      cerr << v[2 * i] << "+i" << v[2 * i + 1] << " \t";
   }
   cerr << endl
        << std::flush;
}

/**
 * Case-insensitive string equality comparison.
 * Works for ASCII strings but not unicode.
 */
Helpers::CiComparisonResult Helpers::caseInsensitiveCompare(const std::string &str1, const std::string &str2)
{
   std::string::const_iterator c1 = str1.begin();
   std::string::const_iterator c2 = str2.begin();
   if (str1.size() < 1 || str2.size() < 1)
   {
      return NoMatch;
   }

   while (c1 != str1.end() && c2 != str2.end())
   {
      unsigned char cc1 = *c1;
      unsigned char cc2 = *c2;
      if (std::tolower(cc1) != std::tolower(cc2))
      {
         return NoMatch;
      }
      ++c1, ++c2;
   }

   if ((c1 == str1.end()) && (c2 == str2.end()))
   {
      return FullMatch;
   }

   return PrefixMatch;
}

bool Helpers::caseInsensitiveMatch(const std::string &str1, const std::string &str2)
{
   return strcasecmp(str1.c_str(), str2.c_str()) == 0;
}

/**
 * Calculate and return the greatest common divisor of two integers.
 */
long long Helpers::gcd(long long a, long long b)
{
   while (true)
   {
      a = a % b;
      if (a == 0)
      {
         return b;
      }
      b = b % a;
      if (b == 0)
      {
         return a;
      }
   }
}