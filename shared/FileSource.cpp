/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>
#include "FileSource.h"
#include "Helpers.h"

#include <mark5access.h>

using std::cerr;
using std::endl;

/**
 * Open the file
 * @return int Returns 0 on success
 * @param  uri File path
 */
int FileSource::open(std::string uri)
{
    /* Determine format: with headers, without headers? */
    std::ostream *log = cfg->tlog;
    this->_uri = std::string(uri);
    this->_format = std::string(cfg->sourceformat_str);
    cfg->sourceformat = Unknown;
    sourceformat_uses_frames = false;

    if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "Mk5B"))
    {
        cfg->sourceformat = Mk5B;
        sourceformat_uses_frames = true;
        frame_header_length = 16;     // specified to be always 16B
        frame_payload_length = 10000; // specified to be always 10.000B
    }
    else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "VDIX") || (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VDIX") == Helpers::PrefixMatch))
    {
        cfg->sourceformat = VDIX;
        std::string format_packet = cfg->sourceformat_str.substr(4);
        sourceformat_uses_frames = true;
        frame_header_length = 32;
        /** Calculate packet length
         * VDIX -> automatically assumes 8000 + 32
         * VDIXxxxx -> The sum of payload + header (32B) = xxxx
         */
        if (atoi(format_packet.c_str()) == 0)
        {
            frame_payload_length = 8000;
        }
        else
        {
            frame_payload_length = atoi(format_packet.c_str()) - frame_header_length;
        }
    }
    else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "iBob"))
    {
        cfg->sourceformat = iBOB;
        sourceformat_uses_frames = true;
        frame_header_length = 4;     // 64-bit UDP packet sequence#
        frame_payload_length = 4096; // depends on ibob settings!!
    }
    else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "K5"))
    {
        cfg->sourceformat = K5;
        frame_header_length = 32;
        sourceformat_uses_frames = true;
        frame_payload_length = 4e6;
    }
    else
    {
        /* Format has headers that overwrite data */
        if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VLBA") == Helpers::PrefixMatch)
        {
            mk5fileoffset = 0;
            _mk5s = new_mark5_stream_absorb(
                new_mark5_stream_file(_uri.c_str(), mk5fileoffset),
                new_mark5_format_generic_from_string(_format.c_str()));
            mark5_stream_print(_mk5s);
            if (!_mk5s)
            {
                *log << "Mark5access ERROR: apparently did not like format " << _format << endl;
                return -1;
            }
            cfg->sourceformat = VLBA;
        }
        else if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "Mark5B") == Helpers::PrefixMatch)
        {
            mk5fileoffset = 0;
            _mk5s = new_mark5_stream_absorb(
                new_mark5_stream_file(_uri.c_str(), mk5fileoffset),
                new_mark5_format_generic_from_string(_format.c_str()));
            mark5_stream_print(_mk5s);
            if (!_mk5s)
            {
                *log << "Mark5access ERROR: apparently did not like format " << _format << endl;
                return -1;
            }
            cfg->sourceformat = Mark5B;
        }
        else if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VDIF") == Helpers::PrefixMatch)
        {
            mk5fileoffset = 0;
            _mk5s = new_mark5_stream_absorb(
                new_mark5_stream_file(_uri.c_str(), mk5fileoffset),
                new_mark5_format_generic_from_string(_format.c_str()));
            mark5_stream_print(_mk5s);
            if (!_mk5s)
            {
                *log << "Mark5access ERROR: apparently did not like format " << _format << endl;
                return -1;
            }
            cfg->sourceformat = VDIF;
        }
        /* Format uses no headers at all */
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "Maxim"))
        {
            cfg->sourceformat = Maxim;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RawSigned"))
        {
            cfg->sourceformat = RawSigned;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RawUnsigned"))
        {
            cfg->sourceformat = RawUnsigned;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RDF"))
        {
            cfg->sourceformat = RDF;
        }
        frame_header_length = 0;
        frame_payload_length = 1;
    }

    /* Show the result */
    if (sourceformat_uses_frames)
    {
        *log << "FileSource   : processing framed data with " << frame_header_length << "-byte headers and "
             << frame_payload_length << "-byte payloads" << endl;
    }
    else
    {
        *log << "FileSource   : processing unframed or data-replacement data format" << endl;
    }

    /* Open the file */
    if (ifile.is_open())
    {
        ifile.close();
    }
    ifile.open(_uri.c_str(), std::ios::binary | std::ios::in);
    if (!ifile.is_open())
    {
        *log << "Could not open input file " << uri << endl;
        got_eof = true;
        return -1;
    }

    /* Read the header and possibly update 'frame_payload_length' */
    this->locateFirstHeader();
    ifile.seekg(-frame_header_length, std::ios_base::cur);

    if (cfg->sourceformat == RDF)
    {
        /* Read the first 256 B and consider the rest as data*/
        std::ifstream::pos_type smp_bytes, seek_pos, frames;
        smp_bytes = 256;
        seek_pos += smp_bytes;
        first_header_offset = seek_pos;
        unsigned char header[256 + 1];
        ifile.read(reinterpret_cast<char *>(&header[0]), smp_bytes);

        char year[4];
        char doy[3];
        unsigned char time[] = "  ";

        int mjd = 0, sec = 0;
        double ns = 0;
        std::stringstream str, str2, str3;

        doy[0] = header[11];
        doy[1] = header[12];
        doy[2] = header[13];
        str << doy;
        int x;
        str >> x;
        year[0] = header[6];
        year[1] = header[7];
        year[2] = header[8];
        year[3] = header[9];
        mjd = 51544 + (std::stoi(year) - 2000) * 365.25 + x;

        time[0] = header[15];
        time[1] = header[16];
        str << time;
        int hh;
        str >> hh;

        time[0] = header[18];
        time[1] = header[19];
        str2 << time;
        int mm;
        str2 >> mm;

        time[0] = header[21];
        time[1] = header[21];
        str3 << time;
        int ss;
        str3 >> ss;

        sec = (hh * 3600) + mm * 60 + ss;

        *log << "RDF          : starting at " << mjd << " MJD " << sec << " sec + " << ns << " ns" << endl;
        cfg->logIO->stream() << "// RDF start time <MJD sec ns>" << endl;
        cfg->logIO->stream() << std::fixed << mjd << " " << sec << " " << ns << endl;
    }

    /* Skip the first part of data? */
    if (cfg->seconds_to_skip > 0)
    {
        if (cfg->sourceformat == VLBA || cfg->sourceformat == Mark5B || cfg->sourceformat == VDIF)
        {
            this->reopen_mark5_stream(cfg->seconds_to_skip);
        }
        else
        {
            std::ifstream::pos_type seek_pos = first_header_offset;
            seek_pos += seconds_to_source_bytes(cfg->seconds_to_skip);
            *log << "Skipping " << cfg->seconds_to_skip << " sample-seconds: "
                 << "seeking to offset " << seek_pos << " bytes" << endl;
            ifile.seekg(seek_pos, std::ios_base::cur);
            first_header_offset = seek_pos; // this will be used for modulo frame-boundary detection
        }
    }

    /* Set a point to stop reading data? */
    if(cfg->seconds_to_stop > 0)
    {
        int read_seconds = cfg->seconds_to_stop - std::max(cfg->seconds_to_skip, 0);
        if (read_seconds > 0)
        {
            num_bytes_unread = seconds_to_source_bytes(read_seconds);
        }
    }

    /* Show the first header, if any */
    if (sourceformat_uses_frames)
    {
        this->inspectAndConsumeHeader();
        ifile.seekg(-frame_header_length, std::ios_base::cur);
    }

    got_eof = false;
    return 0;
}

/**
 * Try to fill the entire bufferspace with new data and return the number of bytes actually read
 * @return int    Returns the amount of bytes read
 * @param  bspace Pointer to Buffer to fill out
 */
int FileSource::read(Buffer *buf)
{
    if (!ifile.is_open() || NULL == buf || this->cfg == NULL)
    {
        buf->setLength(0);
        cerr << "FileSource::read() attempt to read in illegal state" << endl;
        return 0;
    }

    std::ostream *log = cfg->tlog;
    size_t num_bytes_req = buf->getAllocated();
    size_t num_bytes_read = 0;

    // if we want to stop data early, check if we should stop yet
    bool last_frame = false;
    if (cfg->seconds_to_stop > 0 && num_bytes_req > num_bytes_unread)
    {
        num_bytes_req = num_bytes_unread;
        last_frame = true;
    }

    /* Formats with headers that do not overwrite data */
    if (sourceformat_uses_frames)
    {
        char *dst = buf->getData();
        const int framesize = frame_header_length + frame_payload_length;
        int num_bytes_iter = 0;

        while ((num_bytes_req > 0) && !got_eof)
        {
            /* At frame start boundary: decode & skip header */
            std::streampos curr = ifile.tellg();
            if ((curr % framesize) == 0)
            {
                this->inspectAndConsumeHeader();
                continue;
            }

            /* Within frame: grab all data before start of next frame */
            int available = framesize - (curr % framesize);
            ifile.read(dst, available);
            if (ifile.fail() && !ifile.eof())
            {
                *log << "Read I/O error!" << endl
                     << std::flush;
            }
            if (!ifile || !ifile.good())
            {
                got_eof = true;
            }

            /* Advance */
            num_bytes_iter = ifile.gcount();
            dst += num_bytes_iter;
            num_bytes_req -= num_bytes_iter;
            num_bytes_read += num_bytes_iter;
        }
    }

    /* Formats without headers or with headers that overwrite data */
    if (!sourceformat_uses_frames)
    {
        if (cfg->sourceformat == VLBA)
        {
            int status = mark5_stream_copy(_mk5s, num_bytes_req, buf->getData());
            if (status != 0)
            {
                int bytegranularity = _mk5s->samplegranularity * _mk5s->nbit * _mk5s->nchan / 8;
                *log << "mark5_stream_copy returned error (or EOF!) - check that bufsize " << buf->getAllocated()
                     << " matches granularity " << bytegranularity << endl;
                got_eof = true;
            }
            num_bytes_read = num_bytes_req;
        }
        else
        {
            ifile.read(buf->getData(), num_bytes_req);
            if (ifile.fail() && !ifile.eof())
            {
                *log << "Read I/O error!" << endl
                     << std::flush;
            }
            if (!ifile || !ifile.good())
            {
                got_eof = true;
            }
            num_bytes_read = ifile.gcount();
            if ( num_bytes_read != num_bytes_req && !got_eof)
            {
                *log << "Read " << num_bytes_read << " bytes instead of " << num_bytes_req << endl;
            }
        }
    }

    if (last_frame) { got_eof = true; }
    buf->setLength(num_bytes_read);
    num_bytes_unread -= num_bytes_read;
    return num_bytes_read;
}

/**
 * Close the resource
 * @return int
 */
int FileSource::close()
{
    ifile.close();
    return 0;
}

/**
 * Check for end of file
 * @return bool EOF
 */
bool FileSource::eof()
{
    return got_eof;
}

/**
 * On an open stream, scan for the first occurence of a format-dependant
 * data header and set the first_header_offset variable to it.
 * For headerless formats first_header_offset is set to 0.
 */
void FileSource::locateFirstHeader()
{
    std::ostream *log = cfg->tlog;

    if (!sourceformat_uses_frames)
    {

        /* Headerless in the sense of data-replacement headers */
        if (cfg->sourceformat == VLBA || cfg->sourceformat == Mark5B || cfg->sourceformat == VDIF)
        {
            first_header_offset = 0;  // unused
            frame_payload_length = 0; // unused

            int mjd = 0, sec = 0;
            double ns = 0;
            mark5_stream_get_sample_time(_mk5s, &mjd, &sec, &ns);
            if (ns > 0.0f)
            {
                *log << "First frame not at an integer second. "
                     << "Re-opening first integer second and skipping additional "
                     << cfg->seconds_to_skip << "s." << endl;
                int old_sec = sec;
                this->reopen_mark5_stream(cfg->seconds_to_skip);
                mark5_stream_get_sample_time(_mk5s, &mjd, &sec, &ns);
                if ((old_sec + 1) != sec || (ns != 0.0f))
                {
                    *log << "Warning: mark5_stream_get_sample_time gave wrong second" << endl;
                }
            }

            *log << "VLBA/Mk5B/VDIF    : starting at " << mjd << " MJD " << sec << " sec + " << ns << " ns" << endl;
            cfg->logIO->stream() << "// VLBA/Mk5B/VDIF start time <MJD sec ns>" << endl;
            cfg->logIO->stream() << std::fixed << mjd << " " << sec << " " << ns << endl;

            /* Truly raw data or we just don't care about headers */
        }
        else if (cfg->sourceformat == RDF)
        {
            /* timestamp written earlier */
        }
        else
        {
            cfg->logIO->stream() << "// No timestamps extracted from input data" << endl;
            first_header_offset = 0;
        }
    }
    else
    {
        /* With headers */
        if (cfg->sourceformat == Mk5B)
        {
            // we keep life simple and assume a smart recording tool
            // has been used, such that the first header always
            // begins at byte 0!
            // MJD_0 is the 0 position of the truncated MJD
            first_header_offset = 0;
            int mjd = 0, sec = 0;
            double ns = 0;
            int MJD_0 = 57000;
            unsigned char header[frame_header_length + 1];

            if (!ifile.is_open())
            {
                return;
            }
            if (frame_header_length <= 0 || !sourceformat_uses_frames)
            {
                return;
            }

            ifile.read(reinterpret_cast<char *>(&header[0]), frame_header_length);
            if (!ifile || !ifile.good())
            {
                got_eof = true;
                return;
            }

            mjd = MJD_0 + int(header[2 * 4 + 3] >> 4) * 100 + int(header[2 * 4 + 3] & 0x0F) * 10 + int(header[2 * 4 + 2] >> 4);
            sec = int(header[2 * 4 + 2] & 0x0F) * 10000 + int(header[2 * 4 + 1] >> 4) * 1000 + int(header[2 * 4 + 1] & 0x0F) * 100 + int(header[2 * 4 + 0] >> 4) * 10 + int(header[2 * 4 + 0] & 0x0F);

            *log << "Mark5B       : starting at " << mjd << " MJD " << sec << " sec + " << ns << " ns" << endl;
            cfg->logIO->stream() << "// Mark5B start time <MJD sec ns>" << endl;
            cfg->logIO->stream() << std::fixed << mjd << " " << sec << " " << ns << endl;
        }
        else if (cfg->sourceformat == VDIX)
        {
            // we keep it simple again and assume that the headers are at the beginning
            first_header_offset = 0;
            int mjd = 0, sec = 0, sec_from_ref = 0;
            double ns = 0;
            int MJD_0 = 51550;
            int step = 0;
            unsigned char header[frame_header_length + 1];

            if (!ifile.is_open())
            {
                return;
            }
            if (frame_header_length <= 0 || !sourceformat_uses_frames)
            {
                return;
            }

            ifile.read(reinterpret_cast<char *>(&header[0]), frame_header_length);
            if (!ifile || !ifile.good())
            {
                got_eof = true;
                return;
            }
            step = int(header[1 * 4 + 3] & 0x3F);

            sec_from_ref = int((header[3] & 0x3F)) * 16777216 + int(header[2]) * 65536 + int(header[1]) * 256 + int(header[0]);

            mjd = MJD_0 + int(step * 365 / 2) + sec_from_ref / (24 * 3600);
            sec = sec_from_ref % (24 * 3600);

            *log << "VDIF         : starting at " << mjd << " MJD " << sec << " sec + " << ns << " ns" << endl;
            cfg->logIO->stream() << "// VDIF start time <MJD sec ns>" << endl;
            cfg->logIO->stream() << std::fixed << mjd << " " << sec << " " << ns << endl;
        }
        else if (cfg->sourceformat == K5)
        {
            int mjd = 0, sec = 0;
            double ns = 0;

            *log << "K5         : starting at " << mjd << " MJD " << sec << " sec + " << ns << " ns" << endl;
            cfg->logIO->stream() << "// K5 start time <MJD sec ns>" << endl;
            cfg->logIO->stream() << std::fixed << mjd << " " << sec << " " << ns << endl;
        }
        else if (cfg->sourceformat == iBOB)
        {
            first_header_offset = 0;
            cfg->logIO->stream() << "// No timestamps extracted from input data" << endl;
        }
        else
        {
            *log << "FileSource::locateFirstHeader: don't know how to handle '"
                 << cfg->sourceformat << "'!" << endl;
            cfg->logIO->stream() << "// No timestamps extracted from input data" << endl;
        }
    }
}

/**
 * Assumes a header starts at the current read offset.
 * Reads the header and parses it. The read offset
 * after this function returns contains data.
 */
void FileSource::inspectAndConsumeHeader()
{
    if (!ifile.is_open())
    {
        return;
    }
    if (frame_header_length <= 0 || !sourceformat_uses_frames)
    {
        return;
    }

    unsigned char header[frame_header_length + 1];
    ifile.read(reinterpret_cast<char *>(&header[0]), frame_header_length);
    if (!ifile || !ifile.good())
    {
        got_eof = true;
        return;
    }

    if (cfg->sourceformat == Mk5B)
    {
        // http://www.atnf.csiro.au/vlbi/wiki/index.php?n=EXPReS.Mark5BFormat
        // Data on disk is divided into equal-length disk frames (DF). Each DF has
        // a header of 4 32-bit words (4*4=16 bytes) followed by 2500 32-bit words of data
        // (10000 bytes). The DF boundary is aligned with the UT second tick.
        // Word 0   Synchronization word (0xABADDEED)
        // Word 1   Bits 31-16: User specifed,  Bit 15: T - tvg data (test data if set),
        //          Bits 14-0: DF # within second
        // Word 2-3 VLBA BCD Time code and 16 bit CRC
        //
        // Words are stored MSB first, LSB last. So the byte stream is [w0MSB w0xSB w0xSB w0LSB | ... ]
        //
        // Samples are supposedly stored like:
        // "Raw VLBI data is packed within a 32 bit word, with the earliest time sample corresponding to the least significant bits."
        // => [oldest newest] [oldest newest] ...
        //
        // The above does not match official(?) ftp://web.haystack.edu/pub/mark5/005.2.pdf
        // where the format is totally different, the sync word is '111111..
        bool show = false;
        if (header[0 * 4 + 3] != 0xAB && header[0 * 4 + 2] != 0xAD && header[0 * 4 + 1] != 0xDE && header[0 * 4 + 0] != 0xED)
        {
            cerr << "Mark5B: incorrect header at file offset 0x" << ifile.tellg() << endl;
            show = true;
        }
        if (show)
        {
            cerr << "Mark5B header: 0xABADDEED ?= { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 3])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 2])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 0])
                      << " }, user = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[1 * 4 + 3])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[1 * 4 + 2])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[1 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[1 * 4 + 0])
                      << " }, timecode = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[2 * 4 + 3])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[2 * 4 + 2])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[2 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[2 * 4 + 0])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 3])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 2])
                      << " }, crc = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 0])
                      << " }; " << endl;
        }
    }
    else if (cfg->sourceformat == VDIX)
    {
        bool show = false;
        if (header[4 * 4 + 3] != 0x00 && header[4 * 4 + 2] != 0x00 && header[4 * 4 + 1] != 0x00 && header[4 * 4 + 0] != 0x00)
        {
            cerr << "VDIF: incorrect header at file offset 0x" << ifile.tellg() << endl;
            show = true;
        }
        if (show)
        {
            cerr << "VDIF header: 0x00000000 ?= { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[4 * 4 + 3])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[4 * 4 + 2])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[4 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[4 * 4 + 0])
                      << " }, valid data = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 3])
                      << " }, seconds from reference epoch = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 2])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[0 * 4 + 0])
                      << " }, reference epoch = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[1 * 4 + 3])
                      << " }, station = { "
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 1])
                      << std::hex << std::setw(2) << std::setfill('0') << int(header[3 * 4 + 0])
                      << " }, " << endl;
        }
    }
    else if (cfg->sourceformat == iBOB)
    {
        // Sergei format: 4 bytes Unix timestamp, 4 bytes packet seq nr starting from 1
        long second = ((static_cast<long>(header[0])) << 24) + ((static_cast<long>(header[1])) << 16) + ((static_cast<long>(header[2])) << 8) + header[3];
        long frame = ((static_cast<long>(header[4])) << 24) + ((static_cast<long>(header[5])) << 16) + ((static_cast<long>(header[6])) << 8) + header[7];
        if (frame == 1)
        {
            cerr << "iBOB sec=" << second << " frame=" << frame << endl;
        }
    }
}

/**
 * Workaround for mark5acces mark5_stream_seek() bugs.
 * Call this after seeking to the desired timestamp.
 * Determines the current file offset and re-opens
 * the stream at this offset.
 */
void FileSource::reopen_mark5_stream(int secondstoskip)
{
    if (_mk5s == NULL)
        return;

    /* Figure out the offset mainly by guessing:
     * The earlier seek causes mark5access to refill its internal buffer, hence file offset
     * is at the end of that read buffer. Further we remove almost half a frame, so that
     * on reopening the (hopefully) correct first integer-second frame is detected.
     */
    mk5fileoffset += seconds_to_source_bytes(secondstoskip) + _mk5s->frameoffset;
    if (_mk5s->payloadoffset < 0)
    {
        /* we have to seek back a little to avoid a mark5access decode bug */
        mk5fileoffset += _mk5s->payloadoffset;
    }
    *(cfg->tlog) << "FileSource::reopen_mark5_stream: new offset after skipping additional "
                 << secondstoskip << "s is " << mk5fileoffset << endl;

    /* Create new stream */
    delete_mark5_stream(_mk5s);
    _mk5s = new_mark5_stream_absorb(
        new_mark5_stream_file(_uri.c_str(), mk5fileoffset),
        new_mark5_format_generic_from_string(_format.c_str()));
    if (!_mk5s)
    {
        *(cfg->tlog) << "new_mark5_stream failed!" << endl;
    }
    mark5_stream_print(_mk5s);

    /* Seek in the ifstream as well, to what _should_ be the start of a frame */
    ifile.seekg(mk5fileoffset, std::ios_base::beg);

    return;
}

// note this method always rounds up to whole frames in framed data
// for SourceSkipSeconds and SourceStopSeconds this means it may start slightly late
// and end slightly late to adhere to frame boundaries
size_t FileSource::seconds_to_source_bytes(int seconds) {
    if (seconds <= 0) { return 0; } // can't have a negative amount of bytes or seconds
    if (cfg->sourceformat == VLBA || cfg->sourceformat == Mark5B || cfg->sourceformat == VDIF) {
        // these formats use mk5
        if (_mk5s == NULL) { return 0; } // can't do mark5 stuff without a stream
        int64_t nanoseconds_first_frame = (_mk5s->ns == 0) ? 0 : 1000000000ULL - _mk5s->ns;
        int64_t nanoseconds_to_skip = 1000000000ULL * seconds;
        int frames = (nanoseconds_first_frame + nanoseconds_to_skip) / _mk5s->framens;
        return _mk5s->framebytes * frames;
    }
    else
    {
        // these formats use manual fileread
        size_t smp_bytes = (cfg->bits_per_sample * cfg->source_channels * cfg->samplingfreq) / 8;

        if (sourceformat_uses_frames)
        {
            size_t frames = std::ceil((smp_bytes * seconds) / frame_payload_length);
            return frames * (frame_header_length + frame_payload_length);
        }
        else
        {
            return smp_bytes * seconds;
        }
    }
    return 0;
}
