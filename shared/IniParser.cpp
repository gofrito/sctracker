/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "IniParser.h"
#include "Helpers.h"
#include <sstream>
#include <string>
#include <stdlib.h> // atoi()
#include <vector>
#include <iterator>

using std::cerr;
using std::endl;

/***
 * IniParser(filename)
 *
 * Opens, loads and parses the specified INI file.
 * @param filename   File and path to the INI file.
 */
IniParser::IniParser(std::string filename)
{
    std::ifstream file(filename.c_str());
    std::string line;
    int linenr = -1;

    iniSections.clear();
    section_index = -1;

    if (!file.is_open())
    {
        cerr << "Error: Could not open INI file " << filename << endl;
        return;
    }

    /* parse all lines */
    while (!std::getline(file, line).fail())
    {
        std::string mline;
        size_t cpos;
        linenr++;

        // remove all ' ' spaces throughout the string
        std::string element;
        std::istringstream istr(line);
        while (istr >> element)
        {
            mline = mline + element;
        }

        // remove comments
        cpos = mline.find_first_of("'#", 0);
        if (cpos != std::string::npos)
        {
            mline.resize(cpos);
        }

        // skip blank lines
        if (mline.size() < 1)
        {
            continue;
        }

        // add sections
        if (mline.at(0) == '[')
        {
            inisection_t nsec;
            cpos = mline.find_first_of("]", 0);
            if ((cpos == std::string::npos) || (cpos <= 1))
            {
                cerr << "INI section line " << linenr << " '" << line << "' incorrectly formed'" << endl;
                continue;
            }
            nsec.sectionname = mline.substr(1, cpos - 1);
            section_index++;
            iniSections.push_back(nsec);
            continue;
        }

        // add key/value pairs
        cpos = mline.find_first_of("=", 0);
        if ((cpos != std::string::npos) && (cpos > 1))
        {
            if (section_index < 0)
            {
                continue;
            }
            keyvalpair_t kv;
            kv.key = mline.substr(0, cpos);
            kv.value = mline.substr(cpos + 1, std::string::npos);
            if (this->hasKey(kv.key))
            {
                cerr << "Warning: INI section contains duplicate key '" << kv.key << "'! "
                     << "Not assigning new value '" << kv.value << "'." << endl;
                continue;
            }
            iniSections.at(section_index).entries.push_back(kv);
            continue;
        }

        cerr << "INI has malformed line " << linenr << " '" << line << "' "
             << "(parsed as '" << mline << "') " << endl;
    }

    return;
}

/***
 * bool selectSection(sectioname)
 *
 * Finds the INI file section ([sectionname]) and
 * sets it as the active section. The hasKey() and
 * getKeyValue() functions will use the active
 * section to look for key-value pairs.
 * @param sectionname  name of the INI section
 * @return             true if the section is found
 */
bool IniParser::selectSection(std::string sectionname)
{
    this->section_index = -1;

    std::vector<inisection_t>::iterator i;
    for (i = iniSections.begin(); i != iniSections.end(); i++)
    {
        this->section_index++;
        if (Helpers::caseInsensitiveMatch(i->sectionname, sectionname))
        {
            return true;
        }
    }

    this->section_index = -1;
    return false;
}

/***
 * bool hasKey(key)
 *
 * Looks in the active section for the specified key.
 * @param key    name of the key to find
 * @return       true if the key is found
 */
bool IniParser::hasKey(std::string const &key) const
{
    int section = this->section_index;
    if (section < 0)
    {
        return false;
    }

    std::vector<keyvalpair_t> const &entries = iniSections.at(section).entries;
    std::vector<keyvalpair_t>::const_iterator i;
    for (i = entries.begin(); i != entries.end(); i++)
    {
        if (Helpers::caseInsensitiveMatch(i->key, key))
        {
            return true;
        }
    }
    return false;
}

/***
 * bool getKeyValue(key, string value)
 *
 * Gets the value assigned to the given key in the
 * currently active section. If the key is not found,
 * the function returns false and the contents of 'value'
 * remains unchanged.
 * @param key   name of the key to find and read
 * @param value returns the string value of the key if it is found
 * @return      true if the key was found
 */
bool IniParser::getKeyValue(const char *key, std::string &value) const
{
    int section = this->section_index;
    if (section < 0)
    {
        return false;
    }

    std::vector<keyvalpair_t> const &entries = iniSections.at(section).entries;
    std::vector<keyvalpair_t>::const_iterator i;
    for (i = entries.begin(); i != entries.end(); i++)
    {
        if (Helpers::caseInsensitiveMatch(i->key, key))
        {
            value = std::string(i->value);
            return true;
        }
    }
    return false;
}

/**
 * bool getKeyValue(key, int value)
 * Gets the value assigned to the given key in the
 * currently active section. If the key is not found,
 * the function returns false and the contents of 'value'
 * remains unchanged.
 * @param key   name of the key to find and read
 * @param value returns the integer value of the key if it is found
 * @return      true if the key was found
 */
bool IniParser::getKeyValue(const char *key, int &value) const
{
    std::string strvalue;
    bool rc = getKeyValue(key, strvalue);
    if (rc)
    {
        value = static_cast<int>(atof(strvalue.c_str()));
    }
    return rc;
}

bool IniParser::getKeyValue(const char *key, size_t &value) const
{
    int ivalue;
    bool rc = getKeyValue(key, ivalue);
    if (rc)
    {
        value = static_cast<size_t>(ivalue);
    }
    return rc;
}

bool IniParser::getKeyValue(const char *key, float &value) const
{
    std::string strvalue;
    bool rc = getKeyValue(key, strvalue);
    if (rc)
    {
        value = atof(strvalue.c_str());
    }
    return rc;
}

bool IniParser::getKeyValue(const char *key, double &value) const
{
    std::string strvalue;
    bool rc = getKeyValue(key, strvalue);
    if (rc)
    {
        value = atof(strvalue.c_str());
    }
    return rc;
}

bool IniParser::getKeyValue(const char *key, bool &value) const
{
    std::string strvalue;
    bool rc = getKeyValue(key, strvalue);
    if (rc)
    {
        value = Helpers::parseBoolString(strvalue.c_str());
    }
    return rc;
}