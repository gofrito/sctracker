/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "VSIBSource.h"
#include <string>
#include <iostream>
#include <unistd.h>

using std::cerr;
using std::endl;

/**
 * Open the file
 * @return int Returns 0 on success
 * @param  uri File path
 */
int VSIBSource::open(std::string uri)
{
   num_bytes_unread = 16 * 1024 * 1024; // TODO(mars): parse from 'uri'
   fvsib = ::open(VSIB_DEV_PATH, O_RDONLY);
   if (-1 == fvsib)
   {
      cerr << "VSIBSource: could not open " << VSIB_DEV_PATH << endl;
      return -1;
   }
   ::ioctl(fvsib, VSIB_SET_MODE, VSIB_MODE_MODE(0x00) | VSIB_MODE_RUN);
   return 0;
}

/**
 * Try to fill the entire bufferspace with new data and return the number of bytes actually read
 * @return int    Returns the amount of bytes read
 * @param  bspace Pointer to Buffer to fill out
 */
int VSIBSource::read(Buffer *buf)
{
   // TODO(mars): if this sourceformat is ever used, this needs to be implemented
   if(cfg->seconds_to_skip > 0 || cfg->seconds_to_stop > 0)
   {
      cerr << "Warning: SourceSkipSeconds and SourceStopSeconds not implemented "
         << "for VSIBSource input. Parameters will be ignored." << endl;
   }

   char *dst = buf->getData();
   size_t num_bytes_req = buf->getAllocated();
   size_t num_bytes_read = 0;
   size_t num_bytes_iter;

   /* read the data */
   while (num_bytes_req > 0)
   {
      num_bytes_iter = ::read(fvsib, dst, num_bytes_req);
      dst += num_bytes_iter;
      num_bytes_req -= num_bytes_iter;
      num_bytes_read += num_bytes_iter;
   }
   /* set buffer size */
   num_bytes_req = buf->getAllocated();
   buf->setLength(num_bytes_req);
   /* check for "EOF" */
   if (num_bytes_req > num_bytes_unread)
   {
      num_bytes_unread = 0;
   }
   else
   {
      num_bytes_unread -= num_bytes_req;
   }
   return num_bytes_read;
}

/**
 * Close the resource
 * @return int
 */
int VSIBSource::close()
{
   ::ioctl(fvsib, VSIB_SET_MODE, VSIB_MODE_STOP);
   ::close(fvsib);
   num_bytes_unread = 0;
   return 0;
}

/**
 * Check for end of file
 * @return bool EOF
 */
bool VSIBSource::eof()
{
   return (num_bytes_unread <= 0);
}