/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2019 Nelson Nunes                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef __APPLE__
#include "MacPort.h"
#include <cstddef>
#include <stdlib.h>

void *fake_memalign(size_t alignment, size_t size)
{

    // alignment must be >= sizeof(void*)
    if (alignment < sizeof(void *))
    {
        alignment = sizeof(void *);
    }

    void *pointer;
    if (posix_memalign(&pointer, alignment, size) == 0)
    {
        return pointer;
    }

    return nullptr;
}

#endif