/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "Buffer.h"

#include <iostream>
#include <cstring>

#if defined(__APPLE__)
#include <stdlib.h>
#include "MacPort.h"
#else
#include <malloc.h>
#endif

using std::cerr;
using std::endl;

/**
 * Initialize buffer by allocating memory-aligned bytes.
 * @param bytes Amount of bytes to allocate
 */
Buffer::Buffer(size_t bytes)
{
   len_allocated = 0;
   length = 0;
   data = static_cast<char *>(memalign(128, bytes));
   if (data == NULL)
   {
      cerr << "Failed to allocate " << bytes << " bytes." << std::endl;
   }
   else
   {
      len_allocated = bytes;
   }
}

/**
 * Release the buffer
 */
Buffer::~Buffer()
{
   if (data != NULL)
   {
      free(data);
      data = NULL;
   }
}

/**
 * Return pointer to buffer.
 * @return char*
 */
char *Buffer::getData()
{
   return data;
}

/**
 * Return amount of data in the buffer.
 * @return size_t
 */
size_t Buffer::getLength()
{
   return length;
}

/**
 * Returns how many bytes were allocated for the buffer.
 * @return size_t
 */
size_t Buffer::getAllocated()
{
   return len_allocated;
}

/**
 * Externally set the length of contained data.
 * @param len New length
 */
void Buffer::setLength(size_t len)
{
   if (len <= this->len_allocated)
   {
      this->length = len;
   }
   else
   {
      cerr << "Warning: Buffer::setLength(" << len << ") cropped to allocated " << this->len_allocated << std::endl;
      this->length = len_allocated;
   }
}