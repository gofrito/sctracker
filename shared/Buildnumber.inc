BUILD_NUMBER_FILE ?= shared/build-number.txt                                                       

# make file if not exists                                             
buildver_create := $(shell if ! test -f $(BUILD_NUMBER_FILE); then echo 1 > $(BUILD_NUMBER_FILE); fi)

# make callable function from update command
buildver = $(shell echo $$(($$(cat $(BUILD_NUMBER_FILE)) + 1)) > $(BUILD_NUMBER_FILE))

# inject values into Version.c at compile time
TAG = $(shell git describe --tags --abbrev=0)
DATE = $(shell date +'%d-%m-%Y')
NUMBER = $(shell cat $(BUILD_NUMBER_FILE))
.PHONY: shared/Version.cpp
shared/Version.cpp: shared/Version.cpp.template
	sed -e "s/__TAG/$(TAG)/g ; s/__DATE/$(DATE)/g ; s/__NUMBER/$(NUMBER)/g" shared/Version.cpp.template > shared/Version.cpp	