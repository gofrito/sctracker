/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2024 Patrick Yates-Jones                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include "UdpSource.h"
#include "Helpers.h"

#include <mark5access.h>


#ifndef SOCK_NONBLOCK
#define SOCK_NONBLOCK O_NONBLOCK
#endif

using std::cerr;
using std::endl;

/**
 * Open the file
 * @return int Returns 0 on success
 * @param  uri File path
 */
int UdpSource::open(std::string uri)
{
    // Let's split our URI into host address and port
    size_t pos = uri.find_last_of(":");

    if (pos != std::string::npos) {
        this->_host_addr = uri.substr(0, pos);
        this->_host_port = uri.substr(pos + 1); // need to skip our port delimiter
    } else {
        cerr << "UdpSource: unable to parse source '" << uri << "' into address and port" << endl;
    }

    /* Determine format: with headers, without headers? */
    std::ostream *log = cfg->tlog;
    this->_format = std::string(cfg->sourceformat_str);
    cfg->sourceformat = Unknown;
    sourceformat_uses_frames = false;

    // we are gonna need this for socket fun time
    int status;
    struct addrinfo hints = {}, *p, *res;

    if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "Mk5B"))
    {
        cerr << "UDP Mk5B not supported" << endl;
    }
    else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "VDIX") || (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VDIX") == Helpers::PrefixMatch))
    {
        cerr << "UDP VDIX not supported" << endl;
    }
    else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "iBob"))
    {
        cerr << "UDP iBob not supported" << endl;
    }
    else
    {
        /* Format has headers that overwrite data */
        if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VLBA") == Helpers::PrefixMatch)
        {
            cerr << "UDP VLBA not supported" << endl;
        }
        else if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "Mark5B") == Helpers::PrefixMatch)
        {
            cerr << "UDP Mark5B not supported" << endl;
        }
        else if (Helpers::caseInsensitiveCompare(cfg->sourceformat_str, "VDIF") == Helpers::PrefixMatch)
        {
            cerr << "UDP VDIF not supported" << endl;
        }
        /* Format uses no headers at all */
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "Maxim"))
        {
            cerr << "UDP Maxim not supported" << endl;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RawSigned"))
        {
            cfg->sourceformat = RawSigned;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RawUnsigned"))
        {
            cfg->sourceformat = RawUnsigned;
        }
        else if (Helpers::caseInsensitiveMatch(cfg->sourceformat_str, "RDF"))
        {
            cerr << "UDP RDF not supported" << endl;
        }
        frame_header_length = 0;
        frame_payload_length = 1;
    }

    /* Show the result */
    if (sourceformat_uses_frames)
    {
        *log << "UdpSource   : processing framed data with " << frame_header_length << "-byte headers and "
             << frame_payload_length << "-byte payloads" << endl;
    }
    else
    {
        *log << "UdpSource   : processing unframed or data-replacement data format" << endl;
    }

    // okay here we go, let's handle this socket stuff

    // first, we zero our hints struct
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC; // we can handle both IPv4 and IPv6
    hints.ai_socktype = SOCK_DGRAM; // datagram socket please
    hints.ai_flags = AI_PASSIVE;

    // okay let's call getaddrinfo
    if ((status = getaddrinfo(NULL, this->_host_port.c_str(), &hints, &res))) {
        cerr << "getaddrinfo error: " << gai_strerror(status) << endl;
    }

    // now we have a list of addrinfos, so we can walk through them,
    // try to create a socket for each one, and pick the first that is succesful
    for (p = res; p != NULL; p = p->ai_next) {
        // let's try creating our socket
        if ((this->socket_fd = socket(p->ai_family, p->ai_socktype,
                                      p->ai_protocol)) == -1) {
            cerr << "UdpSource: error creating socket" << endl;
            continue;
        }

        // if (fcntl(this->socket_fd, F_SETFL, SOCK_NONBLOCK) == -1) {
        //     cerr << "UdpSource: failed to set non-blocking" << endl;
        //     continue;
        // }

        // let's bind to our address
        if (bind(this->socket_fd, p->ai_addr, p->ai_addrlen) == -1) {
            ::close(this->socket_fd);
            cerr << "UdpSource: failed to bind to address" << endl;
            continue;
        }

        // we have a succesful connection, lets break
        break;
    }

    // clean up our addrinfo linked list
    freeaddrinfo(res);

    if (p == NULL) {
        *log << "UdpSource: failed to bind server socket:" << uri << endl;
        got_eof = true;
        return -1;
    }

    /* Skip the first part of data? */
    if (cfg->seconds_to_skip > 0)
    {
        *log << "UdpSource: skipping data is not supported, ignoring..." << endl;
    }

    /* Set a point to stop reading data? */
    if(cfg->seconds_to_stop > 0)
    {
        *log << "UdpSource: seconds to stop is not supported, ignoring..." << endl;
    }

    got_eof = false;
    return 0;
}

/**
 * Try to fill the entire bufferspace with new data and return the number of bytes actually read
 * @return int    Returns the amount of bytes read
 * @param  bspace Pointer to Buffer to fill out
 */
int UdpSource::read(Buffer *buf)
{
    if (NULL == buf || this->cfg == NULL)
    {
        buf->setLength(0);
        cerr << "UdpSource::read() attempt to read in illegal state" << endl;
        return 0;
    }

    std::ostream *log = cfg->tlog;
    size_t num_bytes_req = buf->getAllocated();
    size_t num_bytes_read = 0;
    int recv_bytes = 0;

    // if we want to stop data early, check if we should stop yet
    bool last_frame = false;
    if (cfg->seconds_to_stop > 0 && num_bytes_req > num_bytes_unread)
    {
        num_bytes_req = num_bytes_unread;
        last_frame = true;
    }

    /* Formats without headers or with headers that overwrite data */
    if (!sourceformat_uses_frames)
    {
        while (num_bytes_read < num_bytes_req-1500) // ew magic number
        {
            if ((recv_bytes = recvfrom(this->socket_fd, (buf->getData()+num_bytes_read), (num_bytes_req - num_bytes_read), 0, NULL, NULL)) == -1) {
                // if (errno == EAGAIN || errno == EWOULDBLOCK) {
                //     // oops we are going to block because no data.
                //     // shall we just return?
                //     num_bytes_read = 0;
                //     break;
                *log << "UdpSource: Read I/O error: " << strerror(errno) << endl << std::flush;
                break;
            }
            num_bytes_read += recv_bytes;
        }

        // ifile.read(buf->getData(), num_bytes_req);
        // if (ifile.fail() && !ifile.eof())
        // {
        //     *log << "Read I/O error!" << endl
        //             << std::flush;
        // }

        if (!this->socket_fd)
        {
            got_eof = true;
        }

        if ( num_bytes_read != num_bytes_req && !got_eof)
        {
            // *log << "Read " << num_bytes_read << " bytes instead of " << num_bytes_req << endl;
        }

        *log << "Read " << num_bytes_read << ", expecting " << num_bytes_req << endl;
    }

    if (last_frame) { got_eof = true; }
    buf->setLength(num_bytes_read);
    num_bytes_unread -= num_bytes_read;
    return num_bytes_read;
}

/**
 * Close the resource
 * @return int
 */
int UdpSource::close()
{
    ::close(this->socket_fd);
    return 0;
}

/**
 * Check for end of file
 * @return bool EOF
 */
bool UdpSource::eof()
{
    return got_eof;
}

// note this method always rounds up to whole frames in framed data
// for SourceSkipSeconds and SourceStopSeconds this means it may start slightly late
// and end slightly late to adhere to frame boundaries
size_t UdpSource::seconds_to_source_bytes(int seconds) {
    if (seconds <= 0) { return 0; } // can't have a negative amount of bytes or seconds

    // Simplified this function to handle regular formats
    size_t smp_bytes = (cfg->bits_per_sample * cfg->source_channels * cfg->samplingfreq) / 8;

    if (sourceformat_uses_frames)
    {
        size_t frames = std::ceil((smp_bytes * seconds) / frame_payload_length);
        return frames * (frame_header_length + frame_payload_length);
    }
    else
    {
        return smp_bytes * seconds;
    }
    return 0;
}
