/***************************************************************************
 *   IA-32/x64 Spacecraft Tracker                                          *
 *   Copyright (C) 2020 Jan Wagner / Guifre Molera Calves                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef HELPERS_H
#define HELPERS_H

#include <sys/time.h>
#include <string>

#define byte_To_MB (1 / (1024.0 * 1024.0))
#define byte_To_kB (1 / 1024.0)

enum PhaseCoeffType
{
  SampleBased = 0,
  TimeBased
};
enum WindowFunctionType
{
  None = 0,
  Cosine,
  Cosine2,
  Hamming,
  Hann,
  Blackman
};

class Helpers
{
public:
  enum CiComparisonResult
  {
    NoMatch,
    PrefixMatch,
    FullMatch
  };

public:
  static double getSysSeconds(void);
  static std::string itoa(int n);

  /**
   * Reinterpret a "yes" or "no" string as a boolean.
   * @return String "yes"/"no" translated into true/false
   */
  static bool parseBoolString(const char *str);

  /**
   * Reinterpret string as Window Function.
   * @return WindowFunctionType corresponding to the
   *         string or Cosine2 as default.
   */
  static WindowFunctionType parseWindowingString(const std::string &str);

  /**
   * Reinterpret string as PhaseCoeffType.
   * @return PhaseCoeffType corresponding to the
   *         string or SampleBased as default.
   */
  static PhaseCoeffType parsePhaseCoeffString(const std::string &str);

  /**
   * Print out a complex vector. Mainly for debugging.
   */
  static void printComplexVector(float *v, int pts);

  /**
   * Case-insensitive string equality comparison (measure of similarity).
   */
  static CiComparisonResult caseInsensitiveCompare(const std::string &str1, const std::string &str2);

  /**
   * Case-insensitive string equality comparison (boolean same or not).
   */
  static bool caseInsensitiveMatch(const std::string &str1, const std::string &str2);

  /**
   * Calculate and return the greatest common divisor of two integers.
   */
  static long long gcd(long long, long long);
};

#endif // HELPERS_H
